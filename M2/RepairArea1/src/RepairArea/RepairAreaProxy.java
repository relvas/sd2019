package RepairArea;

import RepairArea.Communication.*;


public class RepairAreaProxy implements iSharedRegion {

    /**
     * RepairArea used to process the messages.
     */
    private final RepairArea repairArea;

    /**
     * RepairArea Proxy constructor.
     * @param repairArea repairArea to process the messages
     */
    public RepairAreaProxy(RepairArea repairArea){
        this.repairArea = repairArea;
    }

    /**
     * Process and reply a message.
     * @param inMessage message received
     * @param scon communication channel
     * @return message to be replied
     */
    @Override
    public Message processAndReply(Message inMessage, ServerCom scon) {
        Message outMessage = null;

        switch(inMessage.getType()) {
            
            case REPAIR_FIX_IT://m c p
                repairArea.fixIt(inMessage.getMechanicId(), inMessage.getCustomerId(), inMessage.getPartId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case REPAIR_GET_REQUIRED_PART://m c p
                outMessage = new Message(MessageType.STATUS_OK);
                repairArea.getRequiredPart(inMessage.getMechanicId(), inMessage.getCustomerId(), inMessage.getPartId());
                break;
            case REPAIR_PART_AVAILABLE:
                outMessage = new Message(MessageType.RETURN_PART_AVAILABLE, repairArea.partAvailable(inMessage.getCustomerId(), inMessage.getPartId()));
                break;
            case REPAIR_READ_THE_PAPER:
                outMessage = new Message(MessageType.RETURN_MECHANIC_TASK, repairArea.readThePaper(inMessage.getMechanicId()));
                break;
            case REPAIR_REGISTER_SERVICE:
                repairArea.registerService(inMessage.getCustomerId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case REPAIR_RESUME_REPAIR_PROCEDURE:// m c p
                repairArea.resumeRepairProcedure(inMessage.getMechanicId(), inMessage.getCustomerId(), inMessage.getPartId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case REPAIR_START_REPAIR_PROCEDURE:
                outMessage = new Message(MessageType.RETURN_REPAIR_INFO, repairArea.startRepairProcedure(inMessage.getMechanicId()));
                break;
            case REPAIR_STORE_PART:
                repairArea.storePart(inMessage.getPartId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case SERVICE_END:
                repairArea.serviceEnd();
                outMessage = new Message(MessageType.STATUS_OK);
                break;
        }
        return outMessage;
    }

}
