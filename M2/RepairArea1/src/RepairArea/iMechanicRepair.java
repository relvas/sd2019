package RepairArea;

/**
 *  Interface - Mechanic Repair Area
 */

public interface iMechanicRepair {
    
    /**
    * Read The Paper (originated by the mechanic).
    * 
    * @param mechanicID Mechanic identifier
    * 
    */
    public boolean readThePaper(int mechanicID);
    
    /**
    * Start repair procedure.
    * 
    * @param mechanicId Mechanic identifier.
    * @return Identifies the Customer car.
    */
    public int startRepairProcedure(int mechanicId);
    
    /**
    * Fix the car.
    * 
    * @param mechanicID Mechanic identifier
    * @param customerID Customer identifier
    * @param pID Part identifier
    */
    public void fixIt(int mechanicID, int customerID, int pID);
    
    /**
    * Get Required Part  
    * 
    * @param mechanicID Mechanic identifier
    * @param customerID Customer identifier
    * @param pID Identifies the part.
    */
    public void getRequiredPart(int mechanicID, int customerID, int pID);
    
    /**
    * Part available
    * 
    * @param customerID Identifies the Customer.
    * @param pID Part needed to fix the car.
    * @return True if the part is available or false if theres isn't.
    */
    public int partAvailable(int customerID, int pID);
    
    /**
    * Resume Repair Procedure
    * 
    * @param mechanicID Mechanic identifier
    * @param customerID Customer identifier
    * @param pID Part identifier
    */
    public void resumeRepairProcedure(int mechanicID, int customerID, int pID);
    
}
