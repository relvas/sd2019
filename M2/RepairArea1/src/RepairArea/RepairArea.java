package RepairArea;

import RepairArea.EntityStates.*;

import RepairArea.Stubs.*;
import RepairArea.Communication.*;



public class RepairArea implements iManagerRepair, iMechanicRepair{
    private Logger logger;
    
    /**
     * Car Parts available.
     * 
     * @serialField carParts[]
     * 
     * Each index is a different part.
     * Value is the amount available.
     */
    private int[] carParts;
    
    /**
     * Parts Requested
     * 
     * @serialField custReqCarPart
     * 
     * Index is the customer Id
     * The value is the part id requested
     */
    private int[] custReqCarPart;
    
    /**
     * Queue of tasks to do
     * 
     * @serialField taskQueue
     */
    private MemFIFO taskQueue;
   
    /**
     * Flag car waiting for part
     * 
     * @serialField carWaiting[]
     */
    private boolean[] carWaiting;
    
    /**
     * Cars waiting for a car part
     * 
     * @serialField carPartQueue
     */
    private MemFIFO carPartQueue;
    
    /**
     * Not First
     * 
     * @serialField notFirst[]
     */
    private boolean[] notFirst;
    
    /**
     * Flag missing
     * 
     * @serialField flagMissing[]
     */
    private boolean[] flagMissing;
    
    
    public RepairArea(Logger repository){
        
        this.logger = repository;
        this.carParts = new int [SimulationParameters.N_DIFF_PARTS];
        
        int idx;
        for (idx=0;idx<SimulationParameters.N_DIFF_PARTS;idx++)
        {
            carParts[idx] = SimulationParameters.N_PARTS;
        }
        
        custReqCarPart = new int [SimulationParameters.N_CUSTOMERS];
        carWaiting = new boolean [SimulationParameters.N_CUSTOMERS];
        for (idx=0;idx<SimulationParameters.N_CUSTOMERS;idx++)
        {
            custReqCarPart[idx] = -1;
            carWaiting[idx]=false;
        }
        
        taskQueue = new MemFIFO(SimulationParameters.N_CUSTOMERS);
        carPartQueue = new MemFIFO(SimulationParameters.N_CUSTOMERS);
        notFirst= new boolean[SimulationParameters.N_MECHANICS];
        for(int i=0;i<SimulationParameters.N_MECHANICS;i++)
            notFirst[i]=false;
        
        flagMissing = new boolean[SimulationParameters.N_DIFF_PARTS];
        for (int k = 0; k<SimulationParameters.N_DIFF_PARTS;k++){
            flagMissing[k] = false;
        }
    }
    
    
    /**
     * Store Part
     * 
     * @param pId Identifies the part
     */
    @Override
    public synchronized void storePart(int pId){
        //((Manager) Thread.currentThread()).setEntityState(ManagerState.REPL);
        carParts[pId] = SimulationParameters.N_PARTS; 
        logger.setReplenishPart(pId);
        logger.setManagerState(ManagerState.REPL);
        flagMissing[pId]=false;
        
        notifyAll();
    }
    
    
    /**
     * Register service
     * 
     * @param customerId Identifies the Customer
     */
    @Override
    public synchronized void registerService(int customerId){
        
        //((Manager) Thread.currentThread()).setEntityState(ManagerState.POST);
        logger.repairCarSubmitted(customerId);
        logger.setManagerState(ManagerState.POST);
        
        taskQueue.write(customerId);
        
        notifyAll();
        
    }
    
    /**
     * Read the paper
     * 
     * @param mechanicId Identifies the Mechanic
     * @return true if Mechanic is alive or false if the Mechanic is dead
     */
    @Override
    public synchronized boolean readThePaper(int mechanicId){
        //if(notFirst[((Mechanic) Thread.currentThread()).getMechId()]){
        //    ((Mechanic) Thread.currentThread()).setEntityState(MechanicState.WAIT);
        //    logger.setMechanicState(mechanicId, MechanicState.WAIT);
        //}
        //else
        //    notFirst[((Mechanic) Thread.currentThread()).getMechId()]=true;
        
        while(taskQueue.empty() && carPartQueue.empty())
        {
            try
            {
             
                wait();
            }
            catch (InterruptedException ex){ 
                return false;
            }
        }
        return true;
    }
    /**
     * Start repair procedure
     * 
     * @param mechanicId
     * @return car Id 
     */
    @Override
    public synchronized int startRepairProcedure(int mechanicId){
        int aux=-1;

            if((!carPartQueue.empty()) ){
                aux = (int)carPartQueue.read();
                
            }
            else if (!taskQueue.empty()) {
                aux = (int)taskQueue.read();
            }
        if(aux!=-1)
        {
            //((Mechanic) Thread.currentThread()).setEntityState(MechanicState.FIXI);
            logger.setMechanicState(mechanicId, MechanicState.FIXI);
        }
        return aux;
    }
    
    /**
    * Fixing the car
    * 
    * @param mechanicId 
    * @param customerId
    * @param pId
    */
    @Override
    public synchronized void fixIt(int mechanicId, int customerId, int pId)
    {
        //((Mechanic) Thread.currentThread()).setEntityState(MechanicState.FIXI);
        logger.setMechanicState(mechanicId, MechanicState.FIXI); 
    }
    
    /**
    * Get required part
    * 
    * @param mechanicId
    * @param customerId
    * @param pId
    */
    @Override
    public synchronized void getRequiredPart(int mechanicId, int customerId, int pId)
    {
        //((Mechanic) Thread.currentThread()).setEntityState(MechanicState.CHEC);
        logger.setMechanicState(mechanicId, MechanicState.CHEC);
    }
    
    /**
    * Get car parts
    * 
    * @param pId
    * @return carParts[pId]
    */
    private synchronized int getCarParts(int pId){
        return carParts[pId];
    }
    
    /**
     * Decrement car parts
     * 
     * @param pId Identifies the part
     */
    private synchronized void decrementCarParts(int pId){
        carParts[pId]--;
    }
    
    
    /**
     * Part available
     * 
     * @param customerId Identifies the customer
     * @param pId Identitifies the part
     * @return true if part is available or false if isnt
     */
    @Override
    public synchronized int partAvailable(int customerId, int pId)
    {
        if (getCarParts(pId) != 0)
        {
            logger.setVehicleWaitingPart(true,pId,custReqCarPart[customerId]==pId);
            decrementCarParts(pId);
            return pId;
        }
        else 
        {
            logger.setFlagPartMissing(pId);
            logger.setVehicleWaitingPart(false,pId,custReqCarPart[customerId]==pId);
            custReqCarPart[customerId]=pId;
            carPartQueue.write(customerId);
            if(!flagMissing[pId]){
                flagMissing[pId]=true;
                pId=-1;
            }
            else
                pId=-999; 
        
            
            return pId;
        }
    }
    
    /**
    * 
    * @param mechanicId
    * @param customerId
    * @param pId
    */
    @Override
    public synchronized void resumeRepairProcedure(int mechanicId, int customerId, int pId)
    {
        //((Mechanic) Thread.currentThread()).setEntityState(MechanicState.FIXI);
        logger.setMechanicState(mechanicId, MechanicState.FIXI);
    }
    public synchronized void serviceEnd(){       
        RepairAreaMain.serviceEnd = true;      
    }
}
