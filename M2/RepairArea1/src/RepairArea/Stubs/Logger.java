package RepairArea.Stubs;

import RepairArea.Communication.ClientCom;
import RepairArea.Communication.Message;
import RepairArea.Communication.MessageType;
import RepairArea.EntityStates.CustomerState;
import RepairArea.EntityStates.ManagerState;
import RepairArea.EntityStates.MechanicState;

/**
 *
 * @author Rui Dias
 * @author Rodrigo Oliveira
 */

/**
 * This data type implements a logging system.
 */
public class Logger {

    /**
     * Name of the computational system where it is located the server.
     */
    private final String serverHostName;

    /**
     * Number of server listening port.
     */
    private final int serverPortNumb;

    /**
     *  Stub instatiation.
     *
     *    @param hostName Name of the computational system where it is located the server.
     *    @param port Number of server listening port.
     */
    public Logger (String hostName, int port) {
        serverHostName = hostName;
        serverPortNumb = port;
    }


    public void setManagerState(ManagerState managerState) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_STATE_MANAGER, managerState);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void setMechanicState(int mechanicID, MechanicState mechanicState) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_STATE_MECHANIC, mechanicID, mechanicState);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void setCustomerState(int customerID, CustomerState customerState) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_STATE_CUSTOMER, customerID, customerState);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void setCustCarDriven(int cId, int repId, boolean secondTime){
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_OWN_CAR, cId, repId, secondTime);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void repairCarSubmitted(int customerId) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_SERVICE_REQUESTED_BY_MANAGER, customerId);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void setWaitingReplacement(int cId, boolean watRepCar) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_REQUIRES_REP_CAR, cId, watRepCar);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void setFlagPartMissing(int partId) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_MANAGER_IS_ALERTED, partId);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void setBoughtPart(int part) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_PART_ORDERED, part);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void setReplenishPart(int part) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_PART_STOCK, part);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void setInQueueSize(int qsize) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_CUSTOMERS_IN_QUEUE, qsize);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void incrementNWaitingRepl(boolean log) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_CUSTOMERS_WAITING_FOR_REP_CAR_INCREMENT, log);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }
    
    public void decrementNWaitingRepl(boolean log) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_CUSTOMERS_WAITING_FOR_REP_CAR_DECREMENT, log);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }
    
    public void setNRepaired(int cId) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_CARS_REPAIRED, cId);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void setParkedVehicles(boolean remove) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_CUSTOMER_CARS_PARKED, remove);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void incrementNCars() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_REPLACEMENT_CARS_PARKED_INCREMENT);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }
    
    public void decrementNCars() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_REPLACEMENT_CARS_PARKED_DECREMENT);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    public void setVehicleWaitingPart(boolean spendPart, int pId, 
            boolean wasWaitingPart) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.UPDATE_CUSTOMER_VEHICLES_WAITING_FOR_PART, spendPart, pId, wasWaitingPart);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }
}