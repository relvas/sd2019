package RepairArea;

import RepairArea.Communication.*;

import RepairArea.RepairAreaProxy;
import RepairArea.ServiceProvider;
import RepairArea.Stubs.*;

import java.net.SocketTimeoutException;

import static RepairArea.SimulationParameters.*;

public class RepairAreaMain {
    /**
     * Used to check if the service must terminate.
     */
    public static boolean serviceEnd = false;

    public static void main(String[] args) {
        /**
         * Communication channels.
         */
        ServerCom scon, sconi;
        ServiceProvider serviceProvider;

        /**
         * Stub initialization.
         */
        Logger logger = new Logger(LOGGER_HOSTNAME, LOGGER_PORT);

        /**
         * Shared region and proxy initialization.
         */
        RepairArea repairArea = new RepairArea(logger);
        RepairAreaProxy repairAreaInt = new RepairAreaProxy(repairArea);

        /**
         * Start listening on the communication channel.
         */
        scon = new ServerCom(REPAIR_PORT);
        scon.start();

        /**
         * While the service is not terminated, accept connections and send them
         * to the service provider.
         */
        while(!serviceEnd) {
            sconi = scon.accept();
            serviceProvider = new ServiceProvider(sconi, repairAreaInt);
            serviceProvider.start();
        }
        System.out.println("Repair Area Finished");        
    }
}
