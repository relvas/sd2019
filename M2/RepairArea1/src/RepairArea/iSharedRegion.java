package RepairArea;

import RepairArea.Communication.*;

public interface iSharedRegion {
    public Message processAndReply(Message input, ServerCom scom);
}
