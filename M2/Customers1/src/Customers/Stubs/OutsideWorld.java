package Customers.Stubs;

import Customers.Communication.ClientCom;
import Customers.Communication.Message;
import Customers.Communication.MessageType;

/**
 *
 * @author Rui Dias
 * @author Rodrigo Oliveira
 */

/**
 * This data type implements the Outside World stub.
 */
public class OutsideWorld {

    /**
     * Name of the computer system where the server is located.
     */
    private String serverHostName = null;

    /**
     * Number of the server listening port.
     */
    private int serverPortNumb;

    /**
     * Stub initialization
     * @param hostName hostName
     * @param portNumb portNumb
     */
    public OutsideWorld(String hostName, int portNumb){
        serverHostName = hostName;
        serverPortNumb = portNumb;
    }
    
    /**
     * Customer is blocked until it decides to go repair the car
     */
    public void decideOnRepair() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.OUTSIDEWORLD_DECIDE_ON_REPAIR);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }
    
    /**
     * Customer goes back to work with a replace car
     * @param customerID Identification of the Customer
     * @param owncar Check if the customer is driving his own car  
     */    
    public void backToWorkByCar(int customerID, boolean owncar) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.OUTSIDEWORLD_BACK_TO_WORK_BY_CAR, customerID, owncar);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
        
    }

    /**
     * Customer goes back to work without a replace car
     * @param customerID Identification of the Customer
     */  
    public void backToWorkByBus(int customerID) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.OUTSIDEWORLD_BACK_TO_WORK_BY_BUS, customerID);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }
        
    /**
     * Terminate service
     */
    public void serviceEnd() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.SERVICE_END);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();  
    }
}
