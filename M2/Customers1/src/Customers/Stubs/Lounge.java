package Customers.Stubs;

import Customers.Communication.ClientCom;
import Customers.Communication.Message;
import Customers.Communication.MessageType;
import Customers.Customer;

/**
 * 
 * @author Rui Dias
 * @author Rodrigo Oliveira
 */

/**
 * This data type implements the Lounge stub.
 */
public class Lounge {

    /**
     * Name of the computer system where the server is located.
     */
    private String serverHostName = null;

    /**
     * Number of the server listening port.
     */
    private int serverPortNumb;

    /**
     * Stub initialization
     * @param hostName hostName
     * @param portNumb portNumb
     */
    public Lounge(String hostName, int portNumb){
        serverHostName = hostName;
        serverPortNumb = portNumb;
    }
  
    /**
     * Customer enters the queue and waits to talk with manager
     * @param customerID Identification of the Customer
     */   
    public void queueIn(int customerID) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_QUEUE_IN, customerID);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }
        
    /**
     * Customer wakes manager to talk to him
     * @param wantRepCar If Customer wants replace car
     * @param customerID Identification of the Customer
     */
    public void talkWithManager(int customerID, boolean wantRepCar) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_TALK_WITH_MANAGER, customerID, wantRepCar);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    /**
     * Customer pays for service when he goes to the Repair Shop for the second time
     * Waits for the Manager to pay her
     * @param customerId Identify the customer
     */
    public void payForService(int customerId) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Customer customer = (Customer)Thread.currentThread();
        Message msg = new Message(MessageType.LOUNGE_PAY_FOR_SERVICE,customerId);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }

    /**
     * Customer gets the key for the replaced car
     * @param customerID Identification of the Customer
     * @return Identification of the key assigned to the Customer
     */
    public int collectKey( int customerID ) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_COLLECT_KEY, customerID);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
  
        return inMessage.getKey();
    }
}
