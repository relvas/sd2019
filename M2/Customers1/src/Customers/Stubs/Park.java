package Customers.Stubs;

import Customers.Communication.ClientCom;
import Customers.Communication.Message;
import Customers.Communication.MessageType;

/**
 *
 * @author Rui Dias
 * @author Rodrigo Oliveira
 */

/**
 * This data type implements a Park stub.
 */
public class Park {
    
    /**
     * Name of the computer system where the server is located.
     */
    private String serverHostName = null;

    /**
     * Number of the server listening port.
     */
    private int serverPortNumb;

    /**
     * Stub initialization
     * @param hostName hostName
     * @param portNumb portNumb
     */
    public Park(String hostName, int portNumb){
        serverHostName = hostName;
        serverPortNumb = portNumb;
    }
    
    /**
     * Customer goes to repair shop and parks the car in the park
     * @param customerID Identification of the Customer
     */
    public void goToRepairShop( int customerID ) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.PARK_GO_TO_REPAIRSHOP, customerID);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }
    
    /**
     * Customer goes to the park to search for his replace car, key has the same ID has the car
     * @param customerID Identification of the Customer
     * @param key Identification of the key for the replace car, key is the index for the array of replaced cars
     */
    public void findCar(int customerID, int key) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.PARK_FIND_CAR, customerID, key);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();

    }

    /**
     * Customer goes to the park to pick his car, already repaired.Key has the same ID has the car
     * @param customerId Identification of the customer
     */
    public void collectCar(int customerId) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.PARK_COLLECT_CAR, customerId);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();

    }
}