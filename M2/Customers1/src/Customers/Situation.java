package Customers;

/**
 *  Contains all states from the Situation enumerate
 */

public enum Situation {
    /**
    *   Manager is getting new parts
    */
    GETTINGNEWPARTS,
    /**
    *   Manager is allerting customer
    */
    ALERTCOSTUMER,
    /**
    *   Manager is attending customer
    */
    ATTENDINGCOSTUMER,
    /**
    *  There is an error
    */
    ERRO
}
