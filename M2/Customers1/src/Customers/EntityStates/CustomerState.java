package Customers.EntityStates;

/**
 * Contains all states from de Customer entity
 */

public enum CustomerState {
    /**
    *   Normal Life With Car
    */
    NLWC,
    
    /**
    *   Park
    */
    PARK,
    
    /**
    *   Waiting For Replacement Car
    */
    WFRC,
    
    /**
    *   Reception
    */
    RECE,
    
    /**
    *   Normal Life Without Car
    */
    NLWO
}
