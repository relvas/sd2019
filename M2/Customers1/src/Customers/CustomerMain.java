package Customers;

import Customers.Stubs.Lounge;
import Customers.Stubs.OutsideWorld;
import Customers.Stubs.Park;

import static Customers.SimulationParameters.*;

public class CustomerMain {
    public static void main(String[] args) {
        Lounge lounge = new Lounge(LOUNGE_HOSTNAME, LOUNGE_PORT);
        OutsideWorld outsideWorld = new OutsideWorld(OUTSIDE_WORLD_HOSTNAME, OUTSIDE_WORLD_PORT);
        Park park = new Park(PARK_HOSTNAME, PARK_PORT);

        Customer[] customers = new Customer[N_CUSTOMERS];

        /**
         * Initializing Customers
         */
       
        for (int i = 0; i < customers.length; i++) 
            customers[i] = new Customer(i, park, lounge, outsideWorld);
        

        /**
         * Starting Thread
         */
        for (Customer customer : customers) 
            customer.start();
        

        /**
         * Joining Thread
         */
        for (Customer customer : customers) {
            try {
                customer.join();
            } catch (InterruptedException ex) { }
        }
        System.out.println("Customer Finished");        
        outsideWorld.serviceEnd();
    }
}
