echo "*** Script de Deployment ***"

export SSHPASS='3prud3nm'

echo -e "\n** Copying code for execution **"

echo -e "\n> Moving Logger to machine 1"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws01.ua.pt << !
	put -r Logger1/
	bye
!

echo -e "\n> Moving Lounge to machine 2"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws02.ua.pt << !
	put -r Lounge1/
	bye
!

echo -e "\n> Moving OutsideWorld to machine 3"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws03.ua.pt << !
	put -r OutsideWorld1/
	bye
!

echo -e "\n> Moving Park to machine 4"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws04.ua.pt << !
	put -r Park1/
	bye
!

echo -e "\n> Moving RepairArea to machine 5"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws05.ua.pt << !
	put -r RepairArea1/
	bye
!

echo -e "\n> Moving SupplierSite to machine 6"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws06.ua.pt << !
	put -r SupplierSite1/
	bye
!

echo -e "\n> Moving Managers to machine 7"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws07.ua.pt << !
	put -r Managers1/
	bye
!

echo -e "\n> Moving Mechanics to machine 8"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws08.ua.pt << !
	put -r Mechanics1/
	bye
!

echo -e "\n> Moving Customers to machine 9"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws09.ua.pt << !
	put -r Customers1/
	bye
!


##########################################################################################

echo -e "\n** Compiling **"

echo -e "\n> Compiling Logger in machine 1"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws01.ua.pt << EOF
	cd Logger1/
	find . -name "*.java" > files.txt
	javac @files.txt
	rm files.txt
EOF

echo -e "\n> Compiling Lounge in machine 2"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws02.ua.pt << EOF
	cd Lounge1/
	find . -name "*.java" > files.txt
	javac @files.txt
	rm files.txt
EOF

echo -e "\n> Compiling OutsideWorld in machine 3"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws03.ua.pt << EOF
	cd OutsideWorld1/
	find . -name "*.java" > files.txt
	javac @files.txt
	rm files.txt
EOF

echo -e "\n> Compiling Park in machine 4"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws04.ua.pt << EOF
	cd Park1/
	find . -name "*.java" > files.txt
	javac @files.txt
	rm files.txt
EOF

echo -e "\n> Compiling RepairArea in machine 5"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws05.ua.pt << EOF
	cd RepairArea1/
	find . -name "*.java" > files.txt
	javac @files.txt
	rm files.txt
EOF

echo -e "\n> Compiling SupplierSite in machine 6"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws06.ua.pt << EOF
	cd SupplierSite1/
	find . -name "*.java" > files.txt
	javac @files.txt
	rm files.txt
EOF

echo -e "\n> Compiling Manager in machine 7"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws07.ua.pt << EOF
	cd Managers1/
	find . -name "*.java" > files.txt
	javac @files.txt
	rm files.txt
EOF

echo -e "\n> Compiling Mechanic in machine 8"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws08.ua.pt << EOF
	cd Mechanics1/
	find . -name "*.java" > files.txt
	javac @files.txt
	rm files.txt
EOF

echo -e "\n> Compiling Customer in machine 9"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws09.ua.pt << EOF
	cd Customers1/
	find . -name "*.java" > files.txt
	javac @files.txt
	rm files.txt
EOF

###

echo -e "\n** Execution **"

echo -e "\n> Executing Logger in machine 1"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws01.ua.pt << EOF
	cd Logger1/src/logger/
	nohup java -cp /home/sd0407/Logger1/src logger.LoggerMain > /dev/null 2>&1 &
EOF

echo -e "\n> Executing Lounge in machine 2"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws02.ua.pt << EOF
	cd Lounge1/src/Lounge/
	nohup java -cp /home/sd0407/Lounge1/src Lounge.LoungeMain > /dev/null 2>&1 &
EOF

echo -e "\n> Executing OutsideWorld in machine 3"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws03.ua.pt << EOF
	cd OutsideWorld1/src/OutsideWorld/
	nohup java -cp /home/sd0407/OutsideWorld1/src OutsideWorld.OutsideWorldMain > /dev/null 2>&1 &
EOF

echo -e "\n> Executing Park in machine 4"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws04.ua.pt << EOF
	cd Park1/src/Park/
	nohup java -cp /home/sd0407/Park1/src Park.ParkMain > /dev/null 2>&1 &
EOF

echo -e "\n> Executing RepairArea in machine 5"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws05.ua.pt << EOF
	cd RepairArea1/src/RepairArea/
	nohup java -cp /home/sd0407/RepairArea1/src RepairArea.RepairAreaMain > /dev/null 2>&1 &
EOF

echo -e "\n> Executing SupplierSite in machine 6"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws06.ua.pt << EOF
	cd SupplierSite1/src/SupplierSite/
	nohup java -cp /home/sd0407/SupplierSite1/src SupplierSite.SupplierSiteMain > /dev/null 2>&1 &
EOF

# Wait for the shared regions to be execute before the enities

sleep 1

echo -e "\n> Executing Managers in machine 7"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws07.ua.pt << EOF
	cd Managers1/src/Managers/
	nohup java -cp /home/sd0407/Managers1/src Managers.ManagerMain > /dev/null 2>&1 &
EOF

echo -e "\n> Executing Mechanics in machine 8"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws08.ua.pt << EOF
	cd Mechanics1/src/Mechanics/
	nohup java -cp /home/sd0407/Mechanics1/src Mechanics.MechanicMain > /dev/null 2>&1 &
EOF

echo -e "\n> Executing Customers in machine 9"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws09.ua.pt << EOF
	cd Customers1/src/Customers/
	nohup java -cp /home/sd0407/Customers1/src Customers.CustomerMain > /dev/null 2>&1 &
EOF