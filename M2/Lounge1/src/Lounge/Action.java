package Lounge;

/**
 * Contains all states from the Action enumerate
 */

public enum Action {
    /**
    *   Customer doesn't want key
    */
    NOKEY,
    /**
    *   Customer want's key
    */
    KEY,
    /**
    *   Customer want's to pay
    */
    PAY,
    /**
    *   There is an error
    */
    ERROR
}
