package Lounge;

/**
 * Interface - Mechanic Lounge
 */

public interface iMechanicLounge {
    
    /**
    * The Mechanic let the Manager know we need to replace a part.
    * 
    * @param mechanicID Identifies the Mechanic.
    * @param part Identifies the part needed.
    */
    public void letManagerKnow(int mechanicID, int part);
    
    /**
    * The Mechanic let the Manager knows that the car from customerID is ready.
    * 
    * @param mechanicID Identifies the Mechanic.
    * @param customerID Identifies the Customer car.
    */
    public void repairConcluded(int mechanicID, int customerID);
    
}
