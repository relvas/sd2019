package Lounge;

import Lounge.Communication.*;


public class LoungeProxy implements iSharedRegion {

    /**
     * Lounge used to process the messages.
     */
    private final Lounge lounge;

    /**
     * Lounge Proxy constructor.
     * @param lounge lounge to process the messages
     */
    public LoungeProxy(Lounge lounge){
        this.lounge = lounge;
    }

    /**
     * Process and reply a message.
     * @param inMessage message received
     * @param scon communication channel
     * @return message to be replied
     */
    @Override
    public Message processAndReply(Message inMessage, ServerCom scon) {
        Message outMessage = null;

        switch(inMessage.getType()){
            case LOUNGE_APPRAISE_SIT:
                outMessage = new Message(MessageType.RETURN_SITUATION, lounge.appraiseSit());
                break;
            case LOUNGE_COLLECT_KEY:
                outMessage = new Message(MessageType.RETURN_KEY, lounge.collectKey(inMessage.getCustomerId()));
                break;
            case LOUNGE_GET_NEXT_TASK:
                outMessage = new Message(MessageType.RETURN_NEXT_TASK, lounge.getNextTask());
                break;
            case LOUNGE_HAND_CAR_KEY:
                lounge.handCarKey(inMessage.getCustomerId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case LOUNGE_LET_MANAGER_KNOW:
                lounge.letManagerKnow(inMessage.getMechanicId(), inMessage.getPartId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case LOUNGE_PAY_FOR_SERVICE:
                lounge.payForService(inMessage.getCustomerId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case LOUNGE_QUEUE_IN:
                lounge.queueIn(inMessage.getCustomerId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case LOUNGE_RECEIVE_PAYMENT:
                lounge.receivePayment();
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case LOUNGE_REPAIR_CONCLUDED:
                lounge.repairConcluded(inMessage.getMechanicId(), inMessage.getCustomerId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case LOUNGE_TALK_TO_CUSTOMER:
                lounge.talkToCustomer();
                outMessage = new Message(MessageType.STATUS_OK );
                break;
            case LOUNGE_TALK_WITH_MANAGER:
                lounge.talkWithManager(inMessage.getCustomerId(), inMessage.isWantsRepCar());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case SERVICE_END:
                lounge.serviceEnd();
                outMessage = new Message(MessageType.STATUS_OK);
                break;
        }
        return outMessage;
    }
}
