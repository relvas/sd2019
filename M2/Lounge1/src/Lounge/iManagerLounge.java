package Lounge;

/**
 *Interface - Managar Lounge
 */

public interface iManagerLounge {
    
    /**
    * Get next task (originated by the manager).
    * 
    */
    public boolean getNextTask();
    
    /**
    * The Manager checks the next task.
    * 
    * @return Situation Reference to whats next
    */
    public Situation appraiseSit();
    
    /**
    * The Manager talks to the Customer.
    * 
    * @return Action What the customer wants
    */
    public Action talkToCustomer();
    
    /**
     * The Manager checks if there is a replacement car available and if there is a key is delivery to the Customer.  
     * 
     * @param customerID Identifies the Customer.
     */
    public void handCarKey(int customerID);
    
    /**
     * Manager checks if the Customer have a replace car.  
     * 
     */ 
    public void receivePayment();
    
    /**
     * Get part
     * 
     * @return Part
     */
    public int getPartId();
    
     /**
     * Get customer
     * 
     * @return customer
     */
    public int getCId();
    
    
    /**
     * Set Flag restocked
     * 
     * @param partId Identifies the part
     */
    public void setFlagRestocked(int partId);
    
     /**
     * Get Phone
     * 
     * @return phone
     */
    public int getPhoneId();
    
}
