package Lounge;

import Lounge.Communication.*;

public interface iSharedRegion {
    public Message processAndReply(Message input, ServerCom scom);
}
