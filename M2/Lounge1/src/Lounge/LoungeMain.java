package Lounge;

import Lounge.Communication.*;
import Lounge.Stubs.*;

import static Lounge.SimulationParameters.*;

public class LoungeMain {
    /**
     * Used to check if the service must terminate.
     */
    public static boolean serviceEnd = false;

    public static void main(String[] args) {
        /**
         * Communication channels.
         */
        ServerCom scon, sconi;
        ServiceProvider serviceProvider;

        /**
         * Stub initialization.
         */
        Logger logger = new Logger(LOGGER_HOSTNAME, LOGGER_PORT);

        /**
         * Shared region and proxy initialization.
         */
        Lounge lounge = new Lounge(logger);
        LoungeProxy loungeInt = new LoungeProxy(lounge);

        /**
         * Start listening on the communication channel.
         */
        scon = new ServerCom(LOUNGE_PORT);
        scon.start();

        /**
         * While the service is not terminated, accept connections and send them
         * to the service provider.
         */
        while(!serviceEnd) {
            sconi = scon.accept();
            serviceProvider = new ServiceProvider(sconi, loungeInt);
            serviceProvider.start();
        }
        System.out.println("Lounge finished");        

    }
}
