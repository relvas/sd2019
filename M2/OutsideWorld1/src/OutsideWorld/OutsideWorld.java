package OutsideWorld;

import OutsideWorld.EntityStates.*;
import OutsideWorld.Stubs.*;

/**
 * 
 * Shared region - Outside World
 */

public class OutsideWorld implements iCustomerOutside, iManagerOutside{
    
    private Logger logger;
    boolean []carsReady; 
    int finished = 0;
    public OutsideWorld(Logger repository){
        logger = repository;
        
        carsReady = new boolean[SimulationParameters.N_CUSTOMERS];
        for (int i = 0; i <SimulationParameters.N_CUSTOMERS ; i++) 
        {
            carsReady[i]=false;
        }
        
    }
    
    /**
     * Back to work by car (originated by the customer).
     * 
     * @param customerID Identifier of the customer 
     */
    @Override
    public synchronized void backToWorkByCar(int customerID, boolean owncar)
    {
        if(owncar)
        {
            logger.setParkedVehicles(true);
            logger.setCustCarDriven(customerID,-1,true);
        }
        else{
            //logger.setCustCarDriven(customerID, ((Customer) Thread.currentThread()).getKey(), false);
        }  
        
        logger.setCustomerState(customerID, CustomerState.NLWC);        
        while (!carsReady[customerID]) {
            try {
                wait();
            } catch (InterruptedException e){}
        } 
    }
   
    /**
     * Manager phones to the Customer - car fixed.
     *   
     * @param customerID Identifies the Customer.
     */
    @Override
    public synchronized void phoneCustomer(int customerID) {
        
        logger.setManagerState(ManagerState.ALLC);

        carsReady[customerID] = true;
        notifyAll();
    }
    
    /**
     * Back to work by bus (originated by the customer).
     * 
     * @param customerID Identifier of the customer    
     */
    @Override
    public synchronized void backToWorkByBus(int customerID) {
        logger.setCustCarDriven(customerID, -1, false);
        logger.setCustomerState(customerID, CustomerState.NLWO);
        
        while (!carsReady[customerID]) {
            try {
                wait();
            } catch (InterruptedException e){}
        }
        
    }    
    public synchronized void serviceEnd(){       
        OutsideWorldMain.serviceEnd = true;      
    }
}
