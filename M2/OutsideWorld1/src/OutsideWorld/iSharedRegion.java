package OutsideWorld;

import OutsideWorld.Communication.*;

public interface iSharedRegion {
    public Message processAndReply(Message input, ServerCom scom);
}
