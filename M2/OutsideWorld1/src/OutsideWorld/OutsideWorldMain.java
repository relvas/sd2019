package OutsideWorld;

import OutsideWorld.OutsideWorld;
import OutsideWorld.OutsideWorldProxy;
import OutsideWorld.ServiceProvider;
import OutsideWorld.Stubs.Logger;
import OutsideWorld.Communication.*;
import static OutsideWorld.SimulationParameters.*;

public class OutsideWorldMain {
    
    public static boolean serviceEnd = false;

    public static void main(String[] args) {
        /**
         * Communication channels.
         */
        ServerCom scon, sconi;
        ServiceProvider serviceProvider;

        /**
         * Stub initialization.
         */
        Logger logger = new Logger(LOGGER_HOSTNAME, LOGGER_PORT);

        /**
         * Shared region and proxy initialization.
         */
        OutsideWorld outsideWorld = new OutsideWorld(logger);
        OutsideWorldProxy outsideWorldInt = new OutsideWorldProxy(outsideWorld);

        /**
         * Start listening on the communication channel.
         */
        scon = new ServerCom(OUTSIDE_PORT);
        scon.start();

        /**
         * While the service is not terminated, accept connections and send them
         * to the service provider.
         */
        while(!serviceEnd) {
            sconi = scon.accept();
            serviceProvider = new ServiceProvider(sconi, outsideWorldInt);
            serviceProvider.start();
        }
        System.out.println("Outside World Finished");        
    }
}
