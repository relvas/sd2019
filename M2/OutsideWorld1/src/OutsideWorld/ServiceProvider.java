package OutsideWorld;

import OutsideWorld.Communication.*;


public class ServiceProvider extends Thread {

    /**
     * Communication channel with the server.
     */
    private final ServerCom com;

    /**
     * Shared region implementation.
     */
    private final iSharedRegion rtInt;

    /**
     * Service Provider constructor.
     * @param com communication channel with the server.
     * @param rtInt shared region.
     */
    public ServiceProvider(ServerCom com, iSharedRegion rtInt){
        this.com = com;
        this.rtInt = rtInt;
    }

    /**
     * Lifecycle of the service provider.
     */
    @Override
    public void run() {
        try {        
        /**
         * Read object from the communication channel.
         */
        Message inMessage = (Message) com.readObject();
        
        if(inMessage.getType() == MessageType.SERVICE_END){
            com.setSocketEnd();
            com.end();
        }
        /**
         * Process and reply request.
         */
        Message outMessage = rtInt.processAndReply(inMessage, com);

        /**
         * Send reply and close communication channel.
         */
        com.writeObject(outMessage);
        com.close();
        } catch (NullPointerException e) {}
    }
}


