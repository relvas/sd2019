package OutsideWorld;

import OutsideWorld.Communication.*;


public class OutsideWorldProxy implements iSharedRegion{
     private final OutsideWorld outsideWorld;

    /**
     * OutsideWorld Proxy constructor.
     * @param outsideWorld outsideWorld to process the messages
     */
    public OutsideWorldProxy(OutsideWorld outsideWorld){
        this.outsideWorld = outsideWorld;
    }

    /**
     * Process and reply a message.
     * @param inMessage message received
     * @param scon communication channel
     * @return message to be replied
     */
    @Override
    public Message processAndReply(Message inMessage, ServerCom scon) {
        Message outMessage = null;

        switch(inMessage.getType()){
            case OUTSIDEWORLD_BACK_TO_WORK_BY_BUS:
                outsideWorld.backToWorkByBus(inMessage.getCustomerId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case OUTSIDEWORLD_BACK_TO_WORK_BY_CAR:
                outsideWorld.backToWorkByCar(inMessage.getCustomerId(), inMessage.isOnwCar());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            
            case OUTSIDEWORLD_PHONE_CUSTOMER:
                outsideWorld.phoneCustomer(inMessage.getRepairedCarId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case SERVICE_END:
                outsideWorld.serviceEnd();
                outMessage = new Message(MessageType.STATUS_OK);
                break;
        }
        return outMessage;
    }
}
