package OutsideWorld;

/**
 *  Interface - Manager Outside World
 */

public interface iManagerOutside {
    
    /**
     * Manager phones to the Customer - car fixed.
     *   
     * @param customerID Identifies the Customer.
     */
    public void  phoneCustomer(int customerID);
    
}
