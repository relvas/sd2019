echo "*** Local Script ***"

echo -e "\n** Compiling **"

echo -e "\n> Compiling Logger"
cd Logger1/
javac $(find . -name '*.java') 
cd ..

echo -e "\n> Compiling Lounge"
cd Lounge1/
javac $(find . -name '*.java')
cd ..

echo -e "\n> Compiling OutsideWorld"
cd OutsideWorld1/
javac $(find . -name '*.java')
cd ..

echo -e "\n> Compiling Park"
cd Park1/
javac $(find . -name '*.java')
cd ..

echo -e "\n> Compiling RepairArea"
cd RepairArea1/
javac $(find . -name '*.java')
cd ..

echo -e "\n> Compiling SupplierSite"
cd SupplierSite1/
javac $(find . -name '*.java')
cd ..

echo -e "\n> Compiling Customers"
cd Customers1
javac $(find . -name '*.java')
cd ..

echo -e "\n> Compiling Managers"
cd Managers1
javac $(find . -name '*.java')
cd ..

echo -e "\n> Compiling Mechanics"
cd Mechanics1
javac $(find . -name '*.java')
cd ..

##############################################################################

echo -e "\n** Execution **"

echo -e "\n> Executing Logger"
cd Logger1/src/
java logger/LoggerMain &
cd ../..

echo -e "\n> Executing Lounge"
cd Lounge1/src/
java Lounge/LoungeMain &
cd ../..

echo -e "\n> Executing OutsideWorld"
cd OutsideWorld1/src/
java OutsideWorld/OutsideWorldMain &
cd ../..

echo -e "\n> Executing Park"
cd Park1/src/
java Park/ParkMain &
cd ../..

echo -e "\n> Executing RepairArea"
cd RepairArea1/src/
java RepairArea/RepairAreaMain &
cd ../..

echo -e "\n> Executing SupplierSite"
cd SupplierSite1/src/
java SupplierSite/SupplierSiteMain &
cd ../..

# Wait for the shared regions to be execute before the enities

sleep 1

echo -e "\n> Executing Customers"
cd Customers1/src/
java Customers/CustomerMain &
cd ../..

echo -e "\n> Executing Managers"
cd Managers1/src/
java Managers/ManagerMain &
cd ../..

echo -e "\n> Executing Mechanics"
cd Mechanics1/src/
java Mechanics/MechanicMain &
cd ../..

wait

echo -e "\n*** Script End ***"