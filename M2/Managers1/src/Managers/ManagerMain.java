package Managers;


import Managers.Stubs.*;
import static Managers.SimulationParameters.*;

public class ManagerMain {
    public static void main(String[] args) {
        Lounge lounge = new Lounge(LOUNGE_HOSTNAME, LOUNGE_PORT);
        OutsideWorld outsideWorld = new OutsideWorld(OUTSIDE_WORLD_HOSTNAME, OUTSIDE_WORLD_PORT);
        RepairArea repairArea = new RepairArea(REPAIR_HOSTNAME, REPAIR_PORT);
        SupplierSite supplierSite = new SupplierSite(SUPPLIER_HOSTNAME, SUPPLIER_PORT);

        /**
         * Initializing Entitie
         */
        Managers manager = new Managers(supplierSite, lounge, repairArea, outsideWorld);

        /**
         * Starting Thread
         */
        manager.start();

        /**
         * Joining Thread
         */
        try {
            manager.join();
        } catch (InterruptedException ex) { }        
        System.out.println("\033[0;31m Manager ended \033[0m");

        supplierSite.serviceEnd();
        lounge.serviceEnd();
    }
}
