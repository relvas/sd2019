package Managers.Stubs;

import Managers.Communication.ClientCom;
import Managers.Communication.Message;
import Managers.Communication.MessageType;


/**
 *
 * @author Rui Dias
 * @author Rodrigo Oliveira
 */

/**
 * This data type implements the Outside World stub.
 */
public class OutsideWorld {

    /**
     * Name of the computer system where the server is located.
     */
    private String serverHostName = null;

    /**
     * Number of the server listening port.
     */
    private int serverPortNumb;

    /**
     * Stub initialization
     * @param hostName
     * @param portNumb 
     */
    public OutsideWorld(String hostName, int portNumb){
        serverHostName = hostName;
        serverPortNumb = portNumb;
    }

    /**
     * Manager informs the Customer that the car is fixed and search for next task
     * @param customerId Identification of the customer
     */
    public void phoneCustomer(int customerId) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.OUTSIDEWORLD_PHONE_CUSTOMER, customerId);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();        
    }
}

