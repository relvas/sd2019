package Managers.Stubs;

import Managers.Communication.ClientCom;
import Managers.Communication.Message;
import Managers.Communication.MessageType;

/**
 * This data type implements a Repair Area shared region.
 */
public class RepairArea {
  
    /**
     * Name of the computer system where the server is located.
     */
    private String serverHostName = null;

    /**
     * Number of the server listening port.
     */
    private int serverPortNumb;

    /**
     * Stub initialization
     * @param hostName
     * @param portNumb 
     */
    public RepairArea(String hostName, int portNumb){
        serverHostName = hostName;
        serverPortNumb = portNumb;
    }
    
    /**
     * Manager register the service and search for next task
     * @param customerId Identification of the car to repair
     */
    public void registerService(int customerId) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.REPAIR_REGISTER_SERVICE, customerId);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();
    }
    
    /**
     * Manager stores the needed part after getting it from the supplier
     * @param part type of part to store
     */
    public void storePart(int part) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.REPAIR_STORE_PART, part);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();       
    }
    
    /**
     * Manager closes the repair shop after killing all mechanics
     */
    public void closingStore() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.REPAIR_CLOSING_STORE);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();
    }
}
