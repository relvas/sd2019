package Managers.Stubs;

import Managers.Communication.ClientCom;
import Managers.Communication.Message;
import Managers.Communication.MessageType;
import Managers.EntityStates.ManagerState;


/**
 * This data type implements a Supplier Site shared region.
 */
public class SupplierSite {
   
    /**
     * Name of the computer system where the server is located.
     */
    private String serverHostName = null;

    /**
     * Number of the server listening port.
     */
    private int serverPortNumb;

    /**
     * Stub initialization
     * @param hostName
     * @param portNumb 
     */
    public SupplierSite(String hostName, int portNumb){
        serverHostName = hostName;
        serverPortNumb = portNumb;
    }
    
    /**
     * Manager goes to the supplier to buy a needed part 
     * @param missPart Identification of the missing part
     * @return number of parts the Manager bought
     */
    public int goToSupplier(int missPart) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.SUPPLIERSITE_GO_TO_SUPPLIER, missPart);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
        
        return inMessage.getNumberOfParts();
    }
     
    /**
     * Terminate service
     */
    public void serviceEnd() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.SERVICE_END);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();  
    }
}
