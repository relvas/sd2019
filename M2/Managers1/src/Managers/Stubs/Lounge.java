package Managers.Stubs;

import Managers.Action;
import Managers.Communication.ClientCom;
import Managers.Communication.Message;
import Managers.Communication.MessageType;
import Managers.EntityStates.CustomerState;
import Managers.EntityStates.ManagerState;
import Managers.EntityStates.MechanicState;
import Managers.Situation;

/**
 * 
 * @author Rui Dias
 * @author Rodrigo Oliveira
 */

/**
 * This data type implements the Lounge stub.
 */
public class Lounge {

    /**
     * Name of the computer system where the server is located.
     */
    private String serverHostName = null;

    /**
     * Number of the server listening port.
     */
    private int serverPortNumb;

    /**
     * Stub initialization
     * @param hostName
     * @param portNumb 
     */
    public Lounge(String hostName, int portNumb){
        serverHostName = hostName;
        serverPortNumb = portNumb;
    }
  
    /**
     * Manager is blocked if it has nothing to do, if a task need to be done, some entity wakes him
     * @return True if the Manager has work
     */
    public boolean getNextTask() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_GET_NEXT_TASK);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();

        return inMessage.isNextTask();
    }
    
    /**
     * Manager sees what task she has to perform
     * @return The situation she's at
     */
    public Situation appraiseSit() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_APPRAISE_SIT);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
        
        return inMessage.getManagerSituation(); // don't know if correct
    }
            
    /**
     * Manager wakes Customer that is in the queue
     * @return what customer wants to do
     */
    public Action talkToCustomer() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_TALK_TO_CUSTOMER);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
              
        return inMessage.getManagerAction();
    }  
    
    /**
     * Manager gives the Customer the key for the replace car
     * @param customerId identifies the customer
     */
    public void handCarKey(int customerId) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_HAND_CAR_KEY, customerId);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    }
            
    /**
     * Manager receives the payment the second time Customer goes to Repair Shop
     * Waits for the Customer to return the key
     */
    public void receivePayment() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_RECEIVE_PAYMENT);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
        
    }
    
    /**
     * Getting the id of part needed
     * @return part
     */
    public int getPartId(){
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_GET_PARTID);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
        
        return inMessage.getPartId();
    }
    
    
    /**
     * Used to flag the part was restocked
     * @param partId Part needed to restock
     */
    public void setFlagRestocked(int partId){
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_SET_FLAGRESTOCKED, partId);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
        
    }
    
    
    /**
     * Getting the id of the customers phone
     * @return customerId 
     */
    public int getPhoneId(){
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_GET_PHONEID);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
        
        return inMessage.getCustomerId();
    }
    
    
    /**
     * Getting the id of the customers phone
     * @return customerId 
     */
    public int getCId(){
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_GET_CID);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
        
        return inMessage.getCustomerId();
    }
    
    /**
     * Terminate service
     */
    public void serviceEnd() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.SERVICE_END);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();  
    }
}