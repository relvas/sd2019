package Managers.EntityStates;

/**
 * Contains all states from de Manager entity
 */

public enum ManagerState {
    /**
    *   Attending Customer
    */
    ATTC,
    
    /**
    *   Checking What To Do
    */
    CWTD,
    
    /**
    *   Getting New Car Parts
    */
    GNCA,
    
    /**
    *   Posting Job
    */
    POST,
    
    /**
    *   Allerting Customer
    */
    ALLC,
    
    
    /**
    *   Replenish Stock
    */
    REPL
}
