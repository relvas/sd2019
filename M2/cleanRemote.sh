echo "*** Clean Remote Machines Script ***"

export SSHPASS='3prud3nm'

##########################################################################################

echo -e "\n> Delete Logger from machine 1"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws01.ua.pt << EOF
	rm -rf Logger1
EOF

echo -e "\n> Delete Lounge from machine 2"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws02.ua.pt << EOF
	rm -rf Lounge1
EOF

echo -e "\n> Delete OutsideWorld from machine 3"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws03.ua.pt << EOF
	rm -rf OutsideWorld1
EOF

echo -e "\n> Delete Park from machine 4"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws04.ua.pt << EOF
	rm -rf Park1
EOF

echo -e "\n> Delete RepairArea from machine 5"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws05.ua.pt << EOF
	rm -rf RepairArea1
EOF

echo -e "\n> Delete SupplierSite from machine 6"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws06.ua.pt << EOF
	rm -rf SupplierSite1
EOF

echo -e "\n> Delete Managers from machine 7"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws07.ua.pt << EOF
	rm -rf Managers1
EOF
echo -e "\n> Delete Mechanics from machine 8"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws08.ua.pt << EOF
	rm -rf Mechanics1
EOF

echo -e "\n> Delete Customers from machine 9"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws09.ua.pt << EOF
	rm -rf Customers1
EOF