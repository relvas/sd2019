/**
 * This package provides the classes required to establish communication between clients and servers.
 * Contains the messages that can be exchanged.
 */
package logger.Communication;

