package logger.SharedRegion;

import logger.Communication.Message;
import logger.Communication.ServerCom;
import logger.Communication.MessageType;

/**
 * Service Provider implementation.
 * Processes and replies messages accordingly to the internal implementation
 * of a shared region.
 * @author Rui Dias
 * @author Rodrigo Oliveira
 */
public class ServiceProvider extends Thread {

    /**
     * Communication channel with the server.
     */
    private final ServerCom com;

    /**
     * Shared region implementation.
     */
    private final ISharedRegion rtInt;

    /**
     * Service Provider constructor.
     * @param com communication channel with the server.
     * @param rtInt shared region.
     */
    public ServiceProvider(ServerCom com, ISharedRegion rtInt){
        this.com = com;
        this.rtInt = rtInt;
    }

    /**
     * Lifecycle of the service provider.
     */
    @Override
    public void run() {
        try {        
        /**
         * Read object from the communication channel.
         */
        Message inMessage = (Message) com.readObject();
        
        if(inMessage.getType() == MessageType.SERVICE_END){
            com.end();
        }
        /**
         * Process and reply request.
         */
        Message outMessage = rtInt.processAndReply(inMessage, com);

        /**
         * Send reply and close communication channel.
         */
        com.writeObject(outMessage);
        com.close();
        } catch (NullPointerException e) {}
    }
}
