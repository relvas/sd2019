package logger.SharedRegion;

import logger.Communication.Message;
import logger.Communication.ServerCom;

/**
 * This interface represents a shared region interface
 * It has a method to process and reply messages.
 * @author Rui Dias
 * @author Rodrigo Oliveira
 */
public interface ISharedRegion {

    /**
     * Process and reply a message
     * @param inMessage message to be processed
     * @param scon communication channel
     * @return message to be replied
     */
    public Message processAndReply(Message inMessage, ServerCom scon);
}