package logger.SharedRegion;

import logger.Communication.Message;
import logger.Communication.MessageType;
import logger.Communication.ServerCom;

/**
 * Proxy for the logger shared region.
 * Implements the ISharedRegion interface, and listens to the requests,
 * processes them and replies.
 * @author Rui Dias 
 * @author Rodrigo Oliveira
 */
public class LoggerProxy implements ISharedRegion {

    /**
     * Logger used to process the messages.
     */
    private final Logger logger;

    /**
     * Logger Proxy constructor.
     * @param logger logger to process the messages
     */
    public LoggerProxy(Logger logger){
        this.logger = logger;
    }

    /**
     * Process and reply a message.
     * @param inMessage message received
     * @param scon communication channel
     * @return message to be replied
     */
    @Override
    public Message processAndReply(Message inMessage, ServerCom scon) {
        Message outMessage = null;

        switch(inMessage.getType()){
            case UPDATE_CARS_REPAIRED:
                logger.setNRepaired(inMessage.getCustomerId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_CUSTOMER_CARS_PARKED:
                logger.setParkedVehicles(inMessage.isParked());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_STATE_CUSTOMER:
                logger.setCustomerState(inMessage.getCustomerId(), inMessage.getCustomerState());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_CUSTOMER_VEHICLES_WAITING_FOR_PART:
                logger.setVehicleWaitingPart(inMessage.getSpendPart(), inMessage.getPartId(), inMessage.getWasWaitingPart());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_CUSTOMERS_IN_QUEUE:
                logger.setInQueueSize(inMessage.getQSize());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_CUSTOMERS_WAITING_FOR_REP_CAR_INCREMENT:
                logger.incrementNWaitingRepl(inMessage.getLog());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_CUSTOMERS_WAITING_FOR_REP_CAR_DECREMENT:
                logger.decrementNWaitingRepl(inMessage.getLog());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_STATE_MANAGER:
                logger.setManagerState(inMessage.getManagerState());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_STATE_MECHANIC:
                logger.setMechanicState(inMessage.getMechanicId(), inMessage.getMechanicState());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_OWN_CAR:
                logger.setCustCarDriven(inMessage.getCustomerId(), inMessage.getRepId(), inMessage.getSecondTime());
                outMessage = new Message(MessageType.STATUS_OK);
                break;  
            case UPDATE_PART_ORDERED:
                logger.setBoughtPart(inMessage.getPart());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_REPLACEMENT_CARS_PARKED_INCREMENT:
                logger.incrementNCars();
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_REPLACEMENT_CARS_PARKED_DECREMENT:
                logger.decrementNCars();
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_REQUIRES_REP_CAR:
                logger.setWaitingReplacement(inMessage.getCustomerId(), inMessage.isWantsRepCar());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_SERVICE_REQUESTED_BY_MANAGER:
                logger.repairCarSubmitted(inMessage.getCustomerId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_MANAGER_IS_ALERTED:
                logger.setFlagPartMissing(inMessage.getPartId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case UPDATE_PART_STOCK:
                logger.setReplenishPart(inMessage.getPart());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case SERVICE_END:
                logger.serviceEnd();
                outMessage = new Message(MessageType.STATUS_OK);
                break;
        }
        return outMessage;
    }
}