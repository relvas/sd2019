package logger;

/**
 *
 * @author Rui Dias
 * @author Rodrigo Oliveira
 * 
 */
import logger.Communication.ServerCom;
import logger.SharedRegion.Logger;
import logger.SharedRegion.LoggerProxy;
import logger.SharedRegion.ServiceProvider;

import static logger.SimulationParameters.*;

public class LoggerMain {
    /**
     * Used to check if the service must terminate.
     */
    public static boolean serviceEnd = false;


    public static void main(String[] args) {
        /**
         * Communication channels.
         */
        ServerCom scon, sconi;
        ServiceProvider serviceProvider;

        /**
         * Shared region and proxy initialization.
         */
        Logger logger = new Logger("log.txt");
        

        LoggerProxy loggerInt = new LoggerProxy(logger);

        /**
         * Start listening on the communication channel.
         */
        scon = new ServerCom(LOGGER_PORT);
        scon.start();

        /**
         * While the service is not terminated, accept connections and send them
         * to the service provider.
         */
        while(!serviceEnd) {
            sconi = scon.accept();            
            serviceProvider = new ServiceProvider(sconi, loggerInt);
            serviceProvider.start();
        }
        System.out.println("\033[0;31m Logger ended \033[0m");
    }
}
