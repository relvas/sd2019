package SupplierSite;
import SupplierSite.EntityStates.*;

import SupplierSite.Stubs.*;

/**
 * 
 * Shared region - Supplier Site
 */
public class SupplierSite implements iManagerSupplier{
    
    /**
     * General Information
     * 
     * @serialField logger
     */
    private Logger logger; 
 
    public SupplierSite(Logger repository){
        logger = repository;
    }
    
    /**
     * Manager go to Supplier Site
     * 
     * @param part Part to replace.
     * @return parts Number of parts of that type to store.
     */
    @Override
    public synchronized int goToSupplier(int part)
    {
       //((Manager) Thread.currentThread()).setEntityState(ManagerState.GNCA);
       logger.setBoughtPart(part);
       logger.setManagerState(ManagerState.GNCA); 
       
       return part;
    }
    
    public synchronized void serviceEnd(){       
        SupplierSiteMain.serviceEnd = true;      
    }
}
