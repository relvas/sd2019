package SupplierSite.Communication;

import SupplierSite.EntityStates.*;
import java.io.Serializable;
import SupplierSite.*;

public class Message implements Serializable{
    
    /**
     * Serial version of the class. Format used is
     * Class-Group-Number of project (CGGN)
     */
    private static final long serialVersionUID = 3072L;
    
    private MessageType type;
    private ManagerState managerState;
    private CustomerState customerState;
    private MechanicState mechanicState;
    private int customerId;
    private int mechanicId;
    private Action managerAction;
    private Situation managerSituation;
    private int key;
    private boolean nextTask;
    private boolean paymentReceived;
    private int repairInfo;
    private boolean wantsRepCar;
    private boolean parked;
    private boolean carIsReady;
    private boolean waitForKey; //wait for customer to return key
    private int carId;
    private int customerInfo;
    private int customerCarId;
    private int partNeeded;
    private int partId;
    private int repairingCarId;
    private int repairedCarId;
    private int partAvailable;
    private int carIdToRepair;
    private int numberOfParts;
    private int nParts;
    private int missingPartId;
    private boolean mechanicHasTask;
    private int ownCarId;
    private boolean isCarRepaired;
    private boolean isMissingPart;
    private boolean writeCheck;
    private boolean owncar;
    private boolean spendPart;
    private boolean wasWaitingPart;
    private int qsize;
    private boolean log;
    private int repId;
    private boolean secondTime;
    private int part;
    
    public Message(){
        customerId=-1;
        mechanicId=-1;
        managerAction = Action.ERROR;
        managerSituation=Situation.ERRO;
        key=-1;
        qsize=-1;
        repId=-1;
        part=-1;
        secondTime=false;
        log=false;
        parked=false;
        spendPart=false;
        wasWaitingPart=false;
        nextTask=false;
        paymentReceived=false;
        repairInfo=-1;
        wantsRepCar=false;
        carIsReady=false;
        waitForKey=false; //wait for customer to return key
        carId=-1;
        customerInfo=-1;
        customerCarId=-1;
        partNeeded=-1;
        partId=-1;
        repairingCarId=-1;
        repairedCarId=-1;
        partAvailable=-1;
        missingPartId=-1;
        mechanicHasTask=false;
        ownCarId=-1;
        isCarRepaired=false;
        isMissingPart=false;
        carIdToRepair=-1;
        numberOfParts=-1;
        writeCheck=false;
        nParts=-1;
    }
    
    public Message(MessageType mType){
        this();
        this.type = mType;
    }
    
    public Message(int mechanicId, Message mType){
        
    }
    
    public Message(MessageType mType, int value){
        this();
        this.type = mType;
        
        switch (mType){
            case LOUNGE_COLLECT_KEY:
                
            case UPDATE_PART_STOCK:
                this.part=value;
                
            case UPDATE_SERVICE_REQUESTED_BY_MANAGER:
                this.customerId=value; break;
                
            case UPDATE_PART_ORDERED:
                this.partId = value; break;
                
            case UPDATE_MANAGER_IS_ALERTED:
                this.partId = value;
                break;
            
            case UPDATE_CUSTOMERS_IN_QUEUE:
                 this.qsize=value; break;
                
            case LOUNGE_SET_FLAGRESTOCKED:
                this.partId = value; break;    
                
            case REPAIR_STORE_PART:
                this.partId = value; break;
              
            case LOUNGE_HAND_CAR_KEY:
                this.customerId = value; break;    
              
            case OUTSIDEWORLD_BACK_TO_WORK_BY_BUS:
                this.customerId = value; break;
                
            case LOUNGE_PAY_FOR_SERVICE:
                this.customerId = value; break;
                
            case PARK_COLLECT_CAR:
                this.customerId = value; break;
                
            case PARK_GO_TO_REPAIRSHOP:
                this.customerId = value; break;
     
            case RETURN_KEY:
                this.key = value; break;
                
            case PARK_GET_VEHICLE:
            case PARK_RETURN_VEHICLE:
            case RETURN_MECHANIC_GET_CAR:
            case RETURN_COLLECTED_CAR:
            case RETURN_CLIENT_FIND_CAR:
                this.carId = value; break;
                
            case REPAIR_GET_REQUIRED_PART:
            case RETURN_REPAIR_INFO:
                this.repairInfo = value; break;
            
            case REPAIR_REGISTER_SERVICE:
                this.customerId = value;break;
            
            case REPAIR_RESUME_REPAIR_PROCEDURE:
                this.partId = value;break;
            
            case REPAIR_START_REPAIR_PROCEDURE:
            case REPAIR_READ_THE_PAPER:
                this.mechanicId = value; break;
            
            case OUTSIDEWORLD_PHONE_CUSTOMER:
                this.customerId = value; break;
            
            case SUPPLIERSITE_GO_TO_SUPPLIER:
                this.missingPartId = value; break;
            
            case RETURN_CUSTOMER_INFO:
                this.customerInfo = value; break;
                
            case RETURN_PART_NEEDED:
                this.partNeeded = value; break; // todo
           
            case RETURN_PART_AVAILABLE:
                this.partAvailable = value; break;
            
            case RETURN_MISSING_PART:
                this.numberOfParts = value; break;
        }
    }
    
    public Message(MessageType mType, Action action){
         this();
         this.type = mType;
         // case RETURN_ACTION:
         this.managerAction = action;
    }
    
    public Message(MessageType mType, Situation situation){
         this();
         this.type = mType;
         // case RETURN_SITUATION:
         this.managerSituation = situation;
    }
    
    public Message(MessageType mType, boolean value){
        this();
        this.type = mType;
        
        switch(mType){
            
            case RETURN_NEXT_TASK:
                this.nextTask = value; break;
            
            case RETURN_WAIT_FOR_KEY:
                this.waitForKey = value; break;
             
           
            case UPDATE_CUSTOMERS_WAITING_FOR_REP_CAR_INCREMENT:
                this.log=value; break;
            case UPDATE_CUSTOMERS_WAITING_FOR_REP_CAR_DECREMENT:
                this.log=value; break;
            case UPDATE_CARS_REPAIRED:
                this.parked=value; break;
            case UPDATE_REPLACEMENT_CARS_PARKED:
            
                this.writeCheck = value; break;
            
            case RETURN_MECHANIC_TASK:
                this.mechanicHasTask = value; break;
            
        }
    }
    
    public Message(MessageType mType, int a, boolean b){
        this();
        this.type = mType;
        
        switch(mType){
            case LOUNGE_TALK_WITH_MANAGER:
                this.customerId=a;
                this.wantsRepCar=b;
                break;
            case UPDATE_REQUIRES_REP_CAR:
                this.wantsRepCar = b;
                this.customerId = a;
                break;
            default:
                this.customerId=a;
                this.owncar=b;   
        }
        
    }
    
    public Message(MessageType mType, int a, int b){
        this();
        this.type = mType;
        
        switch(mType){
            case LOUNGE_LET_MANAGER_KNOW:
                this.mechanicId = a;
                this.repairInfo = b;
                break;
            
            case LOUNGE_REPAIR_CONCLUDED:
                this.mechanicId = a;
                this.repairingCarId = b;
                break;
            
            /*case PARK_GO_TO_REPAIRSHOP:
                this.customerId = a;
                this.customerCarId = b;
                break;*/
             
            case REPAIR_PART_AVAILABLE:
                this.partId = a;
                this.repairingCarId = b;
                break;
            
            /*case REPAIR_STORE_PART:
                this.numberOfParts = a;
                this.repairInfo = b;
                break;*/
              
            /*case OUTSIDEWORLD_BACK_TO_WORK_BY_CAR:
                this.customerId = a;
                this.carId = b;
                break;*/
                
            
            
            /*case UPDATE_PART_ORDERED:
                this.partId = a;
                this.nParts = b; //check
                break;*/
              
            case PARK_FIND_CAR:
                this.customerId = a;
                this.key = b;
                break;
            
            
        }
  
    }
    
    public Message(MessageType mType, int a, int b, boolean c){
        this();
        this.type=mType;
        switch(mType){
            case UPDATE_OWN_CAR:
                this.repId=b;
                this.customerId=a;
                this.secondTime=c;
        
        }
    }
    
    public Message(MessageType mType, boolean a, int b, boolean c){
        this();
        this.type=mType;
        this.spendPart=a;
        this.partId=b;
        this.wasWaitingPart=c;
        
    }
    
    public Message(MessageType mType, boolean a, boolean b, int value){
         this();
         this.type = mType;
         
         //switch(mType){
             
         //}
         
         //LOUNGE_QUEUE_IN
         this.carIsReady = a;
         this.wantsRepCar = b;
         this.customerId = value;
         
    }
    
    public Message(MessageType mType, boolean a, int value){
        this();
        this.type = mType;
        
        switch(mType){
            case RETURN_WANT_REP_CAR:
            case LOUNGE_TALK_WITH_MANAGER:
            
            
            case UPDATE_REPAIR_STATE:
                this.isCarRepaired = a;
                this.customerId = value;
                break;
             
            case UPDATE_CUSTOMER_VEHICLES_WAITING_FOR_PART:
                this.writeCheck = a;
                this.partId = value;
                break;
            
            
        }
        
    }
    
    public Message(MessageType mType, ManagerState state){
        this();
        this.type = mType;
        //UPDATE_MANAGER_STATE
        this.managerState = state;
    }
    public Message(MessageType mType, CustomerState state){
        this();
        this.type = mType;
        //UPDATE_CUSTOMER_STATE
        this.customerState = state;
    }
    public Message(MessageType mType, MechanicState state){
        this();
        this.type = mType;
        //UPDATE_MECHANIC_STATE
        this.mechanicState = state;
    }
    
    
    public Message(MessageType mType, int id, CustomerState state)
    {
        this();
        this.type = mType;
        //UPDATE_CUSTOMER_STATE
        this.customerState = state;
        this.customerId = id;
        
    }
    public Message(MessageType mType, int id, MechanicState state)
    {
        this();
        this.type = mType;
        //UPDATE_MECHANIC_STATE
        this.mechanicState = state;
        this.mechanicId = id;
    }

    
    //Getters auto generated
    public MessageType getType() {
        return type;
    }
    
    public int getQSize(){
        return qsize;
    }
    
    public int getRepId(){
        return repId;
    }
    
    public boolean getSecondTime(){
        return secondTime;
    }
    
    public boolean getLog(){
        return log;
    }
    
    public int getPart(){
        return part;
    }
    
    public boolean getSpendPart(){
        return spendPart;
    }
    
    public boolean getWasWaitingPart(){
        return wasWaitingPart;
    }

    public ManagerState getManagerState() {
        return managerState;
    }

    public CustomerState getCustomerState() {
        return customerState;
    }

    public MechanicState getMechanicState() {
        return mechanicState;
    }

    public int getCustomerId() {
        return customerId;
    }

    public int getMechanicId() {
        return mechanicId;
    }

    public Action getManagerAction() {
        return managerAction;
    }
    
    public Situation getManagerSituation(){
        return managerSituation;
    }

    public int getKey() {
        return key;
    }

    public boolean isNextTask() {
        return nextTask;
    }

    public boolean isPaymentReceived() {
        return paymentReceived;
    }

    public int getRepairInfo() {
        return repairInfo;
    }

    public boolean isParked(){
        return parked;
    }
    
    public boolean isWantsRepCar() {
        return wantsRepCar;
    }

    public boolean isCarIsReady() {
        return carIsReady;
    }

    public boolean isWaitForKey() {
        return waitForKey;
    }

    public int getCarId() {
        return carId;
    }

    public int getCustomerInfo() {
        return customerInfo;
    }

    public int getCustomerCarId() {
        return customerCarId;
    }

    public int getPartNeeded() {
        return partNeeded;
    }

    public int getPartId() {
        return partId;
    }

    public int getRepairingCarId() {
        return repairingCarId;
    }

    public int getRepairedCarId() {
        return repairedCarId;
    }

    public int getPartAvailable() {
        return partAvailable;
    }

    public int getCarIdToRepair() {
        return carIdToRepair;
    }

    public int getNumberOfParts() {
        return numberOfParts;
    }

    public int getnParts() {
        return nParts;
    }

    public int getMissingPartId() {
        return missingPartId;
    }

    public boolean isMechanicHasTask() {
        return mechanicHasTask;
    }

    public int getOwnCarId() {
        return ownCarId;
    }

    public boolean isIsCarRepaired() {
        return isCarRepaired;
    }

    public boolean isIsMissingPart() {
        return isMissingPart;
    }

    public boolean isWriteCheck() {
        return writeCheck;
    }
    
}
