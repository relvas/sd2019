package SupplierSite;

import SupplierSite.Communication.*;
import SupplierSite.SupplierSite;
import SupplierSite.SupplierSiteProxy;
import SupplierSite.ServiceProvider;
import SupplierSite.Stubs.*;

import static SupplierSite.SimulationParameters.*;

public class SupplierSiteMain {
    /**
     * Used to check if the service must terminate.
     */
    public static boolean serviceEnd = false;

    public static void main(String[] args) {
        /**
         * Communication channels.
         */
        ServerCom scon, sconi;
        ServiceProvider serviceProvider;

        /**
         * Stub initialization.
         */
        Logger logger = new Logger(LOGGER_HOSTNAME, LOGGER_PORT);

        /**
         * Shared region and proxy initialization.
         */
        SupplierSite supplierSite = new SupplierSite(logger);
        SupplierSiteProxy supplierSiteInt = new SupplierSiteProxy(supplierSite);

        /**
         * Start listening on the communication channel.
         */
        scon = new ServerCom(SUPPLIER_PORT);
        scon.start();

        /**
         * While the service is not terminated, accept connections and send them
         * to the service provider.
         */
        while(!serviceEnd) {
            sconi = scon.accept();
            serviceProvider = new ServiceProvider(sconi, supplierSiteInt);
            serviceProvider.start();
        }        
        System.out.println("Supplier Site Finished");
    }
}
