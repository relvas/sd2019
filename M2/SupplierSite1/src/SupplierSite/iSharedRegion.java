package SupplierSite;

import SupplierSite.Communication.*;

public interface iSharedRegion {
    public Message processAndReply(Message input, ServerCom scom);
}
