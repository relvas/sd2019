package SupplierSite;

import SupplierSite.Communication.*;

public class SupplierSiteProxy implements iSharedRegion {

    /**
     * SupplierSite used to process the messages.
     */
    private final SupplierSite supplierSite;

    /**
     * Supplier Site Proxy constructor.
     * @param supplierSite supplierSite to process the messages
     */
    public SupplierSiteProxy(SupplierSite supplierSite) {
        this.supplierSite = supplierSite;
    }

    /**
     * Process and reply a message.
     * @param inMessage message received
     * @param scon communication channel
     * @return message to be replied
     */
    @Override
    public Message processAndReply(Message inMessage, ServerCom scon) {
        Message outMessage = null;

        switch(inMessage.getType()) {
            case SUPPLIERSITE_GO_TO_SUPPLIER:
                outMessage = new Message(MessageType.RETURN_MISSING_PART, supplierSite.goToSupplier(inMessage.getMissingPartId()));
                break;
            case SERVICE_END:
                supplierSite.serviceEnd();
                outMessage = new Message(MessageType.STATUS_OK);
                break;
        }
        return outMessage;
    }

}
