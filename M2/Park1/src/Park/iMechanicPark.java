package Park;

/**
 *  Interface - Mechanic Park
 */

public interface iMechanicPark {
    
    /**
     * Mechanic goes to the park to get the car and verifies which part needs to be replaced  
     * 
     * @param mechanicID Identifies the Customer car.
     * @param customerID Identifies the Mechanic.
     * @return Returns the part to be replaced.
     */
    public int getVehicle(int mechanicID, int customerID);
    
    /**
     * The Mechanic returns the Customer car repared to the Park.  
     * 
     * @param customerID Identifies the Customer car.
     */
    public void returnVehicle(int customerID);
    
}
