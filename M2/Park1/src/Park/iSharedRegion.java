package Park;

import Park.Communication.*;

public interface iSharedRegion {
    public Message processAndReply(Message input, ServerCom scom);
}
