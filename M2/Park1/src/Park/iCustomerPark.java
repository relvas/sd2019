package Park;

/**
 *  Interface - Customer Park
 */

public interface iCustomerPark {
    
    /**
     * Customer go to the repair shop and park the car - his own or the replace car.
     * 
     * @param customerID Identifies the Customer car. 
     */
    public void goToRepairShop(int customerID);
    
    /**
     * Customer finds the replace car.
     * 
     * @param customerID Identifies the Customer car.
     * @param key Identifies the replace car.
     */
    public void findCar(int customerID,int key);
    
    /**
     * Customer removes his repaired car from the Park. 
     * 
     * @param customerID Identifies the Customer car. 
     */
    public void collectCar(int customerID);
    
}
