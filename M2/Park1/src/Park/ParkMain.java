package Park;

import Park.Communication.*;
import Park.Park;
import Park.ParkProxy;
import Park.ServiceProvider;
import Park.Stubs.*;

import static Park.SimulationParameters.*;

public class ParkMain {
    /**
     * Used to check if the service must terminate.
     */
    public static boolean serviceEnd = false;

    public static void main(String[] args) {
        /**
         * Communication channels.
         */
        ServerCom scon, sconi;
        ServiceProvider serviceProvider;

        /**
         * Stub initialization.
         */
        Logger logger = new Logger(LOGGER_HOSTNAME, LOGGER_PORT);

        /**
         * Shared region and proxy initialization.
         */
        Park park = new Park(logger);
        ParkProxy parkInt = new ParkProxy(park);

        /**
         * Start listening on the communication channel.
         */
        scon = new ServerCom(PARK_PORT);
        scon.start();

        /**
         * While the service is not terminated, accept connections and send them
         * to the service provider.
         */
        while(!serviceEnd) {
            sconi = scon.accept();
            serviceProvider = new ServiceProvider(sconi, parkInt);
            serviceProvider.start();
        }
        System.out.println("Park Finished");        
    }
}

