package Park;

import Park.Communication.*;

/**
 * Park proxy for the logger shared region.
 */
public class ParkProxy implements iSharedRegion {

    /**
     * Park used to process the messages.
     */
    private final Park park;

    /**
     * Park Proxy constructor.
     * @param park park to process the messages
     */
    public ParkProxy(Park park){
        this.park = park;
    }

    /**
     * Process and reply a message.
     * @param inMessage message received
     * @param scon communication channel
     * @return message to be replied
     */
    @Override
    public Message processAndReply(Message inMessage, ServerCom scon){
        Message outMessage = null;

        switch(inMessage.getType()) {
            case PARK_COLLECT_CAR:
                park.collectCar(inMessage.getCustomerId());
                outMessage = new Message(MessageType.RETURN_COLLECTED_CAR);
                break;
            case PARK_FIND_CAR:
                park.findCar(inMessage.getCustomerId(), inMessage.getKey());
                outMessage = new Message(MessageType.RETURN_CLIENT_FIND_CAR);
                break;
            case PARK_GET_VEHICLE:
                park.getVehicle(inMessage.getMechanicId(), inMessage.getCustomerId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case PARK_GO_TO_REPAIRSHOP:
                park.goToRepairShop(inMessage.getCustomerId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case PARK_RETURN_VEHICLE:
                park.returnVehicle(inMessage.getCustomerId());
                outMessage = new Message(MessageType.STATUS_OK);
                break;
            case SERVICE_END:
                park.serviceEnd();
                outMessage = new Message(MessageType.STATUS_OK);
                break;
        }
        return outMessage;
    }

}
