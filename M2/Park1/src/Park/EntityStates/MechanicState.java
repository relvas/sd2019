package Park.EntityStates;

/**
 * Contains all states from de Mechanic entity
 */

public enum MechanicState {
    /**
    *   Waiting For Work
    */
    WAIT,
    
    /**
    *   Fixing The Car
    */
    FIXI,
    
    /**
    *   Checking Stock
    */
    CHEC,
    
    /**
    *   Allert Manager
    */
    ALLM
}
