package Mechanics.Communication;

import Mechanics.EntityStates.*;
import java.io.Serializable;
import Mechanics.Action;
import Mechanics.Situation;

public class Message implements Serializable{
    
    /**
     * Serial version of the class. Format used is
     * Class-Group-Number of project (CGGN)
     */
    private static final long serialVersionUID = 3072L;
    
    private MessageType type;
    private ManagerState managerState;
    private CustomerState customerState;
    private MechanicState mechanicState;
    private int customerId;
    private int mechanicId;
    private Action managerAction;
    private int key;
    private boolean nextTask;
    private boolean paymentReceived;
    
    /**
     * part from let manager know
     */
    private int repairInfo; //
    
    private boolean wantsRepCar;
    private boolean carIsReady;
    private boolean waitForKey; //wait for customer to return key
    private int carId;
    private int customerInfo;
    private int customerCarId;
    private int partNeeded;
    private int partId;
    
    /**
     * car id from lounge repair concluded
     */
    private int repairingCarId;
    private int repairedCarId;
    private int partAvailable;
    private int carIdToRepair;
    private int numberOfParts;
    private int nParts;
    private int missingPartId;
    private boolean mechanicHasTask;
    private int ownCarId;
    private boolean isCarRepaired;
    private boolean isMissingPart;
    private boolean writeCheck;
    private boolean owncar;
    
    private int repairingAux;
    
    public Message(){
        customerId=-1;
        mechanicId=-1;
        managerAction = Action.ERROR;
        key=-1;
        nextTask=false;
        paymentReceived=false;
        repairInfo=-1;
        wantsRepCar=false;
        carIsReady=false;
        waitForKey=false; //wait for customer to return key
        carId=-1;
        customerInfo=-1;
        customerCarId=-1;
        partNeeded=-1;
        partId=-1;
        repairingCarId=-1;
        repairedCarId=-1;
        partAvailable=-1;
        missingPartId=-1;
        mechanicHasTask=false;
        ownCarId=-1;
        isCarRepaired=false;
        isMissingPart=false;
        carIdToRepair=-1;
        numberOfParts=-1;
        writeCheck=false;
        nParts=-1;
        repairingAux =-1;
    }
    
    public Message(MessageType mType){
        this();
        this.type = mType;
    }
    
    public Message(MessageType mType, int value){
        this();
        this.type = mType;
        
        switch (mType){
            case LOUNGE_COLLECT_KEY:
            case OUTSIDEWORLD_BACK_TO_WORK_BY_BUS:
                this.customerId = value; break;
                
            case LOUNGE_PAY_FOR_SERVICE:
                this.customerId = value; break;
                
            case PARK_COLLECT_CAR:
                this.customerId = value; break;
            
            case PARK_RETURN_VEHICLE:
            case PARK_GO_TO_REPAIRSHOP:
                this.customerId = value; break;
     
            case RETURN_KEY:
                this.key = value; break;
                
            
            
            case RETURN_MECHANIC_GET_CAR:
            case RETURN_COLLECTED_CAR:
            case RETURN_CLIENT_FIND_CAR:
                this.carId = value; break;
                
            
            case RETURN_REPAIR_INFO:
                this.repairInfo = value; break;
            
            case REPAIR_REGISTER_SERVICE:
                this.carIdToRepair = value;break;
            
            
            
            case REPAIR_START_REPAIR_PROCEDURE:
            case REPAIR_READ_THE_PAPER:
                this.mechanicId = value; break;
            
            case OUTSIDEWORLD_PHONE_CUSTOMER:
                this.repairedCarId = value; break;
            
            case SUPPLIERSITE_GO_TO_SUPPLIER:
                this.missingPartId = value; break;
            
            case RETURN_CUSTOMER_INFO:
                this.customerInfo = value; break;
                
            case RETURN_PART_NEEDED:
                this.partNeeded = value; break; // todo
           
            case RETURN_PART_AVAILABLE:
                this.partAvailable = value; break;
            
            case RETURN_MISSING_PART:
                this.numberOfParts = value; break;
        }
    }
    
    public Message(MessageType mType, Action action){
         this();
         this.type = mType;
         // case RETURN_SITUATION:
         this.managerAction = action;
    }
    
    
    public Message(MessageType mType, boolean value){
        this();
        this.type = mType;
        
        switch(mType){
            case LOUNGE_HAND_CAR_KEY:
                this.paymentReceived = value; break;
            case RETURN_NEXT_TASK:
                this.nextTask = value; break;
            
            case RETURN_WAIT_FOR_KEY:
                this.waitForKey = value; break;
             
            case UPDATE_CUSTOMERS_IN_QUEUE:
            case UPDATE_CUSTOMERS_WAITING_FOR_REP_CAR:
            case UPDATE_CARS_REPAIRED:
            case UPDATE_CUSTOMER_CARS_PARKED:
            case UPDATE_REPLACEMENT_CARS_PARKED:
            case UPDATE_SERVICE_REQUESTED_BY_MANAGER:
                this.writeCheck = value; break;
            
            case RETURN_MECHANIC_TASK:
                this.mechanicHasTask = value; break;
            
        }
    }
    
    public Message(MessageType mType, int a, boolean b){
        this();
        this.type = mType;
        
        switch(mType){
            case LOUNGE_TALK_WITH_MANAGER:
                this.customerId=a;
                this.wantsRepCar=b;
                break;
            default:
                this.customerId=a;
                this.owncar=b;   
        }
        
    }
    
    public Message(MessageType mType, int a, int b){
        this();
        this.type = mType;
        
        
        
        switch(mType){
            
            case LOUNGE_LET_MANAGER_KNOW:
                this.mechanicId = a;
                this.repairInfo = b;
                break;
            
            case LOUNGE_REPAIR_CONCLUDED:
                this.mechanicId = a;
                this.repairingCarId = b;
                break;
            
            /*case PARK_GO_TO_REPAIRSHOP:
                this.customerId = a;
                this.customerCarId = b;
                break;*/
             
            case REPAIR_PART_AVAILABLE:
                this.partId = b;
                this.repairingCarId = a;
                this.customerId = a;
                break;
            
            case REPAIR_STORE_PART:
                this.numberOfParts = a;
                this.repairInfo = b;
                break;
              
            /*case OUTSIDEWORLD_BACK_TO_WORK_BY_CAR:
                this.customerId = a;
                this.carId = b;
                break;*/
                
            case UPDATE_OWN_CAR:
                this.customerId = a;
                this.ownCarId = b;
                break;
            
            case UPDATE_PART_ORDERED:
                this.partId = a;
                this.nParts = b; //check
                break;
              
            case PARK_FIND_CAR:
                this.customerId = a;
                this.key = b;
                break;
            
            case PARK_GET_VEHICLE:
                this.mechanicId = a;
                this.customerId = b;

            
        }
  
    }
    
    
    public Message(MessageType mType, boolean a, boolean b, int value){
         this();
         this.type = mType;
         
         //switch(mType){
             
         //}
         
         //LOUNGE_QUEUE_IN
         this.carIsReady = a;
         this.wantsRepCar = b;
         this.customerId = value;
         
    }
    
    public Message(MessageType mType, boolean a, int value){
        this();
        this.type = mType;
        
        switch(mType){
            case RETURN_WANT_REP_CAR:
            case LOUNGE_TALK_WITH_MANAGER:
            case UPDATE_REQUIRES_REP_CAR:
                this.wantsRepCar = a;
                this.customerId = value;
                break;
            
            case UPDATE_REPAIR_STATE:
                this.isCarRepaired = a;
                this.customerId = value;
                break;
            
            case UPDATE_MANAGER_IS_ALERTED:
                this.isMissingPart = a;
                this.partId = value;
                break;
             
            case UPDATE_CUSTOMER_VEHICLES_WAITING_FOR_PART:
                this.writeCheck = a;
                this.partId = value;
                break;
            
            
        }
        
    }
    public Message(MessageType mType, int a, int b, int c){
        this();
        this.type=mType;
        switch(mType){
            case REPAIR_FIX_IT:
                this.mechanicId=a;
                this.customerId=b;
                this.partId=c;
                break;
        
        
            case REPAIR_GET_REQUIRED_PART:
                this.mechanicId=a;
                this.customerId=b;
                this.partId=c;
                break;
                
            case REPAIR_RESUME_REPAIR_PROCEDURE:
                this.mechanicId = a;
                this.customerId = b;
                this.partId = c;
                break;
        }
        
    }
    
    public Message(MessageType mType, ManagerState state){
        this();
        this.type = mType;
        //UPDATE_MANAGER_STATE
        this.managerState = state;
    }
    public Message(MessageType mType, CustomerState state){
        this();
        this.type = mType;
        //UPDATE_CUSTOMER_STATE
        this.customerState = state;
    }
    public Message(MessageType mType, MechanicState state){
        this();
        this.type = mType;
        //UPDATE_MECHANIC_STATE
        this.mechanicState = state;
    }
    
    
    public Message(MessageType mType, int id, CustomerState state)
    {
        this();
        this.type = mType;
        //UPDATE_CUSTOMER_STATE
        this.customerState = state;
        this.customerId = id;
        
    }
    public Message(MessageType mType, int id, MechanicState state)
    {
        this();
        this.type = mType;
        //UPDATE_MECHANIC_STATE
        this.mechanicState = state;
        this.mechanicId = id;
    }

    
    //Getters auto generated
    public int getRepairingAux(){
        return repairingAux;
    }
    public MessageType getType() {
        return type;
    }

    public ManagerState getManagerState() {
        return managerState;
    }

    public CustomerState getCustomerState() {
        return customerState;
    }

    public MechanicState getMechanicState() {
        return mechanicState;
    }

    public int getCustomerId() {
        return customerId;
    }

    public int getMechanicId() {
        return mechanicId;
    }

    public Action getManagerAction() {
        return managerAction;
    }

    public int getKey() {
        return key;
    }

    public boolean isNextTask() {
        return nextTask;
    }

    public boolean isPaymentReceived() {
        return paymentReceived;
    }

    public int getRepairInfo() {
        return repairInfo;
    }

    public boolean isWantsRepCar() {
        return wantsRepCar;
    }

    public boolean isCarIsReady() {
        return carIsReady;
    }

    public boolean isWaitForKey() {
        return waitForKey;
    }

    public int getCarId() {
        return carId;
    }

    public int getCustomerInfo() {
        return customerInfo;
    }

    public int getCustomerCarId() {
        return customerCarId;
    }

    public int getPartNeeded() {
        return partNeeded;
    }

    public int getPartId() {
        return partId;
    }

    public int getRepairingCarId() {
        return repairingCarId;
    }

    public int getRepairedCarId() {
        return repairedCarId;
    }

    public int getPartAvailable() {
        return partAvailable;
    }

    public int getCarIdToRepair() {
        return carIdToRepair;
    }

    public int getNumberOfParts() {
        return numberOfParts;
    }

    public int getnParts() {
        return nParts;
    }

    public int getMissingPartId() {
        return missingPartId;
    }

    public boolean isMechanicHasTask() {
        return mechanicHasTask;
    }

    public int getOwnCarId() {
        return ownCarId;
    }

    public boolean isIsCarRepaired() {
        return isCarRepaired;
    }

    public boolean isIsMissingPart() {
        return isMissingPart;
    }

    public boolean isWriteCheck() {
        return writeCheck;
    }
    
}
