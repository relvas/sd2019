package Mechanics.Stubs;

import Mechanics.Communication.*;


public class RepairArea {
    private String serverHostName = null;
    private int serverPortNumb;
    
    public RepairArea(String hostName, int port){
        this.serverHostName = hostName;
        this.serverPortNumb = port;
    }
    public boolean readThePaper( int mechanicID ) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.REPAIR_READ_THE_PAPER, mechanicID);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
        
        return inMessage.isMechanicHasTask();
    }

    /**
     * Mechanic starts repairing the car
     * @param mechanicID Identification of the Mechanic
     * @return Information of the repair
     */
    public int startRepairProcedure( int mechanicID ) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.REPAIR_START_REPAIR_PROCEDURE, mechanicID);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();

        com.close();
        
        return inMessage.getRepairingAux();
    }

    
    /**
     * After going to the stock and picking the needed part, the Mechanic continues to repair a car 
     * @param mechanicID Identification of the mechanic
     * @param customerID Identification of the customer
     * @param pID Identification of the part used
     */
    public void resumeRepairProcedure( int mechanicID, int customerID, int pID) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.REPAIR_RESUME_REPAIR_PROCEDURE, mechanicID, customerID, pID);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();
    }

    /**
     * Mechanic is fixing a car for some random time
     * @param mechanicID Identification of the mechanic
     * @param customerID Identification of the customer
     * @param pID Identification of the part used
     */
    public void fixIt(int mechanicID, int customerID, int pID) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.REPAIR_FIX_IT, mechanicID, customerID, pID);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();
    }

    /**
     * Mechanic checks if the part he needs is available
     * @param customerID Identification of the part
     * @param partID Identification of the part
     * @return true if the part is in stock
     */
    public int partAvailable( int customerID, int partID  ) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.REPAIR_PART_AVAILABLE, customerID, partID);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();
    
        return inMessage.getPartAvailable();
    }

    /**
     * Mechanic goes to check if the part he needs is available in stock
     * @param mechanicId Identification of the mechanic
     * @param customerId Identification of the customer
     * @param pId Identification of the part used
     * @return Identification of the part Mechanic needs
     */
    public int getRequiredPart(int mechanicId, int customerId, int pId) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.REPAIR_GET_REQUIRED_PART, mechanicId, customerId, pId);
        com.writeObject(msg);
        Message inMessage = (Message) com.readObject();
        com.close();

        return inMessage.getPartNeeded();
    }
     
    /**
     * Terminate service
     */
    public void serviceEnd() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.SERVICE_END);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();  
    }
}
