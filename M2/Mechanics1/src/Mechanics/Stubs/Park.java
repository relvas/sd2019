package Mechanics.Stubs;

import Mechanics.Communication.*;

public class Park {
    private String serverHostName = null;
    private int serverPortNumb;
    
    public Park(String hostName, int port){
        this.serverHostName = hostName;
        this.serverPortNumb = port;
    }
    
    public int getVehicle(int mechanicID, int customerID) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.PARK_GET_VEHICLE, mechanicID, customerID);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        
        
        com.close();
        return inMessage.getPartId();
    }

    /**
     * Mechanic goes to the park to place the repaired car
     * @param customerID Identification customer
     */
    public void returnVehicle( int customerID ) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.PARK_RETURN_VEHICLE, customerID);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();
    }
    
    /**
     * Terminate service
     */
    public void serviceEnd() {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.SERVICE_END);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();  
    }
}
