
package Mechanics.Stubs;

import Mechanics.Communication.*;

public class Lounge {
    private String serverHostName = null;
    private int serverPortNumb;
    
    public Lounge(String hostName, int port){
        this.serverHostName = hostName;
        this.serverPortNumb = port;
    }
    
    
    public void letManagerKnow(int mechanicID, int part) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_LET_MANAGER_KNOW, mechanicID, part);
        com.writeObject(msg);
        
        Message inMessage = (Message) com.readObject();
        com.close();
    }
    
    
    public void repairConcluded(int mechanicID, int carID) {
        ClientCom com = new ClientCom (serverHostName, serverPortNumb);

        while(!com.open()) {
            try {
                Thread.currentThread ().sleep ((long) (10));
            } catch (InterruptedException ex) { }
        }

        Message msg = new Message(MessageType.LOUNGE_REPAIR_CONCLUDED, mechanicID, carID);
        com.writeObject(msg);

        Message inMessage = (Message) com.readObject();
        com.close();
    }
    
}
