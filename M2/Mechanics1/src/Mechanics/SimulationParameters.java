package Mechanics;

/**
 * Serves as a way of providing global variables to all the Entities and Shared
 * Regions in the problem.
 * 
 */ 

public class SimulationParameters {
    
     /**
     * Logger server hostname.
     */
    public final static String LOGGER_HOSTNAME = "localhost";

    /**
     * Logger server port.
     */
    public final static int LOGGER_PORT = 22471;

    /**
     * Lounge server hostname.
     */
    public final static String LOUNGE_HOSTNAME = "localhost";

    /**
     * Lounge server port.
     */
    public final static int LOUNGE_PORT = 22472;

    /**
     * Outside World server hostname.
     */
    public final static String OUTSIDE_HOSTNAME = "localhost";

    /**
     * Outside World server port.
     */
    public final static int OUTSIDE_PORT = 22473;

    /**
     * Park server hostname.
     */
    public final static String PARK_HOSTNAME = "localhost";

    /**
     * Park server port.
     */
    public final static int PARK_PORT = 22474;

    /**
     * Repair Area server hostname.
     */
    public final static String REPAIR_HOSTNAME = "localhost";

    /**
     * Repair Area server port.
     */
    public final static int REPAIR_PORT = 22475;

    /**
     * Supplier Site server hostname.
     */
    public final static String SUPPLIER_HOSTNAME = "localhost";

    /**
     * Supplier Site server port.
     */
    public final static int SUPPLIER_PORT = 22476;
    
    
    /**
     * Number of existing Mechanics.
     */
    public static final int N_MECHANICS = 2;
    
    /**
     * Number of customers.
     */
    public static final int N_CUSTOMERS = 30;
    
    
    /**
     * Number of existing replacements cars. 
     */
    public static final int N_REPLACEMENT_CARS = 3;
    
    
    /**
     * Number of different parts.
     */
    public static final int N_DIFF_PARTS = 3;
    
    /**
     * Number of exising parts stored.
     */
    public static final int N_PARTS = 5;
    
}
