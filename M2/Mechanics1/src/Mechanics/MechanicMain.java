package Mechanics;

import Mechanics.Stubs.*;
import static Mechanics.SimulationParameters.*;

public class MechanicMain {

    public static void main(String[] args) {
        Lounge lounge = new Lounge(LOUNGE_HOSTNAME, LOUNGE_PORT);
        RepairArea repairArea = new RepairArea(REPAIR_HOSTNAME, REPAIR_PORT);
        Park park = new Park(PARK_HOSTNAME, PARK_PORT);
        
        Mechanic[] mechanics = new Mechanic[N_MECHANICS];
        
        /**
         * Initializing Mechanics
         */
        
        for (int i = 0; i < mechanics.length; i++) 
            mechanics[i] = new Mechanic(i, park, lounge, repairArea);
    
        /**
         * Starting Thread
         */
        for (Mechanic m : mechanics) 
            m.start();
        
        
        for (Mechanic m : mechanics){
     
            try {
                m.join();
            } catch (InterruptedException ex) { }
        }
        
        /**
         *  Mechanics Finished
         */
        System.out.println("Mechanics finished");    
        park.serviceEnd();
        repairArea.serviceEnd();
    }
   

    
}
