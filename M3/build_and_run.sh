bold=$(tput bold)
normal=$(tput sgr0)
echo "${bold}*** Script de Deployment ***${normal}"

export SSHPASS='3prud3nm'

###

echo -e "\n${bold}* Cópia do código a executar em cada nó *${normal}"


echo -e "\n${bold}->${normal} A mover Registry e Logger para a máquina ${bold}1${normal}"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws01.ua.pt << !
    put -r RMI
    put -r Logger
    bye
!

echo -e "\n${bold}->${normal} A mover Lounge para a máquina ${bold}2${normal}"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws02.ua.pt << !
    put -r Lounge
    bye
!

echo -e "\n${bold}->${normal} A mover Outside World para a máquina ${bold}3${normal}"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws03.ua.pt << !
    put -r OutsideWorld
    bye
!

echo -e "\n${bold}->${normal} A mover Park para a máquina ${bold}4${normal}"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws04.ua.pt << !
    put -r Park
    bye
!

echo -e "\n${bold}->${normal} A mover Repair Area para a máquina ${bold}5${normal}"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws05.ua.pt << !
    put -r RepairArea
    bye
!

echo -e "\n${bold}->${normal} A mover Supplier Site para a máquina ${bold}6${normal}"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws06.ua.pt << !
    put -r SupplierSite
    ls SupplierSite/src/Interfaces
    bye
!

echo -e "\n${bold}->${normal} A mover Customers para a máquina ${bold}7${normal}"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws07.ua.pt << !
    put -r Customer
    bye
!

echo -e "\n${bold}->${normal} A mover Mechanics para a máquina ${bold}8${normal}"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws08.ua.pt << !
    put -r Mechanic
    bye
!

echo -e "\n${bold}->${normal} A mover Manager para a máquina ${bold}9${normal}"
sshpass -e sftp -o StrictHostKeyChecking=no sd0407@l040101-ws09.ua.pt << !
    put -r Manager
    bye
!



###

echo -e "\n${bold}* Compilação do código em cada nó *${normal}"


echo -e "\n${bold}->${normal} A compilar Registry e Logger na máquina ${bold}1${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws01.ua.pt << EOF

    javac RMI/src/Interfaces/*.java RMI/src/MainPackage/*.java RMI/src/EntitiesStates/*.java

    mv RMI/src/Interfaces/*.class RMI/src/target/Interfaces/
    mv RMI/src/MainPackage/*.class RMI/src/target/MainPackage/
    mv RMI/src/EntitiesStates/*.class RMI/src/target/EntitiesStates/
    
    
    javac Logger/src/Interfaces/*.java Logger/src/MainPackage/*.java Logger/src/EntitiesStates/*.java

    mv Logger/src/Interfaces/*.class Logger/src/target/Interfaces/
    mv Logger/src/MainPackage/*.class Logger/src/target/MainPackage/
    mv Logger/src/EntitiesStates/*.class Logger/src/target/EntitiesStates/

    cd Public

    rm -rf registry
    rm -rf logger

    mkdir -p registry
    mkdir -p logger
    
    cd registry
    mkdir -p classes
    cd ..
    
    cd logger
    mkdir -p classes
    cd ..

    cd ..
    mv RMI/src/target/* Public/registry/classes/
    mv Logger/src/target/* Public/logger/classes/
    rm -rf RMI
    rm -rf Logger

EOF

echo -e "\n${bold}->${normal} A compilar Lounge na máquina ${bold}2${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws02.ua.pt << EOF
	javac Lounge/src/Interfaces/*.java Lounge/src/MainPackage/*.java Lounge/src/EntitiesStates/*.java Lounge/src/Repository/*.java

	mv Lounge/src/Interfaces/*.class Lounge/src/target/Interfaces/
    mv Lounge/src/MainPackage/*.class Lounge/src/target/MainPackage/
    mv Lounge/src/EntitiesStates/*.class Lounge/src/target/EntitiesStates/
    mv Lounge/src/Repository/*.class Lounge/src/target/Repository/

    cd Public
    rm -rf classes
    mkdir -p classes
    cd ..

    mv Lounge/src/target/* Public/classes/
    rm -rf Lounge

EOF

echo -e "\n${bold}->${normal} A compilar Outside World na máquina ${bold}3${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws03.ua.pt << EOF
	javac OutsideWorld/src/Interfaces/*.java OutsideWorld/src/MainPackage/*.java OutsideWorld/src/EntitiesStates/*.java

	mv OutsideWorld/src/Interfaces/*.class OutsideWorld/src/target/Interfaces/
    mv OutsideWorld/src/MainPackage/*.class OutsideWorld/src/target/MainPackage/
    mv OutsideWorld/src/EntitiesStates/*.class OutsideWorld/src/target/EntitiesStates/

    cd Public
    rm -rf classes
    mkdir -p classes
    cd ..

    mv OutsideWorld/src/target/* Public/classes/
    rm -rf OutsideWorld

EOF

echo -e "\n${bold}->${normal} A compilar Park na máquina ${bold}4${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws04.ua.pt << EOF
	javac Park/src/Interfaces/*.java Park/src/MainPackage/*.java Park/src/EntitiesStates/*.java

	mv Park/src/Interfaces/*.class Park/src/target/Interfaces/
    mv Park/src/MainPackage/*.class Park/src/target/MainPackage/
    mv Park/src/EntitiesStates/*.class Park/src/target/EntitiesStates/

    cd Public
    rm -rf classes
    mkdir -p classes
    cd ..

    mv Park/src/target/* Public/classes/
    rm -rf Park
   
EOF

echo -e "\n${bold}->${normal} A compilar Repair Area na máquina ${bold}5${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws05.ua.pt << EOF
    javac RepairArea/src/Interfaces/*.java RepairArea/src/MainPackage/*.java RepairArea/src/EntitiesStates/*.java RepairArea/src/Repository/*.java

    mv RepairArea/src/Interfaces/*.class RepairArea/src/target/Interfaces/
    mv RepairArea/src/MainPackage/*.class RepairArea/src/target/MainPackage/
    mv RepairArea/src/EntitiesStates/*.class RepairArea/src/target/EntitiesStates/
    mv RepairArea/src/Repository/*.class RepairArea/src/target/Repository/

    cd Public
    rm -rf classes
    mkdir -p classes
    cd ..

    mv RepairArea/src/target/* Public/classes/
    rm -rf RepairArea
    
EOF

echo -e "\n${bold}->${normal} A compilar Supplier Site na máquina ${bold}6${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws06.ua.pt << EOF
    javac SupplierSite/src/Interfaces/*.java SupplierSite/src/MainPackage/*.java SupplierSite/src/EntitiesStates/*.java

	mv SupplierSite/src/Interfaces/*.class SupplierSite/src/target/Interfaces/
    mv SupplierSite/src/MainPackage/*.class SupplierSite/src/target/MainPackage/
    mv SupplierSite/src/EntitiesStates/*.class SupplierSite/src/target/EntitiesStates/

    cd Public
    rm -rf classes
    mkdir -p classes
    cd ..

    mv SupplierSite/src/target/* Public/classes/
    rm -rf SupplierSite
     
EOF


echo -e "\n${bold}->${normal} A compilar Customers na máquina ${bold}7${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws07.ua.pt << EOF
	javac Customer/src/Interfaces/*.java Customer/src/MainPackage/*.java Customer/src/EntitiesStates/*.java Customer/src/Repository/*.java

	mv Customer/src/Interfaces/*.class Customer/src/target/Interfaces/
    mv Customer/src/MainPackage/*.class Customer/src/target/MainPackage/
    mv Customer/src/EntitiesStates/*.class Customer/src/target/EntitiesStates/
    mv Customer/src/Repository/*.class Customer/src/target/Repository/

    cd Public
    rm -rf classes
    mkdir -p classes
    cd ..

    mv Customer/src/target/* Public/classes/
    rm -rf Customer
    
EOF

echo -e "\n${bold}->${normal} A compilar Mechanics na máquina ${bold}8${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws08.ua.pt << EOF
    javac Mechanic/src/Interfaces/*.java Mechanic/src/MainPackage/*.java Mechanic/src/EntitiesStates/*.java Mechanic/src/Repository/*.java

    mv Mechanic/src/Interfaces/*.class Mechanic/src/target/Interfaces/
    mv Mechanic/src/MainPackage/*.class Mechanic/src/target/MainPackage/
    mv Mechanic/src/EntitiesStates/*.class Mechanic/src/target/EntitiesStates/
    mv Mechanic/src/Repository/*.class Mechanic/src/target/Repository/

    cd Public
    rm -rf classes
    mkdir -p classes
    cd ..

    mv Mechanic/src/target/* Public/classes/
    rm -rf Mechanic
    
EOF

echo -e "\n${bold}->${normal} A compilar Manager na máquina ${bold}9${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws09.ua.pt << EOF
	javac Manager/src/Interfaces/*.java Manager/src/MainPackage/*.java Manager/src/EntitiesStates/*.java Manager/src/Repository/*.java

	mv Manager/src/Interfaces/*.class Manager/src/target/Interfaces/
    mv Manager/src/MainPackage/*.class Manager/src/target/MainPackage/
    mv Manager/src/EntitiesStates/*.class Manager/src/target/EntitiesStates/
    mv Manager/src/Repository/*.class Manager/src/target/Repository/

    cd Public
    rm -rf classes
    mkdir -p classes
    cd ..

    mv Manager/src/target/* Public/classes/
    rm -rf Manager
    
EOF


###

echo -e "\n${bold}* Execução do código em cada nó *${normal}"


echo -e "\n${bold}->${normal} A iniciar e executar Registry e executar Logger na máquina ${bold}1${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws01.ua.pt << EOF
    cd Public/registry/classes
    nohup rmiregistry -J-Djava.rmi.server.useCodebaseOnly=true 22478 > a 2>&1 &

    sleep 5
    
    nohup java -Djava.rmi.server.codebase="http://l040101-ws01.ua.pt/sd0407/registry/classes/"\
    -Djava.rmi.server.useCodebaseOnly=true\
    -Djava.security.policy=java.policy\
    MainPackage.ServerRegisterRemoteObject > b 2>&1 &
    cd ../..
    cd logger/classes/
    
    sleep 5

    nohup java -Djava.rmi.server.codebase="http://l040101-ws01.ua.pt/sd0407/registry/classes/"\
    -Djava.rmi.server.useCodebaseOnly=true\
    -Djava.security.policy=java.policy\
    MainPackage.Main > c 2>&1 &
EOF

sleep 5

echo -e "\n${bold}->${normal} A executar Lounge na máquina ${bold}2${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws02.ua.pt << EOF
    cd Public/classes/
    nohup java -Djava.rmi.server.codebase="http://l040101-ws01.ua.pt/sd0407/registry/classes/"\
    -Djava.rmi.server.useCodebaseOnly=true\
    -Djava.security.policy=java.policy\
    MainPackage.Main > o 2>&1 &
EOF

sleep 1

echo -e "\n${bold}->${normal} A executar OutsideWorld na máquina ${bold}3${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws03.ua.pt << EOF
    cd Public/classes/
    nohup java -Djava.rmi.server.codebase="http://l040101-ws01.ua.pt/sd0407/registry/classes/"\
    -Djava.rmi.server.useCodebaseOnly=true\
    -Djava.security.policy=java.policy\
    MainPackage.Main > o 2>&1 &
EOF

sleep 1

echo -e "\n${bold}->${normal} A executar Park na máquina ${bold}4${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws04.ua.pt << EOF
    cd Public/classes/
    nohup java -Djava.rmi.server.codebase="http://l040101-ws01.ua.pt/sd0407/registry/classes/"\
    -Djava.rmi.server.useCodebaseOnly=true\
    -Djava.security.policy=java.policy\
    MainPackage.Main > o 2>&1 &
EOF

sleep 1

echo -e "\n${bold}->${normal} A executar RepairArea na máquina ${bold}5${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws05.ua.pt << EOF
    cd Public/classes/
    nohup java -Djava.rmi.server.codebase="http://l040101-ws01.ua.pt/sd0407/registry/classes/"\
    -Djava.rmi.server.useCodebaseOnly=true\
    -Djava.security.policy=java.policy\
    MainPackage.Main > o 2>&1 &
EOF

sleep 1

echo -e "\n${bold}->${normal} A executar SupplierSite na máquina ${bold}6${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws06.ua.pt << EOF
    cd Public/classes/
    nohup java -Djava.rmi.server.codebase="http://l040101-ws01.ua.pt/sd0407/registry/classes/"\
    -Djava.rmi.server.useCodebaseOnly=true\
    -Djava.security.policy=java.policy\
    MainPackage.Main > o 2>&1 &
EOF

# Wait for the shared regions to be launched before lanching the intervening enities

sleep 5

echo -e "\n${bold}->${normal} A executar Manager na máquina ${bold}9${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws09.ua.pt << EOF
    cd Public/classes/
    nohup java -Djava.rmi.server.codebase="http://l040101-ws01.ua.pt/sd0407/registry/classes/"\
    -Djava.rmi.server.useCodebaseOnly=true\
    -Djava.security.policy=java.policy\
    MainPackage.Main > o 2>&1 &
EOF
sleep 5

echo -e "\n${bold}->${normal} A executar Customers na máquina ${bold}7${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws07.ua.pt << EOF
    cd Public/classes/
    nohup java -Djava.rmi.server.codebase="http://l040101-ws01.ua.pt/sd0407/registry/classes/"\
    -Djava.rmi.server.useCodebaseOnly=true\
    -Djava.security.policy=java.policy\
    MainPackage.Main > o 2>&1 &
EOF

sleep 5

echo -e "\n${bold}->${normal} A executar Mechanics na máquina ${bold}8${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws08.ua.pt << EOF
    cd Public/classes/
    nohup java -Djava.rmi.server.codebase="http://l040101-ws01.ua.pt/sd0407/registry/classes/"\
    -Djava.rmi.server.useCodebaseOnly=true\
    -Djava.security.policy=java.policy\
    MainPackage.Main > o 2>&1 &
EOF
