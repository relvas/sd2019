package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface - Manager Supplier Site
 */

public interface iSupplierSite extends Remote{
    
    /**
     * Manager go to Supplier Site
     * 
     * @param part Part to replace.
     * @return parts Number of parts of that type to store.
     * @throws java.rmi.RemoteException
     */
    public int goToSupplier(int part) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
}
