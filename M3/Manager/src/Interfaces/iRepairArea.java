package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface - Manager Repair Area
 */

public interface iRepairArea extends Remote{
    
    /**
     * Manager store part.  
     *  
     * @param part Identifies the part.
     * @throws java.rmi.RemoteException
     */
    public void storePart(int part) throws RemoteException;
    
    /**
     * The Manager register a service.
     * 
     * @param customerID Identifies the Customer car.
     * @throws java.rmi.RemoteException
     */
    public void registerService(int customerID) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    
    /**
     * Stop mechanics by setting stop condition to true
     * @throws RemoteException 
     */
    public void stopMechanics() throws RemoteException;

}
