package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *Interface - Manager Lounge
 */

public interface iLounge extends Remote{
    
    /**
    * Get next task (originated by the manager).
    * 
     * @return 
     * @throws java.rmi.RemoteException
    */
    public boolean getNextTask() throws RemoteException;
    
    /**
    * The Manager checks the next task.
    * 
    * @return Situation Reference to whats next
     * @throws java.rmi.RemoteException
    */
    public Situation appraiseSit() throws RemoteException;
    
    /**
    * The Manager talks to the Customer.
    * 
    * @return Action What the customer wants
     * @throws java.rmi.RemoteException
    */
    public Action talkToCustomer() throws RemoteException;
    
    /**
     * The Manager checks if there is a replacement car available and if there is a key is delivery to the Customer.  
     * 
     * @param customerID Identifies the Customer.
     * @throws java.rmi.RemoteException
     */
    public void handCarKey(int customerID) throws RemoteException;
    
    /**
     * Manager checks if the Customer have a replace car.  
     * 
     * @throws java.rmi.RemoteException
     */ 
    public void receivePayment() throws RemoteException;
    
    /**
     * Get part
     * 
     * @return Part
     * @throws java.rmi.RemoteException
     */
    public int getPartId() throws RemoteException;
    
     /**
     * Get customer
     * 
     * @return customer
     * @throws java.rmi.RemoteException
     */
    public int getCId() throws RemoteException;
    
    
    /**
     * Set Flag restocked
     * 
     * @param partId Identifies the part
     * @throws java.rmi.RemoteException
     */
    public void setFlagRestocked(int partId) throws RemoteException;
    
     /**
     * Get Phone
     * 
     * @return phone
     */
    public int getPhoneId() throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    
}
