package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *  Interface - Manager Outside World
 */

public interface iOutsideWorld extends Remote{
    
    /**
     * Manager phones to the Customer - car fixed.
     *   
     * @param customerID Identifies the Customer.
     * @throws java.rmi.RemoteException
     */
    public void  phoneCustomer(int customerID) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
}
