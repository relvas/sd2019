package MainPackage;

import EntitiesStates.*;
import Interfaces.*;
import genclass.GenericIO;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *  Este tipo de dados implementa o thread manager.<p>
 *  A sua vida divide-se por várias tarefas que são realizadas sequencialmente.<p>
 *  Nesta solução, as tarefas vão ser as seguintes:
 *      - no princípio da sua actividade pode ser acordado pelo mecânico ou 
 *            pelo costumer;
 *      - se for acordado pelo costumer fala com ele;
 *      - se for acordado pelo mecânico pode ter de mandar vir peças do site do fornecedor 
 *            ou ligar ao cliente para vaisar que o seu carro esta pronto;
 *      - está sempre a verificar o que ainda tem para fazer até não haver nada;
 */

public class Manager extends Thread{
    
    /**
    *   Estado do manager
    *   @serialField entityState
    */
    private ManagerState entityState;
    
    /**
    *  Identificação do manager
    *
    *    @serialField managerId
    */
    
    private int managerId;
    
    /**
    *  Lounge
    *
    *    @serialField lounge
    */

    private iLounge lounge;
    
    
    /**
    *  Repair Area
    *
    *    @serialField repair
    */

    private iRepairArea repair;
    
    /**
    *  Supplier Site
    *
    *    @serialField supplier
    */

    private iSupplierSite supplier;
    
    /**
    *  Outside World
    *
    *    @serialField outside
    */

    private iOutsideWorld outside;
    
    
    /**
    *  Action
    *
    *    @serialField action
    */
    private Action action;
    
    /**
    *  Instanciação do thread manager.
    *
    *    
    *    @param supplier supplier site
    *    @param lounge lounge
    *    @param repair repair area
    *    @param outside outside world
    */

    public Manager (iSupplierSite supplier, iLounge lounge, iRepairArea repair, iOutsideWorld outside)
    {
        
        this.supplier = supplier;
        this.lounge = lounge;
        this.repair = repair;
        this.outside = outside;
    }
    
    
    /**
    *  Ciclo de vida do thread mecânico.
    */
    @Override
    public void run ()
    {
        int partId;
        int stock;
        int cId;
        boolean test;
        try {
        while(true) 
        { 
            
                this.entityState = ManagerState.CWTD;
                test=lounge.getNextTask();
                if(!test){
                    break;
                }
                switch(this.lounge.appraiseSit())
                {
                    case GETTINGNEWPARTS:
                        this.entityState = ManagerState.GNCA;
                        partId=lounge.getPartId();
                        stock=supplier.goToSupplier(partId);
                        repair.storePart(stock);
                        lounge.setFlagRestocked(partId);
                        break;
                        
                    case ALERTCOSTUMER:
                        this.entityState = ManagerState.ALLC;
                        cId = lounge.getPhoneId();
                        outside.phoneCustomer(cId);
                        break;
                        
                    case ATTENDINGCOSTUMER:
                        this.entityState = ManagerState.ATTC;
                        action = lounge.talkToCustomer();
                        cId = lounge.getCId();
                        switch(action)
                        {
                            case NOKEY:
                                this.entityState = ManagerState.POST;
                                repair.registerService(cId);
                                break;
                                
                            case KEY:
                                this.entityState = ManagerState.ATTC;
                                lounge.handCarKey(cId);
                                this.entityState = ManagerState.POST;
                                repair.registerService(cId);
                                break;
                                
                            case PAY:
                                this.entityState = ManagerState.ATTC;
                                lounge.receivePayment();
                                break;
                        }
                        break;
                }
                
           
        }
        repair.stopMechanics();
         } catch (RemoteException e) {
                GenericIO.writelnString("Remote Exception: " + e.getMessage());
                System.exit(1);
         }
    } 


    /**
     * This functions returns the Manager's current state.
     *
     * @return entityState current state of the Manager.
     * @throws java.rmi.RemoteException
     */
    public ManagerState getEntityState() throws RemoteException
    {
        return entityState;
    }

    /**
     * This function sets the Manager's state.
     *
     * @param entityState Next state to set the Manager.
     * @throws java.rmi.RemoteException
     */
    public void setEntityState(ManagerState entityState) throws RemoteException
    {
        this.entityState = entityState;
    }

}