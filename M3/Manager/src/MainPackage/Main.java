package MainPackage;

import genclass.GenericIO;

import Interfaces.*;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


/**
 * Manager main class.
 * 
 */

public class Main
{
    public static void main(String[] args) throws IOException
    {
         
        String rmiRegHostName = SimParameters.REGISTRY_HOST_NAME;
        int rmiRegPortNumb = SimParameters.REGISTRY_PORT;

        Manager man;
        
        iLogger logI = null;
        iLounge lgI = null;
        iOutsideWorld owI = null;
        iRepairArea reI = null;
        iSupplierSite suI = null;
        
        if (System.getSecurityManager () == null)
            System.setSecurityManager (new SecurityManager ());
        GenericIO.writelnString ("Security manager was installed!");
        

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            logI = (iLogger) registry.lookup(SimParameters.LOGGER_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating repository: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Repository is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            lgI = (iLounge) registry.lookup(SimParameters.LOUNGE_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating lounge: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Lounge is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            owI = (iOutsideWorld) registry.lookup(SimParameters.OUTSIDE_WORLD_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating Outside World: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Outside World is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            reI = (iRepairArea) registry.lookup(SimParameters.REPAIR_AREA_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating Park: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Park is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }
        
        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            suI = (iSupplierSite) registry.lookup(SimParameters.SUPPLIER_SITE_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating Park: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Park is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }


        GenericIO.writelnString ("Starting manager...");
        
        man = new Manager(suI, lgI, reI, owI);
        man.start();
        /**
         * Joining Thread
         */
        try {
            man.join();
        } catch (InterruptedException ex) { }
        GenericIO.writelnString ("Manager ended lifecycle.");
        
        try {
            logI.serviceEnd();
            lgI.serviceEnd();
            owI.serviceEnd();
            reI.serviceEnd();
            suI.serviceEnd();
        } catch (RemoteException ex) {
            System.out.println("Exception thrown while calling service end: " + ex.getMessage () );
            System.exit (1);
        }
        
    }
}