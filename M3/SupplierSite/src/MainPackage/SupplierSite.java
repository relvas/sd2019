package MainPackage;

import EntitiesStates.*;
import Interfaces.*;
import java.rmi.RemoteException;

/**
 * 
 * Shared region - Supplier Site
 */
public class SupplierSite implements iSupplierSite{
    
    /**
     * General Information
     * 
     * @serialField logger
     */
    private iLogger logger; 
 
    public SupplierSite(iLogger repository){
        logger = repository;
    }
    
    /**
     * Manager go to Supplier Site
     * 
     * @param part Part to replace.
     * @return parts Number of parts of that type to store.
     * @throws java.rmi.RemoteException
     */
    
    @Override
    public synchronized int goToSupplier(int part) throws RemoteException
    {
       //((Manager) Thread.currentThread()).setEntityState(ManagerState.GNCA);
       logger.setBoughtPart(part);
       logger.setManagerState(ManagerState.GNCA); 
       
       return part;
    }
    
    /**
     * Stop the service and shut down the betting center.
     * @throws java.rmi.RemoteException
     */
    public synchronized void serviceEnd() throws RemoteException{
        Main.serviceEnd = true;
        notifyAll();
    }
}
