package MainPackage;

import Interfaces.Register;
import genclass.GenericIO;
import Interfaces.iLogger;
import Interfaces.iSupplierSite;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Starts the OutsideWorld
 * 
 */
public class Main
{
    /**
     * Used to check if the service must terminate.
     */
    public static boolean serviceEnd = false;
    
    public static void main(String[] args)
    {

        String rmiRegHostName = SimParameters.REGISTRY_HOST_NAME;
        int rmiRegPortNumb = SimParameters.REGISTRY_PORT;
        
        String nameEntryBase = SimParameters.REGISTRY_NAME_ENTRY;
        String nameEntryObject = SimParameters.SUPPLIER_SITE_NAME_ENTRY;
        Registry registry = null;
        Register iregister = null;

        iLogger ilog = null;
        
        if (System.getSecurityManager () == null)
            System.setSecurityManager (new SecurityManager ());
        GenericIO.writelnString ("Security manager was installed!");
        
        try {
            registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção na criação do registo RMI: " + e.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O registo RMI foi criado!");

        try {
           
            ilog = (iLogger) registry.lookup(SimParameters.LOGGER_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção na localização do log: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException e) {
            GenericIO.writelnString("Log não está registado: " + e.getMessage() + "!");
            System.exit(1);
        }

        
        iSupplierSite owI = null;
        SupplierSite ow = new SupplierSite(ilog);
        try {
            owI = (iSupplierSite) UnicastRemoteObject.exportObject((Remote) ow, SimParameters.SUPPLIER_SITE_PORT);
        } catch (RemoteException ex) {
            GenericIO.writelnString("Excepção na geração do stub para o control: " + ex.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O stub para o Supplier Site foi gerado!");

       
        

        try {
            iregister = (Register) registry.lookup(nameEntryBase);
        } catch (RemoteException e) {
            GenericIO.writelnString("RegisterRemoteObject lookup Supplier Site: " + e.getMessage());
            System.exit(1);
        } catch (NotBoundException e) {
            GenericIO.writelnString("RegisterRemoteObject not bound Supplier Site: " + e.getMessage());
            System.exit(1);
        }

        try {
            iregister.bind(nameEntryObject, (Remote) owI);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção no registo do Supplier Site: " + e.getMessage());
            System.exit(1);
        } catch (AlreadyBoundException ex) {
            GenericIO.writelnString("O Supplier Site já está registado: " + ex.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O Supplier Site foi registado!");
        
        
        /* Wait for the service to end */
        while(!serviceEnd){
            try {
                synchronized(ow){
                    ow.wait();
                }
            } catch (InterruptedException ex) {
                GenericIO.writelnString("Main thread of Supplier Site was interrupted.");
                System.exit(1);
            }
        }
        
        GenericIO.writelnString("Supplier Site finished execution.");
        
        
         /* Unregister shared region */
        try
        { iregister.unbind (nameEntryObject);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("Supplier Site unregistration exception: " + e.getMessage ());
          System.exit (1);
        } catch (NotBoundException ex) {
          GenericIO.writelnString ("Supplier Site unregistration exception: " + ex.getMessage ());
          System.exit (1);
        }
        GenericIO.writelnString ("Supplier Site object was unregistered!");
        
        /* Unexport shared region */
        try
        { UnicastRemoteObject.unexportObject (ow, false);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("Supplier Site unexport exception: " + e.getMessage ());
          System.exit (1);
        }
        
        GenericIO.writelnString ("Supplier Site object was unexported successfully!");

    }
}