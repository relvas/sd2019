package Interfaces;

import EntitiesStates.ManagerState;
import java.rmi.Remote;
import java.rmi.RemoteException;


public interface iLogger extends Remote{
    
    public void setBoughtPart(int part) throws RemoteException;
    
    public void setManagerState(ManagerState mState) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
}
