package Interfaces;

import EntitiesStates.ManagerState;
import EntitiesStates.MechanicState;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * Logger Interface
 */
public interface iLogger extends Remote{
    
    public void setReplenishPart(int part) throws RemoteException;
    
    public void setManagerState(ManagerState mState) throws RemoteException;
    
    public void repairCarSubmitted(int customerId) throws RemoteException;

    public void setMechanicState(int mId, MechanicState mState) throws RemoteException;

    public void setVehicleWaitingPart(boolean spendPart, int pId, boolean wasWaitingPart) throws RemoteException;

    public void setFlagPartMissing(int pId) throws RemoteException;  
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    
}
