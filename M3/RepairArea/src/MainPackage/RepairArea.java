package MainPackage;

import EntitiesStates.*;
import Interfaces.iLogger;
import Interfaces.iRepairArea;
import Repository.MemFIFO;
import java.rmi.RemoteException;

/**
 * 
 * Repair Area
 */
public class RepairArea implements iRepairArea{
    private iLogger logger;
    
    /**
     * Car Parts available.
     * 
     * @serialField carParts[]
     * 
     * Each index is a different part.
     * Value is the amount available.
     */
    private int[] carParts;
    
    /**
     * Parts Requested
     * 
     * @serialField custReqCarPart
     * 
     * Index is the customer Id
     * The value is the part id requested
     */
    private int[] custReqCarPart;
    
    /**
     * Queue of tasks to do
     * 
     * @serialField taskQueue
     */
    private MemFIFO taskQueue;
   
    /**
     * Flag car waiting for part
     * 
     * @serialField carWaiting[]
     */
    private boolean[] carWaiting;
    
    /**
     * Cars waiting for a car part
     * 
     * @serialField carPartQueue
     */
    private MemFIFO carPartQueue;
    
    /**
     * Not First
     * 
     * @serialField notFirst[]
     */
    private boolean[] notFirst;
    
    /**
     * Flag missing
     * 
     * @serialField flagMissing[]
     */
    private boolean[] flagMissing;
    
    private boolean stop;
    
    
    public RepairArea(iLogger repository){
        
        this.logger = repository;
        this.carParts = new int [SimParameters.N_DIFF_PARTS];
        
        int idx;
        for (idx=0;idx<SimParameters.N_DIFF_PARTS;idx++)
        {
            carParts[idx] = SimParameters.N_PARTS;
        }
        
        custReqCarPart = new int [SimParameters.N_CUSTOMERS];
        carWaiting = new boolean [SimParameters.N_CUSTOMERS];
        for (idx=0;idx<SimParameters.N_CUSTOMERS;idx++)
        {
            custReqCarPart[idx] = -1;
            carWaiting[idx]=false;
        }
        
        taskQueue = new MemFIFO(SimParameters.N_CUSTOMERS);
        carPartQueue = new MemFIFO(SimParameters.N_CUSTOMERS);
        notFirst= new boolean[SimParameters.N_MECHANICS];
        for(int i=0;i<SimParameters.N_MECHANICS;i++)
            notFirst[i]=false;
        
        flagMissing = new boolean[SimParameters.N_DIFF_PARTS];
        for (int k = 0; k<SimParameters.N_DIFF_PARTS;k++){
            flagMissing[k] = false;
        }
        stop=false;
    }
    
    
    /**
     * Store Part
     * 
     * @param pId Identifies the part
     */
    @Override
    public synchronized void storePart(int pId) throws RemoteException{
        //((Manager) Thread.currentThread()).setEntityState(ManagerState.REPL);
        carParts[pId] = SimParameters.N_PARTS; 
        logger.setReplenishPart(pId);
        logger.setManagerState(ManagerState.REPL);
        flagMissing[pId]=false;
        
        notifyAll();
    }
    
    
    /**
     * Register service
     * 
     * @param customerId Identifies the Customer
     */
    @Override
    public synchronized void registerService(int customerId) throws RemoteException{
        
        //((Manager) Thread.currentThread()).setEntityState(ManagerState.POST);
        logger.repairCarSubmitted(customerId);
        logger.setManagerState(ManagerState.POST);
        
        taskQueue.write(customerId);
        
        notifyAll();
        
    }
    
    /**
     * Read the paper
     * 
     * @param mechanicId Identifies the Mechanic
     * @return true if Mechanic is alive or false if the Mechanic is dead
     */
    @Override
    public synchronized boolean readThePaper(int mechanicId) throws RemoteException{
        //if(notFirst[((Mechanic) Thread.currentThread()).getMechId()]){
        //    ((Mechanic) Thread.currentThread()).setEntityState(MechanicState.WAIT);
        //    logger.setMechanicState(mechanicId, MechanicState.WAIT);
        //}
        //else
        //    notFirst[((Mechanic) Thread.currentThread()).getMechId()]=true;
        
        while(taskQueue.empty() && carPartQueue.empty())
        {
            if(stop)return false;
            try
            {
             
                wait();
            }
            catch (InterruptedException ex){ 
                return false;
            }
        }
        
        return true;
    }
    /**
     * Start repair procedure
     * 
     * @param mechanicId
     * @return car Id 
     */
    @Override
    public synchronized int startRepairProcedure(int mechanicId) throws RemoteException{
        int aux=-1;

            if((!carPartQueue.empty()) ){
                aux = (int)carPartQueue.read();
                
            }
            else if (!taskQueue.empty()) {
                aux = (int)taskQueue.read();
            }
        if(aux!=-1)
        {
            //((Mechanic) Thread.currentThread()).setEntityState(MechanicState.FIXI);
            logger.setMechanicState(mechanicId, MechanicState.FIXI);
        }
        return aux;
    }
    
    /**
    * Fixing the car
    * 
    * @param mechanicId 
    * @param customerId
    * @param pId
    */
    @Override
    public synchronized void fixIt(int mechanicId, int customerId, int pId) throws RemoteException
    {
        //((Mechanic) Thread.currentThread()).setEntityState(MechanicState.FIXI);
        logger.setMechanicState(mechanicId, MechanicState.FIXI); 
    }
    
    /**
    * Get required part
    * 
    * @param mechanicId
    * @param customerId
    * @param pId
    */
    @Override
    public synchronized void getRequiredPart(int mechanicId, int customerId, int pId) throws RemoteException
    {
        //((Mechanic) Thread.currentThread()).setEntityState(MechanicState.CHEC);
        logger.setMechanicState(mechanicId, MechanicState.CHEC);
    }
    
    /**
    * Get car parts
    * 
    * @param pId
    * @return carParts[pId]
    */
    private synchronized int getCarParts(int pId) throws RemoteException{
        return carParts[pId];
    }
    
    /**
     * Decrement car parts
     * 
     * @param pId Identifies the part
     */
    private synchronized void decrementCarParts(int pId) throws RemoteException{
        carParts[pId]--;
    }
    
    
    /**
     * Part available
     * 
     * @param customerId Identifies the customer
     * @param pId Identitifies the part
     * @return true if part is available or false if isnt
     */
    @Override
    public synchronized int partAvailable(int customerId, int pId) throws RemoteException
    {
        if (getCarParts(pId) != 0)
        {
            logger.setVehicleWaitingPart(true,pId,custReqCarPart[customerId]==pId);
            decrementCarParts(pId);
            return pId;
        }
        else 
        {
            logger.setFlagPartMissing(pId);
            logger.setVehicleWaitingPart(false,pId,custReqCarPart[customerId]==pId);
            custReqCarPart[customerId]=pId;
            carPartQueue.write(customerId);
            if(!flagMissing[pId]){
                flagMissing[pId]=true;
                pId=-1;
            }
            else
                pId=-999; 
        
            
            return pId;
        }
    }
    
    /**
    * 
    * @param mechanicId
    * @param customerId
    * @param pId
    */
    @Override
    public synchronized void resumeRepairProcedure(int mechanicId, int customerId, int pId) throws RemoteException
    {
        //((Mechanic) Thread.currentThread()).setEntityState(MechanicState.FIXI);
        logger.setMechanicState(mechanicId, MechanicState.FIXI);
    }
    
    /**
     * Stop the service and shut down the betting center.
     * @throws java.rmi.RemoteException
     */
    public synchronized void serviceEnd() throws RemoteException{
        Main.serviceEnd = true;
        notifyAll();
    }
    
    /**
     * Sets the stop condition to true
     * @throws RemoteException 
     */
    @Override
    public synchronized void stopMechanics() throws RemoteException{
        stop = true;
    }
}
