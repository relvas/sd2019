package MainPackage;

import Interfaces.Register;
import genclass.GenericIO;
import Interfaces.iLogger;
import Interfaces.iRepairArea;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Starts the Repair Area
 * 
 */
public class Main
{
    /**
     * Used to check if the service must terminate.
     */
    public static boolean serviceEnd = false;
    
    public static void main(String[] args)
    {

        String rmiRegHostName = SimParameters.REGISTRY_HOST_NAME;
        int rmiRegPortNumb = SimParameters.REGISTRY_PORT;
        
        String nameEntryBase = SimParameters.REGISTRY_NAME_ENTRY;
        String nameEntryObject = SimParameters.REPAIR_AREA_NAME_ENTRY;
        Registry registry = null;
        Register iregister = null;

        iLogger ilog = null;
        
        if (System.getSecurityManager () == null)
            System.setSecurityManager (new SecurityManager ());
        GenericIO.writelnString ("Security manager was installed!");
        
        try {
            registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção na criação do registo RMI: " + e.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O registo RMI foi criado!");

        try {
           
            ilog = (iLogger) registry.lookup(SimParameters.LOGGER_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção na localização do log: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException e) {
            GenericIO.writelnString("Log não está registado: " + e.getMessage() + "!");
            System.exit(1);
        }

        
        iRepairArea owI = null;
        RepairArea ow = new RepairArea(ilog);
        try {
            owI = (iRepairArea) UnicastRemoteObject.exportObject((Remote) ow, SimParameters.REPAIR_AREA_PORT);
        } catch (RemoteException ex) {
            GenericIO.writelnString("Excepção na geração do stub para o control: " + ex.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O stub para o Repair Area foi gerado!");

       
        

        try {
            iregister = (Register) registry.lookup(nameEntryBase);
        } catch (RemoteException e) {
            GenericIO.writelnString("RegisterRemoteObject lookup Repair Area: " + e.getMessage());
            System.exit(1);
        } catch (NotBoundException e) {
            GenericIO.writelnString("RegisterRemoteObject not bound Repair Area: " + e.getMessage());
            System.exit(1);
        }

        try {
            iregister.bind(nameEntryObject, (Remote) owI);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção no registo do Repair Area: " + e.getMessage());
            System.exit(1);
        } catch (AlreadyBoundException ex) {
            GenericIO.writelnString("O Repair Area já está registado: " + ex.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O Repair Area foi registado!");
        
        
        /* Wait for the service to end */
        while(!serviceEnd){
            try {
                synchronized(ow){
                    ow.wait();
                }
            } catch (InterruptedException ex) {
                GenericIO.writelnString("Main thread of Repair Area was interrupted.");
                System.exit(1);
            }
        }
        
        GenericIO.writelnString("Repair Area finished execution.");
        
        
         /* Unregister shared region */
        try
        { iregister.unbind (nameEntryObject);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("Repair Area unregistration exception: " + e.getMessage ());
          System.exit (1);
        } catch (NotBoundException ex) {
          GenericIO.writelnString ("Repair Area unregistration exception: " + ex.getMessage ());
          System.exit (1);
        }
        GenericIO.writelnString ("Repair Area object was unregistered!");
        
        /* Unexport shared region */
        try
        { UnicastRemoteObject.unexportObject (ow, false);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("Repair Area unexport exception: " + e.getMessage ());
          System.exit (1);
        }
        
        GenericIO.writelnString ("Repair Area object was unexported successfully!");

    }
}