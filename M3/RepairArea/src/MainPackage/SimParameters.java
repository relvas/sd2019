package MainPackage;

/**
 * Serves as a way of providing global variables to all the Entities and Shared
 * Regions in the problem.
 * 
 */ 

public class SimParameters {
    
     /**
     * Logger server hostname.
     */
    public final static String LOGGER_HOSTNAME = "l040101-ws01.ua.pt";

    /**
     * Logger server port.
     */
    public final static int LOGGER_PORT = 22471;
    
    /**
     * Logger name entry for the register.
     */
    public final static String LOGGER_NAME_ENTRY = "LoggerHandler";

    /**
     * Lounge server hostname.
     */
    public final static String LOUNGE_HOSTNAME = "l040101-ws02.ua.pt";

    /**
     * Lounge server port.
     */
    public final static int LOUNGE_PORT = 22472;
    
    /**
     * Lounge name entry for the register.
     */
    public final static String LOUNGE_NAME_ENTRY = "LoungeHandler";

    /**
     * Outside World server hostname.
     */
    public final static String OUTSIDE_WORLD_HOSTNAME = "l040101-ws03.ua.pt";

    /**
     * Outside World server port.
     */
    public final static int OUTSIDE_WORLD_PORT = 22473;
    
    /**
     * Outside World name entry for the register.
     */
    public final static String OUTSIDE_WORLD_NAME_ENTRY = "OutsideWorldHandler";

    /**
     * Park server hostname.
     */
    public final static String PARK_HOSTNAME = "l040101-ws04.ua.pt";

    /**
     * Park server port.
     */
    public final static int PARK_PORT = 22474;

    /**
     * Park name entry for the register.
     */
    public final static String PARK_NAME_ENTRY = "ParkHandler";
    
    /**
     * Repair Area server hostname.
     */
    public final static String REPAIR_AREA_HOSTNAME = "l040101-ws05.ua.pt";

    /**
     * Repair Area server port.
     */
    public final static int REPAIR_AREA_PORT = 22475;
    
    /**
     * Repair Area name entry for the register.
     */
    public final static String REPAIR_AREA_NAME_ENTRY = "RepairAreaHandler";

    /**
     * Supplier Site server hostname.
     */
    public final static String SUPPLIER_SITE_HOSTNAME = "l040101-ws06.ua.pt";

    /**
     * Supplier Site server port.
     */
    public final static int SUPPLIER_SITE_PORT = 22476;
    
    /**
     * Supplier Site name entry for the register.
     */
    public final static String SUPPLIER_SITE_NAME_ENTRY = "SupplierSiteHandler";
    
     /**
     * Server registry host name.
     */
    public final static String SERVER_REGISTRY_HOST_NAME = "l040101-ws01.ua.pt";
    
    /**
     * Server registry server port.
     */
    public final static int SERVER_REGISTRY_PORT = 22477;
    
    /**
     * Server registry host name.
     */
    public final static String REGISTRY_HOST_NAME = "l040101-ws01.ua.pt";
    
    /**
     * Server registry server port.
     */
    public final static int REGISTRY_PORT = 22478;
    
    /**
     * Registry name entry for the register.
     */
    public final static String REGISTRY_NAME_ENTRY = "RegisterHandler";
    
    
    
    /**
     * Number of existing Mechanics.
     */
    public static final int N_MECHANICS = 2;
    
    /**
     * Number of customers.
     */
    public static final int N_CUSTOMERS = 30;
    
    
    /**
     * Number of existing replacements cars. 
     */
    public static final int N_REPLACEMENT_CARS = 3;
    
    
    /**
     * Number of different parts.
     */
    public static final int N_DIFF_PARTS = 3;
    
    /**
     * Number of exising parts stored.
     */
    public static final int N_PARTS = 5;
    
}
