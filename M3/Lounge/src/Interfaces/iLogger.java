package Interfaces;

import EntitiesStates.CustomerState;
import EntitiesStates.ManagerState;
import EntitiesStates.MechanicState;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * Lounge Logger interface
 */
public interface iLogger extends Remote{
    
    public void setManagerState(ManagerState mState) throws RemoteException;
    
    public void setInQueueSize(int qsize) throws RemoteException;
    
    public void incrementNCars() throws RemoteException;
        
    public void setParkedVehicles(boolean remove) throws RemoteException;
    
    public void setMechanicState(int mId, MechanicState mState) throws RemoteException;
    
    public void setCustomerState(int cId, CustomerState cState) throws RemoteException;
    
    public void setWaitingReplacement(int cId, boolean wantRep) throws RemoteException;
    
    public void incrementNWaitingRepl(boolean log) throws RemoteException;
    
    public void decrementNCars() throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;

    
}
