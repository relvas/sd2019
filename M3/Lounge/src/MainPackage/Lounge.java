package MainPackage;

import EntitiesStates.*;
import Repository.MemFIFO;
import Interfaces.*;
import java.rmi.RemoteException;

/** 
 * Shared region - Lounge
 * 
 */

public class Lounge implements iLounge{
 
    /**
     * General Information 
     * 
     * @serialField logger
     */
    private iLogger logger;
    
    
    /**
    * Number of available replace car.
    * 
    * @serialField  ncars
    */
   private int ncars;
    
    /**
    * Number of replace car keys. 
    * 
    * @serialField keys[]
    */
    private int keys[];
    
    /**
    * Boolean client payed the service 
    * 
    * @serialField payed
    */
    private boolean payed;
    

    /**
    * Manager tasks related to customers
    *
    * @serialField managerToDo;
    */
    private MemFIFO managerToDo;
    
    /**
    * Manager tasks related to mechanics
    *
    * @serialField managerToDoMec;
    */
    private MemFIFO managerToDoMec;
    
    
    /**
    * Alert manager car is ready to pick up
    *
    * @serialField managerToDoReady;
    */
    private MemFIFO managerToDoReady;
    
    
    /**
    * Attending the current customerId
    *
    * @serialField turnCurrentCId;
    */
    private int turnCurrentCId;
    
    /**
    * Attending the current customerId (control)
    *
    * @serialField turnCurrentCIdCondition;
    */
    private int turnCurrentCIdCondition;
    
    /**
    * Waiting for customer
    *
    * @serialField waitingCustomer;
    */
    private boolean waitingCustomer;
    
    /**
    * Check if the current customer wants replecement car
    *
    * @serialField currentCWantRepCar;
    */
    private boolean currentCWantRepCar;
    
    /**
    * Customer wants to pay
    *
    * @serialField wantToPay;
    */
    private boolean wantToPay;
    
    
    /**
    *  Customers waiting for replace car.
    *
    * @serialField waitingRepCar
    */
    private MemFIFO waitingRepCar;
    
    /**
    * Array to know if customer has to pay
    * index=cId
    * value=hasToPay
    * @serialField doIHaveToPay;
    */
    private boolean[] doIHaveToPay;
    
    /**
     * Queue size
     * 
     * @serialField queueSize
     */
    private int queueSize = 0;
    
    /**
     * Not first
     * 
     * @serialField notFirst
     */
    private boolean notFirst;
    
    /**
     * Flag Missing
     * 
     * @serialField flagMissing[]
     */
    private boolean[] flagMissing;
    
    /**
     * Turn to pick up car
     * 
     * @serialField turnTopickUpCar
     */
    private int turnToPickUpCar=-1;
    
    /**
     * Is Coming from queue in
     * 
     * @serialField isComingFromQueueIn
     */
    private boolean isComingFromQueueIn;
    
    /**
     * Id of who's paying
     * 
     * @serialField payingId
     */
    private int payingId=-1;
    
    /**
     *  Counter for payments. Used to check if the 30 customers have paid already.
     */
    private int counterPayment;
    
    /**
    * Init the Lounge.
    *
    * @param repository Instance that implements GeneralInformation methods.
    */
    public Lounge(iLogger repository)
    {
        logger = repository;
        waitingRepCar = new MemFIFO (SimParameters.N_CUSTOMERS);
        ncars = SimParameters.N_REPLACEMENT_CARS;
        
        managerToDo= new MemFIFO(SimParameters.N_CUSTOMERS);
        managerToDoMec= new MemFIFO(SimParameters.N_CUSTOMERS);
        managerToDoReady= new MemFIFO(SimParameters.N_CUSTOMERS);
        
        keys = new int [SimParameters.N_REPLACEMENT_CARS];
        
        for(int i = 0; i < SimParameters.N_REPLACEMENT_CARS; i++)
            keys[i] = -1;
        
        payed = false;
        counterPayment=0;
        waitingCustomer=true;
        isComingFromQueueIn=false;
        turnCurrentCId=-1;
        turnCurrentCIdCondition=-1;
        wantToPay=false;
        doIHaveToPay = new boolean[SimParameters.N_CUSTOMERS];
        for (int k = 0; k<SimParameters.N_CUSTOMERS;k++ ){
            doIHaveToPay[k] = false;
        }
        
        notFirst=false;
        flagMissing = new boolean[SimParameters.N_DIFF_PARTS];
        for (int k = 0; k<SimParameters.N_DIFF_PARTS;k++){
            flagMissing[k] = false;
        }
    }
    
    /**
    * Get next task (originated by the manager).
    * 
    *  
    */
    
    public synchronized boolean getNextTask() throws RemoteException
    {
        if(notFirst){
            //((Manager) Thread.currentThread()).setEntityState(ManagerState.CWTD);
            logger.setManagerState(ManagerState.CWTD);

        }
        else
            notFirst = true;
            
        if(counterPayment==30) return false;
        
        while (managerToDo.empty() && managerToDoMec.empty() && managerToDoReady.empty()) 
        {
            try
            {
                wait ();
            }
            catch (InterruptedException e){return false; }
        }
        
        return true;
    }
    
    /**
     *  Manager is appraising situation to decide what to do.
     * 
     */
    
    public synchronized Situation appraiseSit() throws RemoteException
    {
       if (!managerToDo.empty()) {
            isComingFromQueueIn=true;
            return Situation.ATTENDINGCOSTUMER; 
        }
        else if (!managerToDoMec.empty()) {
            return Situation.GETTINGNEWPARTS;
        }
        else if(!managerToDoReady.empty()){
            return Situation.ALERTCOSTUMER;
        }
        return Situation.ERRO;
    }
    
    /**
    * The Manager talks to the Customer.
    * 
    * @return What the customer wants
    */
    
    
    public synchronized Action talkToCustomer() throws RemoteException{
        //((Manager) Thread.currentThread()).setEntityState(ManagerState.ATTC);
        decrementQueueSize();
        logger.setInQueueSize(queueSize);
        logger.setManagerState(ManagerState.ATTC);
        turnCurrentCId=-1;
        
        waitingCustomer=true;
          
        turnCurrentCId = (int)managerToDo.read();
        turnCurrentCIdCondition=turnCurrentCId;
        
        
        notifyAll();
        
        while(waitingCustomer)
        {
            try
            {
                wait();
            } catch (InterruptedException e){}
        }
        if(doIHaveToPay[turnCurrentCId])
            return Action.PAY;
        
        doIHaveToPay[turnCurrentCId]=true;
        
        if(currentCWantRepCar){
            currentCWantRepCar=false;
            return Action.KEY;
        }
        
        return Action.NOKEY;
        
        
    }
    
    /**
     * The Manager checks if there is a replacement car available and if there is a key is delivery to the Customer.  
     * 
     * @param customerID Identifies the Customer.
     */
    
    public synchronized void handCarKey(int customerID) throws RemoteException
    {
       turnToPickUpCar=customerID;
       turnCurrentCId=-1;
       notifyAll();
    }
    
    /**
     * Manager checks if the Customer have a replace car.  
     * 
     */ 
    
    public synchronized void receivePayment() throws RemoteException
    {
        counterPayment++;
        notifyAll();
        while(!payed)
        {
            try
            {
                wait();
            } catch (InterruptedException e){}
        }
        turnToPickUpCar=-1;
        for(int i = 0; i < SimParameters.N_REPLACEMENT_CARS; i++)
        {
            if(keys[i] == payingId){
                keys[i]=-1;
                try {
                    turnToPickUpCar=(int)waitingRepCar.peek();
                } catch (Exception e) {
                }
                logger.incrementNCars();
                notifyAll();       
            }
        }
        payingId=-1;
        payed = false;
    }
    
    /**
     * Get part
     * 
     * @return Part
     */
    
    public synchronized int getPartId() throws RemoteException{
        return (int)managerToDoMec.read();
    }
    
    /**
     * Set Flag restocked
     * 
     * @param partId Identifies the part
     */
    
    public synchronized void setFlagRestocked(int partId) throws RemoteException{
       flagMissing[partId]=false;
    }
    /**
     * Get customer
     * 
     * @return customer
     */
    
    public synchronized int getCId() throws RemoteException{
        return turnCurrentCId;
    }
    
    /**
     * Get Phone
     * 
     * @return phone
     */
    
    public synchronized int getPhoneId() throws RemoteException{
        return (int)managerToDoReady.read();
    }
    
    
    /**
    * The Mechanic let the Manager know we need to replace a part.
    * 
    * @param mechanicID Identifies the Mechanic.
    * @param part Identifies the part needed.
    */
    
    public synchronized void letManagerKnow(int mechanicID, int part) throws RemoteException
    {
        //((Mechanic)Thread.currentThread()).setEntityState(MechanicState.ALLM); 
        logger.setParkedVehicles(false);
        logger.setMechanicState(mechanicID, MechanicState.ALLM);
        if(part>=0){
            managerToDoMec.write(part); 
        }    
        notifyAll();
    }
    
    /**
    * The Mechanic let the Manager knows that the car from customerID is ready.
    * 
    * @param mechanicID Identifies the Mechanic.
    * @param customerID Identifies the Customer car.
    */
    
    public synchronized void repairConcluded(int mechanicID, int customerID) throws RemoteException
    {
        //((Mechanic)Thread.currentThread()).setEntityState(MechanicState.ALLM);
        managerToDoReady.write(customerID);
        
        logger.setMechanicState(mechanicID, MechanicState.ALLM);
        
        notifyAll();
    }
    
    
    /**
     * Customer is waiting for is turn to talk with the Manager.   
     * 
     * @param customerID Identifies the Customer.
     */
    
    public synchronized void queueIn(int customerID) throws RemoteException
    {
        //((Customer) Thread.currentThread()).setEntityState(CustomerState.RECE);
        incrementQueueSize();
        logger.setInQueueSize(queueSize);
        logger.setCustomerState(customerID, CustomerState.RECE);
        
        managerToDo.write(customerID);
       
        notifyAll();
        while(turnCurrentCIdCondition!=customerID)
        {
            try
            {
                wait();
            } catch (InterruptedException e){}
        }
        turnCurrentCIdCondition=-1; 
    }
    
    /**
     * Talk with the Manager.  
     * 
     * @param customerID Identifies the Customer
     * @param wantRepCar Identifies if a replace car is needed.
     */
    
    public synchronized void talkWithManager(int customerID, boolean wantRepCar) throws RemoteException
    {
        logger.setWaitingReplacement(turnCurrentCId, wantRepCar);
        if (wantRepCar) { 
            currentCWantRepCar=true;
            waitingRepCar.write(customerID);
        }
        else{
            currentCWantRepCar=false;
        }
        waitingCustomer=false;
        notifyAll();
    }
    
    /**
     * The Customer collects the key of the replaced car.  
     * 
     * @param customerID Identifies the Customer.
     * @return key Identifies the replaced car.
     */
    
    public synchronized int collectKey(int customerID) throws RemoteException
    {
        //((Customer) Thread.currentThread()).setEntityState(CustomerState.WFRC);
        logger.incrementNWaitingRepl(false);
        logger.setCustomerState(customerID, CustomerState.WFRC);
        
        turnCurrentCIdCondition=-1;
        
        while(true)
        {
            if(turnToPickUpCar==customerID)
            {
                turnToPickUpCar=-1;
                for(int i = 0; i < SimParameters.N_REPLACEMENT_CARS; i++)
                {
                    if(keys[i] == -1){
                        keys[i]=customerID;
                        waitingRepCar.read();
                        logger.decrementNCars();
                        
                        return i;
                    }
                }
            }
            try
            {  
                wait();
            } catch (InterruptedException e){} 
        }   
    }
    
    /**
    * Pay for Service.  
    * @param customerID Identifies the Customer.
    */
    
    public synchronized void payForService(int customerID) throws RemoteException
    {
        turnCurrentCIdCondition=-1;
        waitingCustomer=false;
        payingId=customerID;
        payed = true;
        notifyAll();
    }
    
    /**
     * Increment queue size
     */
    private synchronized void incrementQueueSize() throws RemoteException{
        this.queueSize++;
    }
    
    /**
     * Decrement queue size
     */
    private synchronized void decrementQueueSize() throws RemoteException{
        this.queueSize--;
    }
    
    /**
     * Stop the service and shut down the betting center.
     * @throws java.rmi.RemoteException
     */
    public synchronized void serviceEnd() throws RemoteException{
        Main.serviceEnd = true;
        notifyAll();
    }
}