package MainPackage;

import Interfaces.Register;
import genclass.GenericIO;
import Interfaces.iLogger;
import Interfaces.iLounge;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Starts the Lounge Area
 * 
 */
public class Main
{
    /**
     * Used to check if the service must terminate.
     */
    public static boolean serviceEnd = false;
    
    public static void main(String[] args)
    {
        /**
         * RMI Registry Hostname
         */
        String rmiRegHostName = SimParameters.REGISTRY_HOST_NAME;
        
        /**
         * RMI Registry Port Number
         */
        int rmiRegPortNumb = SimParameters.REGISTRY_PORT;
        
        /**
         * RMI Registry Entry Base Name
         */
        String nameEntryBase = SimParameters.REGISTRY_NAME_ENTRY;
        
        /**
         * RMI Registry Object Lounge
         */
        String nameEntryObject = SimParameters.LOUNGE_NAME_ENTRY;
        
        /**
         * RMI Registry 
         */
        Registry registry = null;
        /**
         * RMI Registry Interface
         */
        Register iregister = null;
        
        /**
         * Lounge Logger Interface
         */
        iLogger ilog = null;
        
        if (System.getSecurityManager () == null)
            System.setSecurityManager (new SecurityManager ());
        GenericIO.writelnString ("Security manager was installed!");
        
        
        try {
            registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção na criação do registo RMI: " + e.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O registo RMI foi criado!");

        try {
            ilog = (iLogger) registry.lookup(SimParameters.LOGGER_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção na localização do log: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException e) {
            GenericIO.writelnString("Log não está registado: " + e.getMessage() + "!");
            e.printStackTrace();
            System.exit(1);
        }

        /**
         * Lounge Interface
         */
        iLounge loI = null;
        /**
         * Lounge 
         */
        Lounge ow = new Lounge(ilog);
        try {
            loI = (iLounge) UnicastRemoteObject.exportObject((Remote) ow, SimParameters.LOUNGE_PORT);
        } catch (RemoteException ex) {
            GenericIO.writelnString("Excepção na geração do stub para o control: " + ex.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O stub para o Lounge foi gerado!");

       
        

        try {
            iregister = (Register) registry.lookup(nameEntryBase);
        } catch (RemoteException e) {
            GenericIO.writelnString("RegisterRemoteObject lookup Lounge: " + e.getMessage());
            System.exit(1);
        } catch (NotBoundException e) {
            GenericIO.writelnString("RegisterRemoteObject not bound Lounge: " + e.getMessage());
            System.exit(1);
        }

        try {
            iregister.bind(nameEntryObject, (Remote) loI);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção no registo do Lounge: " + e.getMessage());
            System.exit(1);
        } catch (AlreadyBoundException ex) {
            GenericIO.writelnString("O Lounge já está registado: " + ex.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O Lounge foi registado!");
        
        
        /* Wait for the service to end */
        while(!serviceEnd){
            try {
                synchronized(ow){
                    ow.wait();
                }
            } catch (InterruptedException ex) {
                GenericIO.writelnString("Main thread of Lounge was interrupted.");
                System.exit(1);
            }
        }
        
        GenericIO.writelnString("Lounge finished execution.");
        
        
         /* Unregister shared region */
        try
        { iregister.unbind (nameEntryObject);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("Lounge unregistration exception: " + e.getMessage ());
          System.exit (1);
        } catch (NotBoundException ex) {
          GenericIO.writelnString ("Lounge unregistration exception: " + ex.getMessage ());
          System.exit (1);
        }
        GenericIO.writelnString ("Lounge object was unregistered!");
        
        /* Unexport shared region */
        try
        { UnicastRemoteObject.unexportObject (ow, false);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("Lounge unexport exception: " + e.getMessage ());
          System.exit (1);
        }
        
        GenericIO.writelnString ("Lounge object was unexported successfully!");

    }
}