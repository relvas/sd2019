package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *  Interface - Customer Park
 */

public interface iPark extends Remote{
    
    /**
     * Customer go to the repair shop and park the car - his own or the replace car.
     * 
     * @param customerID Identifies the Customer car. 
     * @throws java.rmi.RemoteException 
     */
    
    public void goToRepairShop(int customerID) throws RemoteException;
    
    /**
     * Customer finds the replace car.
     * 
     * @param customerID Identifies the Customer car.
     * @param key Identifies the replace car.
     * @throws java.rmi.RemoteException
     */
    public void findCar(int customerID,int key) throws RemoteException;
    
    /**
     * Customer removes his repaired car from the Park. 
     * 
     * @param customerID Identifies the Customer car. 
     * @throws java.rmi.RemoteException 
     */
    public void collectCar(int customerID) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    
}
