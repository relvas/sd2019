package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface - Customer Lounge
 */

public interface iLounge extends Remote{
    
    /**
     * Customer is waiting for is turn to talk with the Manager.   
     * 
     * @param customerID Identifies the Customer.
     * @throws java.rmi.RemoteException
     */
    public void queueIn(int customerID) throws RemoteException;
    
    /**
     * Talk with the Manager.  
     * 
     * @param customerID Identifies the Customer
     * @param wantRepCar Identifies if a replace car is needed.
     * @throws java.rmi.RemoteException
     */
    public void talkWithManager(int customerID, boolean wantRepCar) throws RemoteException;
    
    /**
     * The Customer collects the key of the replaced car.  
     * 
     * @param customerID Identifies the Customer.
     * @return key Identifies the replaced car.
     * @throws java.rmi.RemoteException
     */
    public int collectKey(int customerID) throws RemoteException;
    
    
    /**
    * Pay for Service.  
    * @param customerID Identifies the Customer.
     * @throws java.rmi.RemoteException
    */
    public void payForService(int customerID) throws RemoteException;
    
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    
    
}
