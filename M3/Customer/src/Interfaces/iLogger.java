package Interfaces;

import EntitiesStates.*;
import java.rmi.Remote;
import java.rmi.RemoteException;


public interface iLogger extends Remote{
    
    public void setCustCarDriven(int cId, int repId, boolean secondTime) throws RemoteException;
    
    public void setCustomerState(int cId, CustomerState cState) throws RemoteException;
    
    public void setParkedVehicles(boolean remove) throws RemoteException;
    
    public void setMechanicState(int mId, MechanicState mState) throws RemoteException;
    
    public void setManagerState(ManagerState mState) throws RemoteException;
    
    public void repairCarSubmitted(int customerId) throws RemoteException;
    
    public void setInQueueSize(int qsize) throws RemoteException;
    
    public void setWaitingReplacement(int cId, boolean wantRep) throws RemoteException;
    
    public void incrementNWaitingRepl(boolean log) throws RemoteException;
    
    public void decrementNWaitingRepl(boolean log) throws RemoteException;
    
    public void setReplacementVehicles(int ncars) throws RemoteException;
    
    public void incrementNCars() throws RemoteException;
            
    public void decrementNCars() throws RemoteException;
    
    public void setNRepaired(int cId) throws RemoteException;
    
    public void setVehicleWaitingPart(boolean spendPart, int pId, boolean wasWaitingPart) throws RemoteException;

    public void setFlagPartMissing(int pId) throws RemoteException;
    
    public void setReplenishPart(int part) throws RemoteException;
    
    public void setBoughtPart(int part) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    
}
