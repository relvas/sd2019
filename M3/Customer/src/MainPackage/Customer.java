package MainPackage;
import EntitiesStates.*;
import Interfaces.*;
import genclass.GenericIO;
import java.rmi.RemoteException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *  Este tipo de dados implementa o thread costumer.<p>
 *  A sua vida divide-se por duas tarefas principais, ter uma vida normal e decidir ir reparar o carro, 
 *  que são realizadas sequencialmente.<p>
 *  Nesta solução, decidir ir reparar o carro divide-se nas sub-tarefas seguintes:
 *      - no princípio da sua actividade é acordado ao fim de um tempo aleatório e decide ir reparar o carro;
 *      - mete-se na fila da receção à espera de ser atendido;
 *      - Quando chega à sua vez fala com o manager  decide se quer ou não um carro de substituição; 
 *      - se quiser volta para casa de carro, caso contratio volta para casa de autocarro;
 *      - quando o carro está pronto o manger liga-lhe e ele volta à fila de espera para ser atendido;
 *      - paga o serviço, recolhe as chaves do seu carro e volta à sia vida normal;
 */

public class Customer extends Thread{
    /**
    *   Estado do customer
    *   @serialField entityState
    */
    private CustomerState entityState;
    
    /**
    *  Identificação do customer
    *
    *    @serialField customerId
    */
    
    private int customerId;
    
    /**
    *  Lounge
    *
    *    @serialField lounge
    */

    private iLounge lounge;
    
    
    /**
    *  Outside World
    *
    *    @serialField outside
    */

    private iOutsideWorld outside;
    
    /**
    *  Park
    *
    *    @serialField park
    */

    private iPark park;
    
    /**
    *  Want replacement car
    *
    *    @serialField wantRepCar
    */
    private boolean wantRepCar;
    
    /**
    *  Car Key
    *
    *    @serialField key
    */
    private int key;
    
    /**
    *  Thread manager instantiation.
    *
    *    costumerId identificação do customer
    *    @param customerId customerId
    *    @param park park
    *    @param lounge lounge
    *    @param outside outside world
    *    @throws java.rmi.RemoteException
    */

    public Customer (int customerId, iPark park, iLounge lounge, iOutsideWorld outside) throws RemoteException
    {
        this.customerId = customerId;
        this.park = park;
        this.lounge = lounge;
        this.outside = outside;
        Random r = new Random();
        this.wantRepCar = r.nextBoolean();
        
    }
    
    
    /**
    *  Customer thread's life cycle.
    */
    @Override
    public void run () 
    {
        try {
            this.entityState = CustomerState.NLWC;
            decideOnRepair();
            this.entityState = CustomerState.PARK;
            park.goToRepairShop(customerId);
            this.entityState = CustomerState.RECE;
            lounge.queueIn(customerId);
            lounge.talkWithManager(customerId, wantRepCar);
            if (wantRepCar){
                this.entityState = CustomerState.WFRC;
                key=lounge.collectKey(customerId);
                this.entityState = CustomerState.PARK;
                park.findCar(customerId, key);
                this.entityState = CustomerState.NLWC;
                outside.backToWorkByCar(customerId,false);
                this.entityState = CustomerState.PARK;
                park.goToRepairShop(customerId);
            }
            else
            {
                this.entityState = CustomerState.NLWO;
                outside.backToWorkByBus(customerId);
                
            }
            this.entityState = CustomerState.RECE;
            lounge.queueIn(customerId);
            lounge.payForService(customerId);
            this.entityState = CustomerState.PARK;
            park.collectCar(customerId);
            this.entityState = CustomerState.NLWC;
            outside.backToWorkByCar(customerId, true);
        } catch (RemoteException e) {
            GenericIO.writelnString("Remote Exception: " + e.getMessage());
            System.exit(1);
        }
        
    }
    
    /**
     * This function waits a random time for the customer to decide on repairing
     * the car.
     */
    private void decideOnRepair() 
    {
        try
        { 
            sleep ((long) (1 + 100 * Math.random ()));
        }
        catch (InterruptedException e) {}
    }

      
    /**
     * This function returns the Customer's current state.
     *
     * @return entityState current state of the Customer.
     * @throws java.rmi.RemoteException
     */
    public CustomerState getEntityState() throws RemoteException
    {
        return this.entityState;
    }

    /**
     * This function sets the Customer's state.
     *
     * @param entityState New Customer State.
     * @throws java.rmi.RemoteException
     */
    public void setEntityState(CustomerState entityState) throws RemoteException
    {
        this.entityState = entityState;
    }

    /**
     * This function returns the Key
     * 
     * @return key identifies the replace car
     * @throws java.rmi.RemoteException
     */
    public int getKey() throws RemoteException{
        return key;
    }

    /**
     * This function identifies if the customer wants replacement car
     * 
     * @return wantRepCar
     * @throws java.rmi.RemoteException
     */
    public boolean isWantRepCar() throws RemoteException{
        return wantRepCar;
    }
    
    
    
}