package MainPackage;


import genclass.GenericIO;

import Interfaces.*;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


/**
 * Customer's main class.
 * 
 */

public class Main
{
    
    public static void main(String[] args) throws IOException
    {
        
        String rmiRegHostName = SimParameters.REGISTRY_HOST_NAME;
        int rmiRegPortNumb = SimParameters.REGISTRY_PORT;

        Customer [] cust;
        
       
        
        iLogger logI = null;
        iLounge lgI = null;
        iOutsideWorld owI = null;
        iPark pkI = null;
        
        if (System.getSecurityManager () == null)
            System.setSecurityManager (new SecurityManager ());
        GenericIO.writelnString ("Security manager was installed!");
        

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            logI = (iLogger) registry.lookup(SimParameters.LOGGER_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating repository: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Repository is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            lgI = (iLounge) registry.lookup(SimParameters.LOUNGE_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating lounge: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Lounge is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            owI = (iOutsideWorld) registry.lookup(SimParameters.OUTSIDE_WORLD_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating Outside World: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Outside World is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            pkI = (iPark) registry.lookup(SimParameters.PARK_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating Park: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Park is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }


        GenericIO.writelnString ("Starting customers...");
        int n; 
        cust = new Customer[SimParameters.N_CUSTOMERS];
        for(n=0; n<SimParameters.N_CUSTOMERS; n++)
        {
            cust[n] = new Customer(n, pkI, lgI, owI);
        }

        int j;
        for(j = 0; j<SimParameters.N_CUSTOMERS;j++)
            cust[j].start();

        int i;
        for(i = 0; i<SimParameters.N_CUSTOMERS;i++)
        {
            try {
                cust[i].join();
            } catch (InterruptedException e) {}
        }
        
        GenericIO.writelnString ("Customers ended lifecycle.");
        
        /*try {
            logI.serviceEnd();
            //lgI.serviceEnd();
            owI.serviceEnd();
            pkI.serviceEnd();
        } catch (RemoteException ex) {
            System.out.println("Exception thrown while calling service end: " + ex.getMessage () );
            ex.printStackTrace();
            System.exit (1);
        }*/
                
    }
}