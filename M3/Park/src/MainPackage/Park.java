package MainPackage;

import EntitiesStates.*;
import Interfaces.iLogger;
import Interfaces.iPark;
import java.rmi.RemoteException;


/**
 * 
 * Shared region - Park
 */

public class Park implements iPark{
    
    /**
     * Reference to the General Information
     * 
     * @serialField logger
     */
    private iLogger logger; 
    
    
    /**
     * Array to Customers cars.
     * 
     * @serialFiel cars[]
     */
    private boolean [] cars;

    /**
     * Array to Replacement cars.
     * 
     * @serialField carsRep[]
     */
    private int [] carsRep;
    
    /**
     * Which part needs to be replaced.
     * 
     * @serialField part
     */
    private int part;
    
   /**
     * Cart part id
     * 
     * @serialField carPartId
     */
    private int[] carPartId;
    
    
    /**
    * Init the Park.
    *
    * @param repository Instance that implements GeneralInformation methods.
    */        
    public Park(iLogger repository){
        logger = repository;
        cars = new boolean [SimParameters.N_CUSTOMERS];
        carsRep = new int [SimParameters.N_REPLACEMENT_CARS];
        part=-1;
        carPartId = new int[SimParameters.N_CUSTOMERS];
        for (int i = 0; i < SimParameters.N_CUSTOMERS; i++) 
        {
            cars[i]=false;
            carPartId[i]=-1;
        }
        
        for (int i = 0; i < SimParameters.N_REPLACEMENT_CARS; i++) 
        {
            carsRep[i]=-1;
        }
    }
    
    /**
     * Customer go to the repair shop and Park the car - his own or the replace car.
     * 
     * @param customerID Identifies the Customer car. 
     */
    @Override
    public synchronized void goToRepairShop(int customerID) throws RemoteException
    {
        
        
        
        if(!cars[customerID]){
            logger.setParkedVehicles(false);
        }
        
        logger.setCustomerState(customerID, CustomerState.PARK);
        for(int i = 0; i < SimParameters.N_REPLACEMENT_CARS; i++)
        {
            if(carsRep[i] == customerID) 
                carsRep[i] = -1;
            else  
                cars[customerID] = true;
        }
    }
    
  
    /**
     * Customer WFRCs the replace car.
     * 
     * @param customerID Identifies the Customer car.
     * @param key Identifies the replace car.
     */
    @Override
    public synchronized void findCar(int customerID, int key) throws RemoteException
    {
        
        
        logger.decrementNWaitingRepl(false);
        logger.setCustomerState(customerID, CustomerState.PARK);
        
        for(int i = 0; i < SimParameters.N_REPLACEMENT_CARS; i++)
        {
            if(i == key)
                carsRep[i] = customerID;
        }   
    }
    
    /**
     * Customer removes his repaired car from the Park. 
     * 
     * @param customerID Identifies the Customer car. 
    */
    @Override
    public synchronized void collectCar(int customerID) throws RemoteException
    {
        
        logger.setCustomerState(customerID, CustomerState.PARK);
        cars[customerID]=false;
        notifyAll();
    }
    
    
    /**
     * Mechanic goes to the park to get the car and verifies which part needs to be replaced.  
     * 
     * @param mechanicID Identifies the Customer car.
     * @param customerID Identifies the Mechanic.
     * @return Returns the part to be replaced.
     */
    @Override
    public synchronized int getVehicle(int mechanicID, int customerID) throws RemoteException
    {
        
       
        logger.setParkedVehicles(true);
        logger.setMechanicState(mechanicID, MechanicState.FIXI);
        if (carPartId[customerID]>=0)
            return carPartId[customerID];
                
        double a;
        cars[customerID] = false;
        a = (Math.random() * ((3 - 0) + 1)) + 0;
        if(a < 1)
            part = 0;
        else if(a > 1 && a < 2)
            part = 1;
        else
            part = 2;
        
        carPartId[customerID]=part;
        return part; 
    }
    
    /**
     * The Mechanic returns the Customer car repared to the Park.  
     * 
     * @param customerID Identifies the Customer car.
     */
    @Override
    public synchronized void returnVehicle(int customerID) throws RemoteException
    {
        logger.setParkedVehicles(false);
        logger.setNRepaired(customerID);
        cars[customerID] = true;
    }
    
    /**
     * Stop the service and shut down the betting center.
     * @throws java.rmi.RemoteException
     */
    @Override
    public synchronized void serviceEnd() throws RemoteException{
        Main.serviceEnd = true;
        notifyAll();
    }
}

