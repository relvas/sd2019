package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * Park Interface
 */
public interface iPark extends Remote{
    
    /**
     * Customer go to the repair shop and park the car - his own or the replace car.
     * 
     * @param customerID Identifies the Customer car. 
     * @throws java.rmi.RemoteException 
     */
    public void goToRepairShop(int customerID) throws RemoteException;
    
    /**
     * Customer finds the replace car.
     * 
     * @param customerID Identifies the Customer car.
     * @param key Identifies the replace car.
     * @throws java.rmi.RemoteException
     */
    public void findCar(int customerID,int key) throws RemoteException;
    
    /**
     * Customer removes his repaired car from the Park. 
     * 
     * @param customerID Identifies the Customer car. 
     * @throws java.rmi.RemoteException 
     */
    public void collectCar(int customerID) throws RemoteException;
    
    /**
     * Mechanic goes to the park to get the car and verifies which part needs to be replaced  
     * 
     * @param mechanicID Identifies the Customer car.
     * @param customerID Identifies the Mechanic.
     * @return Returns the part to be replaced.
     * @throws java.rmi.RemoteException
     */
    public int getVehicle(int mechanicID, int customerID) throws RemoteException;
    
    /**
     * The Mechanic returns the Customer car repared to the Park.  
     * 
     * @param customerID Identifies the Customer car.
     * @throws java.rmi.RemoteException
     */
    public void returnVehicle(int customerID) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    

}
