package Interfaces;

import EntitiesStates.CustomerState;
import EntitiesStates.MechanicState;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * Logger Interface
 */
public interface iLogger extends Remote{
    
    public void setParkedVehicles(boolean remove) throws RemoteException;

    public void setCustomerState(int cId, CustomerState cState) throws RemoteException;

    public void decrementNWaitingRepl(boolean log) throws RemoteException;
                
    public void setMechanicState(int mId, MechanicState mState) throws RemoteException;

    public void setNRepaired(int cId) throws RemoteException;

    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    
 
}
