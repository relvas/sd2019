package MainPackage;


import genclass.GenericIO;

import Interfaces.*;
import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


/**
 * Mechanic's main class.
 * 
 */

public class Main
{
    public static void main(String[] args) throws IOException
    {
       
        String rmiRegHostName = SimParameters.REGISTRY_HOST_NAME;
        int rmiRegPortNumb = SimParameters.REGISTRY_PORT;

        Mechanic [] mec;
        
        iLogger logI = null;
        iLounge lgI = null;
        iRepairArea reI = null;
        iPark pkI = null;
        
        if (System.getSecurityManager () == null)
            System.setSecurityManager (new SecurityManager ());
        GenericIO.writelnString ("Security manager was installed!");
        

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            logI = (iLogger) registry.lookup(SimParameters.LOGGER_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating repository: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Repository is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            lgI = (iLounge) registry.lookup(SimParameters.LOUNGE_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating lounge: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Lounge is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            reI = (iRepairArea) registry.lookup(SimParameters.REPAIR_AREA_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating Outside World: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Outside World is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }

        try {
            Registry registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
            pkI = (iPark) registry.lookup(SimParameters.PARK_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Exception thrown while locating Park: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException ex) {
            GenericIO.writelnString("Park is not registered: " + ex.getMessage() + "!");
            System.exit(1);
        }

        GenericIO.writelnString ("Starting mechanics...");
        int n; 
        mec = new Mechanic[SimParameters.N_MECHANICS];
        for(n=0; n<SimParameters.N_MECHANICS; n++)
        {
            mec[n] = new Mechanic(n, pkI, lgI, reI);
        }

        int j;
        for(j = 0; j<SimParameters.N_MECHANICS;j++)
            mec[j].start();

        int i;
        for(i = 0; i<SimParameters.N_MECHANICS;i++)
        {
            try {
                mec[i].join();
            } catch (InterruptedException e) {}
        }
        
        GenericIO.writelnString ("Mechanics ended lifecycle.");
        
        try {
            //logI.serviceEnd();
            //lgI.serviceEnd();
            //reI.serviceEnd();
            pkI.serviceEnd();
        } catch (RemoteException ex) {
            System.out.println("Exception thrown while calling service end: " + ex.getMessage () );
            System.exit (1);
        }
                
    }
}