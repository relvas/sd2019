package MainPackage;

import EntitiesStates.*;
import Interfaces.*;
import genclass.GenericIO;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *  Este tipo de dados implementa o thread mecânico.<p>
 *  A sua vida divide-se por duas tarefas principais, ler o jornal e trabalhar, que são realizadas sequencialmente.<p>
 *  Nesta solução, trabalhar supõe o desenvolver das sub-tarefas seguintes:
 *      - no princípio da sua actividade é acordado pelo manager e vai buscar um carro ao parque;
 *      - verifica se existe a peça disponível para efeturar a reparação;
 *      - caso exista começa a reparação;
 *      - caso não exista avisa o manager e volta a ler o jornal;
 *      - ao terminar a reparação leva o carro até ao parque e volta a ler o jornal;
 */

public class Mechanic extends Thread {
    
    /**
    *   Estado do mecanico
    * 
    *   @serialField entityState
    */
    private MechanicState entityState;
    
    
   /**
   *  Identificação do mecanico
   *
   *    @serialField mechanicId
   */

   private int mechanicId;
   
   
   
  /**
   *  Parque
   *
   *    @serialField park
   */

   private iPark park;
   
   
  /**
   *  Lounge
   *
   *    @serialField lounge
   */

   private iLounge lounge;
   
   
   
  /**
   *  Repair Area
   *
   *    @serialField repair
   */

   private iRepairArea repair;
   
   /**
   *  Instanciação do thread mecânico.
   *
   *    @param mechanicId identificação do mecanico
   *    @param park parque
   *    @param lounge lounge
   *    @param repair repair area
   */

   public Mechanic (int mechanicId, iPark park, iLounge lounge, iRepairArea repair)
   {
      this.mechanicId = mechanicId;
      this.park = park;
      this.lounge = lounge;
      this.repair = repair;
   }
   
   /**
   *  Ciclo de vida do thread mecânico.
   */

   @Override
   public void run ()
   {
      boolean test;
      try {
      while(true)                                 
      {
          
              entityState = MechanicState.WAIT;
              test=repair.readThePaper(mechanicId);
              if(!test) break;
              int cId;
              int pId;
              entityState = MechanicState.FIXI;
              cId=repair.startRepairProcedure(mechanicId);
              if(cId==-1){
                  continue; 
              }
              pId=park.getVehicle(mechanicId, cId);
              entityState = MechanicState.CHEC;
              repair.getRequiredPart(mechanicId, cId, pId);
              int pId2=repair.partAvailable(cId, pId);
              if (pId2<0)
              {
                  if (pId2==-1) {
                      entityState = MechanicState.ALLM;
                      lounge.letManagerKnow(mechanicId, pId);
                      continue;
                  }
                  else{
                      entityState = MechanicState.ALLM;
                      lounge.letManagerKnow(mechanicId, -2);
                      continue;
                  }
              }
              else
              {
                  entityState = MechanicState.FIXI;
                  repair.resumeRepairProcedure(mechanicId, cId, pId);
              }
              entityState = MechanicState.FIXI;
              repair.fixIt(mechanicId, cId, pId);          
              park.returnVehicle(cId);
              entityState = MechanicState.WAIT;
              lounge.repairConcluded(mechanicId, cId);
          
      }
      } catch (RemoteException e) {
                GenericIO.writelnString("Remote Exception: " + e.getMessage());
                System.exit(1);
          }
      
   }
  

   /**
     * This funtions returns the Mechanic's actual state.
     *
     * @return entityState Actual state of the Mechanic.
     * @throws java.rmi.RemoteException
     */
    public MechanicState getEntityState() throws RemoteException
    {
        return entityState;
    }

    /**
     * This funtions sets the Mechanic's state.
     *
     * @param entityState Next state to set the Mechanic.
     * @throws java.rmi.RemoteException
     */
    public void setEntityState(MechanicState entityState) throws RemoteException
    {
        this.entityState = entityState;
    }
    
    /**
     * This functions returns the Mechanic ID
     * 
     * @return mechanicId
     * @throws java.rmi.RemoteException
     */
    public int getMechId() throws RemoteException{
        return mechanicId;
    }
}
