package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *  Interface - Mechanic Repair Area
 */

public interface iRepairArea extends Remote{
    
    /**
    * Read The Paper (originated by the mechanic).
    * 
    * @param mechanicID Mechanic identifier
     * @throws java.rmi.RemoteException
    * 
    */
    public boolean readThePaper(int mechanicID) throws RemoteException;
    
    /**
    * Start repair procedure.
    * 
    * @param mechanicId Mechanic identifier.
    * @return Identifies the Customer car.
     * @throws java.rmi.RemoteException
    */
    public int startRepairProcedure(int mechanicId) throws RemoteException;
    
    /**
    * Fix the car.
    * 
    * @param mechanicID Mechanic identifier
    * @param customerID Customer identifier
    * @param pID Part identifier
     * @throws java.rmi.RemoteException
    */
    public void fixIt(int mechanicID, int customerID, int pID) throws RemoteException;
    
    /**
    * Get Required Part  
    * 
    * @param mechanicID Mechanic identifier
    * @param customerID Customer identifier
    * @param pID Identifies the part.
     * @throws java.rmi.RemoteException
    */
    public void getRequiredPart(int mechanicID, int customerID, int pID) throws RemoteException;
    
    /**
    * Part available
    * 
    * @param customerID Identifies the Customer.
    * @param pID Part needed to fix the car.
    * @return True if the part is available or false if theres isn't.
     * @throws java.rmi.RemoteException
    */
    public int partAvailable(int customerID, int pID) throws RemoteException;
    
    /**
    * Resume Repair Procedure
    * 
    * @param mechanicID Mechanic identifier
    * @param customerID Customer identifier
    * @param pID Part identifier
     * @throws java.rmi.RemoteException
    */
    public void resumeRepairProcedure(int mechanicID, int customerID, int pID) throws RemoteException;
    
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
}
