package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *  Interface - Mechanic Park
 */

public interface iPark extends Remote{
    
    /**
     * Mechanic goes to the park to get the car and verifies which part needs to be replaced  
     * 
     * @param mechanicID Identifies the Customer car.
     * @param customerID Identifies the Mechanic.
     * @return Returns the part to be replaced.
     * @throws java.rmi.RemoteException
     */
    public int getVehicle(int mechanicID, int customerID) throws RemoteException;
    
    /**
     * The Mechanic returns the Customer car repared to the Park.  
     * 
     * @param customerID Identifies the Customer car.
     */
    public void returnVehicle(int customerID) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    
}
