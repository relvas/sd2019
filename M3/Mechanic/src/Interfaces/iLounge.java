package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface - Mechanic Lounge
 */

public interface iLounge extends Remote{
    
    /**
    * The Mechanic let the Manager know we need to replace a part.
    * 
    * @param mechanicID Identifies the Mechanic.
    * @param part Identifies the part needed.
     * @throws java.rmi.RemoteException
    */
    public void letManagerKnow(int mechanicID, int part) throws RemoteException;
    
    /**
    * The Mechanic let the Manager knows that the car from customerID is ready.
    * 
    * @param mechanicID Identifies the Mechanic.
    * @param customerID Identifies the Customer car.
     * @throws java.rmi.RemoteException
    */
    public void repairConcluded(int mechanicID, int customerID) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    
}
