bold=$(tput bold)
normal=$(tput sgr0)
echo "${bold}*** Script de Limpeza das Máquinas ***${normal}"

export SSHPASS='3prud3nm'


###

echo -e "\n${bold}->${normal} A eliminar Logger da máquina ${bold}1${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws01.ua.pt << EOF
	rm -rf RMI
	rm -rf Logger
	cd Public
	rm -rf registry
	rm -rf logger
	killall java
	killall rmiregistry
EOF

echo -e "\n${bold}->${normal} A eliminar Lounge da máquina ${bold}2${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws02.ua.pt << EOF
	rm -rf Lounge
	cd Public
	rm -rf classes
	killall java
	killall rmiregistry
EOF

echo -e "\n${bold}->${normal} A eliminar Outside World da máquina ${bold}3${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws03.ua.pt << EOF
	rm -rf OutsideWorld
	cd Public
	rm -rf classes
	killall java
	killall rmiregistry
EOF

echo -e "\n${bold}->${normal} A eliminar Park da máquina ${bold}4${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws04.ua.pt << EOF
	rm -rf Park
	cd Public
	rm -rf classes
	killall java
	killall rmiregistry
EOF

echo -e "\n${bold}->${normal} A eliminar Repair Area da máquina ${bold}5${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws05.ua.pt << EOF
	rm -rf RepairArea
	cd Public
	rm -rf classes
	killall java
	killall rmiregistry
EOF

echo -e "\n${bold}->${normal} A eliminar SupplierSite da máquina ${bold}6${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws06.ua.pt << EOF
	rm -rf SupplierSite
	cd Public
	rm -rf classes
	killall java
	killall rmiregistry
EOF

echo -e "\n${bold}->${normal} A eliminar Customers da máquina ${bold}7${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws07.ua.pt << EOF
	rm -rf Customer
	cd Public
	rm -rf classes
	killall java
	killall rmiregistry
EOF

echo -e "\n${bold}->${normal} A eliminar Mechanics da máquina ${bold}8${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws08.ua.pt << EOF
	rm -rf Mechanic
	cd Public
	rm -rf classes
	killall java
	killall rmiregistry
EOF

echo -e "\n${bold}->${normal} A eliminar Manager da máquina ${bold}9${normal}"
sshpass -e ssh -o StrictHostKeyChecking=no sd0407@l040101-ws09.ua.pt << EOF
	rm -rf Manager
	cd Public
	rm -rf classes
	killall java
	killall rmiregistry
EOF

