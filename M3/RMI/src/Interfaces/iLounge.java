package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * Lounge Interface
 */
public interface iLounge extends Remote{
    
     /**
     * Customer is waiting for is turn to talk with the Manager.   
     * 
     * @param customerID Identifies the Customer.
     * @throws java.rmi.RemoteException
     */
    public void queueIn(int customerID) throws RemoteException;
    
    /**
     * Talk with the Manager.  
     * 
     * @param customerID Identifies the Customer
     * @param wantRepCar Identifies if a replace car is needed.
     * @throws java.rmi.RemoteException
     */
    public void talkWithManager(int customerID, boolean wantRepCar) throws RemoteException;
    
    /**
     * The Customer collects the key of the replaced car.  
     * 
     * @param customerID Identifies the Customer.
     * @return key Identifies the replaced car.
     */
    public int collectKey(int customerID) throws RemoteException;
    
    
    /**
    * Pay for Service.  
    * @param customerID Identifies the Customer.
     * @throws java.rmi.RemoteException
    */
    public void payForService(int customerID) throws RemoteException;
    
     /**
    * Get next task (originated by the manager).
    * 
     * @return 
     * @throws java.rmi.RemoteException
    */
    public boolean getNextTask() throws RemoteException;
    
    /**
    * The Manager checks the next task.
    * 
    * @return Situation Reference to whats next
     * @throws java.rmi.RemoteException
    */
    public Situation appraiseSit() throws RemoteException;
    
    /**
    * The Manager talks to the Customer.
    * 
    * @return Action What the customer wants
     * @throws java.rmi.RemoteException
    */
    public Action talkToCustomer() throws RemoteException;
    
    /**
     * The Manager checks if there is a replacement car available and if there is a key is delivery to the Customer.  
     * 
     * @param customerID Identifies the Customer.
     * @throws java.rmi.RemoteException
     */
    public void handCarKey(int customerID) throws RemoteException;
    
    /**
     * Manager checks if the Customer have a replace car.  
     * 
     * @throws java.rmi.RemoteException
     */ 
    public void receivePayment() throws RemoteException;
    
    /**
     * Get part
     * 
     * @return Part
     * @throws java.rmi.RemoteException
     */
    public int getPartId() throws RemoteException;
    
     /**
     * Get customer
     * 
     * @return customer
     * @throws java.rmi.RemoteException
     */
    public int getCId() throws RemoteException;
    
    
    /**
     * Set Flag restocked
     * 
     * @param partId Identifies the part
     * @throws java.rmi.RemoteException
     */
    public void setFlagRestocked(int partId) throws RemoteException;
    
     /**
     * Get Phone
     * 
     * @return phone
     * @throws java.rmi.RemoteException
     */
    public int getPhoneId() throws RemoteException;
    
    /**
    * The Mechanic let the Manager know we need to replace a part.
    * 
    * @param mechanicID Identifies the Mechanic.
    * @param part Identifies the part needed.
     * @throws java.rmi.RemoteException
    */
    public void letManagerKnow(int mechanicID, int part) throws RemoteException;
    
    /**
    * The Mechanic let the Manager knows that the car from customerID is ready.
    * 
    * @param mechanicID Identifies the Mechanic.
    * @param customerID Identifies the Customer car.
     * @throws java.rmi.RemoteException
    */
    public void repairConcluded(int mechanicID, int customerID) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    
}
