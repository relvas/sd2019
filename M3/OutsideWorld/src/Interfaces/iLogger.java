package Interfaces;

import EntitiesStates.CustomerState;
import EntitiesStates.ManagerState;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * Interface Logger
 */
public interface iLogger extends Remote{
    
    public void setParkedVehicles(boolean remove) throws RemoteException;
    
    public void setCustCarDriven(int cId, int repId, boolean secondTime) throws RemoteException;
 
    public void setCustomerState(int cId, CustomerState cState) throws RemoteException;
    
    public void setManagerState(ManagerState mState) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    

}
