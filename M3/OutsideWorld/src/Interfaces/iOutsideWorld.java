package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 
 * Interface OutsideWorld
 */
public interface iOutsideWorld extends Remote{
    
    /**
     * Back to work by car (originated by the customer).
     * 
     * @param customerID Identifier of the customer.
     * @param owncar Identifies the car driven by the customer.
     * @throws java.rmi.RemoteException
     */
    public void backToWorkByCar(int customerID, boolean owncar) throws RemoteException;
    
    /**
     * Back to work by bus (originated by the customer).
     * 
     * @param customerID Identifier of the customer    
     * @throws java.rmi.RemoteException    
     */
    public void backToWorkByBus(int customerID) throws RemoteException;
    
    /**
     * Manager phones to the Customer - car fixed.
     *   
     * @param customerID Identifies the Customer.
     * @throws java.rmi.RemoteException
     */
    public void  phoneCustomer(int customerID) throws RemoteException;
    
    /**
     * Stop the service and shuts down the shared region.
     * @throws java.rmi.RemoteException
     */
    public void serviceEnd() throws RemoteException;
    
    
}
