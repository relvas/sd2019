package MainPackage;

import EntitiesStates.*;
import Interfaces.iLogger;
import Interfaces.iOutsideWorld;
import java.rmi.RemoteException;

/**
 * 
 * Shared region - Outside World
 */

public class OutsideWorld implements iOutsideWorld{
    
    private iLogger logger;
    boolean []carsReady; 
    int finished = 0;
    public OutsideWorld(iLogger repository){
        logger = repository;
        
        carsReady = new boolean[SimParameters.N_CUSTOMERS];
        for (int i = 0; i <SimParameters.N_CUSTOMERS ; i++) 
        {
            carsReady[i]=false;
        }
        
    }
    
    /**
     * Back to work by car (originated by the customer).
     * 
     * @param customerID Identifier of the customer 
     */
    @Override
    public synchronized void backToWorkByCar(int customerID, boolean owncar) throws RemoteException
    {
        if(owncar)
        {
            logger.setParkedVehicles(true);
            logger.setCustCarDriven(customerID,-1,true);
        }
        else{
            //logger.setCustCarDriven(customerID, ((Customer) Thread.currentThread()).getKey(), false);
        }  
        
        logger.setCustomerState(customerID, CustomerState.NLWC);        
        while (!carsReady[customerID]) {
            try {
                wait();
            } catch (InterruptedException e){}
        } 
    }
   
    /**
     * Manager phones to the Customer - car fixed.
     *   
     * @param customerID Identifies the Customer.
     */
    @Override
    public synchronized void phoneCustomer(int customerID) throws RemoteException {
        
        logger.setManagerState(ManagerState.ALLC);

        carsReady[customerID] = true;
        notifyAll();
    }
    
    /**
     * Back to work by bus (originated by the customer).
     * 
     * @param customerID Identifier of the customer    
     */
    @Override
    public synchronized void backToWorkByBus(int customerID) throws RemoteException{
        logger.setCustCarDriven(customerID, -1, false);
        logger.setCustomerState(customerID, CustomerState.NLWO);
        
        while (!carsReady[customerID]) {
            try {
                wait();
            } catch (InterruptedException e){}
        }
        
    } 
    
    /**
     * Stop the service and shut down the betting center.
     * @throws java.rmi.RemoteException
     */
    public synchronized void serviceEnd() throws RemoteException{
        Main.serviceEnd = true;
        notifyAll();
    }
}
