package MainPackage;

import Interfaces.Register;
import genclass.GenericIO;
import Interfaces.iLogger;
import Interfaces.iOutsideWorld;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Starts the OutsideWorld
 * 
 */
public class Main
{
    /**
     * Used to check if the service must terminate.
     */
    public static boolean serviceEnd = false;
    
    public static void main(String[] args)
    {

        String rmiRegHostName = SimParameters.REGISTRY_HOST_NAME;
        int rmiRegPortNumb = SimParameters.REGISTRY_PORT;
        
        String nameEntryBase = SimParameters.REGISTRY_NAME_ENTRY;
        String nameEntryObject = SimParameters.OUTSIDE_WORLD_NAME_ENTRY;
        Registry registry = null;
        Register iregister = null;

        iLogger ilog = null;
        
        if (System.getSecurityManager () == null)
            System.setSecurityManager (new SecurityManager ());
        GenericIO.writelnString ("Security manager was installed!");
        
        
        try {
            registry = LocateRegistry.getRegistry(rmiRegHostName, rmiRegPortNumb);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção na criação do registo RMI: " + e.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O registo RMI foi criado!");

        try {
           
            ilog = (iLogger) registry.lookup(SimParameters.LOGGER_NAME_ENTRY);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção na localização do log: " + e.getMessage() + "!");
            System.exit(1);
        } catch (NotBoundException e) {
            GenericIO.writelnString("Log não está registado: " + e.getMessage() + "!");
            System.exit(1);
        }

        
        iOutsideWorld owI = null;
        OutsideWorld ow = new OutsideWorld(ilog);
        try {
            owI = (iOutsideWorld) UnicastRemoteObject.exportObject((Remote) ow, SimParameters.OUTSIDE_WORLD_PORT);
        } catch (RemoteException ex) {
            GenericIO.writelnString("Excepção na geração do stub para o control: " + ex.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O stub para o OutsideWorld foi gerado!");

       
        

        try {
            iregister = (Register) registry.lookup(nameEntryBase);
        } catch (RemoteException e) {
            GenericIO.writelnString("RegisterRemoteObject lookup OutsideWorld: " + e.getMessage());
            System.exit(1);
        } catch (NotBoundException e) {
            GenericIO.writelnString("RegisterRemoteObject not bound OutsideWorld: " + e.getMessage());
            System.exit(1);
        }

        try {
            iregister.bind(nameEntryObject, (Remote) owI);
        } catch (RemoteException e) {
            GenericIO.writelnString("Excepção no registo do OutsideWorld: " + e.getMessage());
            System.exit(1);
        } catch (AlreadyBoundException ex) {
            GenericIO.writelnString("O OutsideWorld já está registado: " + ex.getMessage());
            System.exit(1);
        }
        GenericIO.writelnString("O OutsideWorld foi registado!");
        
        
        /* Wait for the service to end */
        while(!serviceEnd){
            try {
                synchronized(ow){
                    ow.wait();
                }
            } catch (InterruptedException ex) {
                GenericIO.writelnString("Main thread of OutsideWorld was interrupted.");
                System.exit(1);
            }
        }
        
        GenericIO.writelnString("OutsideWorld finished execution.");
        
        
         /* Unregister shared region */
        try
        { iregister.unbind (nameEntryObject);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("OutsideWorld unregistration exception: " + e.getMessage ());
          System.exit (1);
        } catch (NotBoundException ex) {
          GenericIO.writelnString ("OutsideWorld unregistration exception: " + ex.getMessage ());
          System.exit (1);
        }
        GenericIO.writelnString ("OutsideWorld object was unregistered!");
        
        /* Unexport shared region */
        try
        { UnicastRemoteObject.unexportObject (ow, false);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("OutsideWorld unexport exception: " + e.getMessage ());
          System.exit (1);
        }
        
        GenericIO.writelnString ("OutsideWorld object was unexported successfully!");

    }
}