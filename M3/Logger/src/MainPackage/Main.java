package MainPackage;

import Interfaces.*;
import genclass.GenericIO;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;


/**
 * Main Logger program.
 * Registers the logger in the register server and waits for connections
 */
public class Main {
    
    /**
     * Used to check if the service must terminate.
     */
    public static boolean serviceEnd = false;
    
    /**
     * Main control center launcher
     * @param args args
     */
    public static void main(String [] args){
        
        /* get location of the registry service */
        String rmiRegHostName = SimParameters.REGISTRY_HOST_NAME;
        int rmiRegPortNumb = SimParameters.REGISTRY_PORT;
        
        String nameEntryBase = SimParameters.REGISTRY_NAME_ENTRY;
        String nameEntryObject = SimParameters.LOGGER_NAME_ENTRY;
        Registry registry = null;
        Register iregister = null;
        
        /* create and install the security manager */ 
        if (System.getSecurityManager () == null)
            System.setSecurityManager (new SecurityManager ());
        GenericIO.writelnString ("Security manager was installed!");
        
        
        /* Get the RMI server registry */
        try
        { registry = LocateRegistry.getRegistry (rmiRegHostName, rmiRegPortNumb);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("RMI registry locate exception: " + e.getMessage ());
          System.exit (1);
        }

        
        /**
         * Shared region initialization.
         */
        
        Logger logger = new Logger();
        iLogger loggerInt = null;
        
        
        /**
         * Export logger to the registry
         */
        try
        { loggerInt = (iLogger) UnicastRemoteObject.exportObject ((Remote)logger, SimParameters.LOGGER_PORT);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("Logger stub generation exception: " + e.getMessage ());
          System.exit (1);
        }
        
        /* register it with the general registry service */
        try
        {   
            iregister = (Register) registry.lookup (nameEntryBase);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("Register lookup exception: " + e.getMessage ());
          System.exit (1);
        }
        catch (NotBoundException e)
        { GenericIO.writelnString ("Register not bound exception: " + e.getMessage ());
          System.exit (1);
        }
        
        try
        {   
            iregister.bind (nameEntryObject, loggerInt);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("Logger registration exception: " + e.getMessage ());
          System.exit (1);
        }
        catch (AlreadyBoundException e)
        { GenericIO.writelnString ("Logger already bound exception: " + e.getMessage ());
          System.exit (1);
        }
        GenericIO.writelnString ("Logger object was registered!");
        
        /* Wait for the service to end */
        while(!serviceEnd){
            try {
                synchronized(logger){
                    logger.wait();
                }
            } catch (InterruptedException ex) {
                GenericIO.writelnString("Main thread of logger was interrupted.");
                System.exit(1);
            }
        }
        
        GenericIO.writelnString("Logger finished execution.");
        
        /* Unregister shared region */
        try
        { iregister.unbind (nameEntryObject);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("Logger unregistration exception: " + e.getMessage ());
          System.exit (1);
        } catch (NotBoundException ex) {
          GenericIO.writelnString ("Logger unregistration exception: " + ex.getMessage ());
          System.exit (1);
        }
        GenericIO.writelnString ("Logger object was unregistered!");
        
        /* Unexport shared region */
        try
        { UnicastRemoteObject.unexportObject ((Remote)logger, false);
        }
        catch (RemoteException e)
        { GenericIO.writelnString ("Logger unexport exception: " + e.getMessage ());
          System.exit (1);
        }
        
        GenericIO.writelnString ("Logger object was unexported successfully!");
        
    }
    
}