package MainPackage;


import genclass.GenericIO;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import EntitiesStates.*;
import Interfaces.iLogger;
import java.rmi.RemoteException;
 /**
  * 
  * Logger class
  */
public class Logger implements iLogger{
    /**
     * @serialField fName filename
     */
    private String fName = "log.txt";
    /**
     * @serialField fWriter file writer object
     */
    private FileWriter fWriter;
    /**
     * @serialField managerState manager state
     */
    private ManagerState managerState;              //Stat
    /**
     * @serialField mechanicState Mech state array
     */
    private MechanicState[] mechanicState;          //St[0:1]
    /**
     * @serialField customerState customer state array
     */
    private CustomerState[] customerState;          //S[0:29]
    /**
     * @serialField customerVehicleDriven Vehicle driven array
     */
    private String[] customerVehicleDriven;         //cID, replacementID, or '-'
    /**
     * @serialField customerRequiresReplacement T or F replacement car array
     */
    private String[] customerRequiresReplacement;   //T or F
    /**
     * @serialField customerVehicleRepaired car repaired array
     */
    private String[] customerVehicleRepaired;       //T or F
    
    /* LOUNGE */
    /**
     * @serialField inQueueSize InQ size
     */
    private int inQueueSize;                        //InQ
    /**
     * @serialField nWaitingReplacement WtK
     */
    private int nWaitingReplacement;                //WtK
    /**
     * @serialField nRepairedVehicles NRV
     */
    private int nRepairedVehicles;                  //NRV
    
    /* PARK */
    /**
     * @serialField nParkedCustVehicles NCV
     */
    private int nParkedCustVehicles;                //NCV
    /**
     * @serialField nAvailableReplacementVehicles NPV
     */
    private int nAvailableReplacementVehicles;      //NPV
    
    
    /* REPAIR AREA */
    /**
     * @serialField nServiceRequests NSRQ
     */
    private int nServiceRequests;                   //NSRQ
    /**
     * @serialField  nAvailablePart0 Prt0
     */
    private int nAvailablePart0;                    //Prt0
    /**
     * @serialField nVehicWaitingPart0 NV0
     */
    private int nVehicWaitingPart0;                 //NV0
    /**
     * @serialField flagMissingPart0 S0
     */
    private char flagMissingPart0;                   //S0
    /**
     * @serialField nAvailablePart1 Prt1
     */
    private int nAvailablePart1;                    //Prt1
    /**
     * @serialField nVehicWaitingPart1 NV1
     */
    private int nVehicWaitingPart1;                 //NV1
    /**
     * @serialField flagMissingPart1 S1
     */
    private char flagMissingPart1;                   //S1
    
    /**
     * @serialField nAvailablePart2 Prt2
     */
    private int nAvailablePart2;                    //Prt2
    /**
     * @serialField nVehicWaitingPart2 NV2
     */
    private int nVehicWaitingPart2;                 //NV2
    /**
     * @serialField flagMissingPart2 S2
     */
    private char flagMissingPart2;                   //S2
    
    
    /* SUPPLIERS SITE  */
    /**
     * @serialField partPurchaseCounter0 PP0
     */
    private int partPurchaseCounter0;               //PP0
    /**
     * @serialField partPurchaseCounter1 PP1
     */
    private int partPurchaseCounter1;               //PP1
    /**
     * @serialField partPurchaseCounter2 PP2
     */
    private int partPurchaseCounter2;               //PP2
    
    
    
    /**
     * Constructor
     */
    public Logger(){
        this(null);
    }
        /**
         * Constructor
         * @param fName filename 
         */
    public Logger(String fName){
      
        if (fName!=null){
            if(!fName.equals("")) this.fName = fName;
        }
        try{
            this.fWriter = new FileWriter(new File(this.fName));
        }
        catch(IOException ex){
            GenericIO.writelnString("Exception a criar o FileWriter: \n");
            ex.printStackTrace();
        }
        
        //initial states
        this.managerState = ManagerState.CWTD;
        this.mechanicState = new MechanicState[2];
        this.mechanicState[0] = MechanicState.WAIT;
        this.mechanicState[1] = MechanicState.WAIT;
        this.customerState = new CustomerState[30];
        this.customerRequiresReplacement = new String[30];
        this.customerVehicleDriven = new String[30];
        this.customerVehicleRepaired = new String[30];
        for(int i=0;i<30;i++){
            this.customerState[i]=CustomerState.NLWC;
            this.customerVehicleDriven[i]=String.format("%4d",+i);
            this.customerRequiresReplacement[i]="   F";//TODO mudar 
            this.customerVehicleRepaired[i]="   F";
        }
        
        this.inQueueSize=0;                       //InQ
        this.nWaitingReplacement=0;                //WtK
        this.nRepairedVehicles=0;                  //NRV
        this.nParkedCustVehicles=0;                //NCV
        this.nAvailableReplacementVehicles=SimParameters.N_REPLACEMENT_CARS;      //NPV
        this.nServiceRequests=0;                   //NSRQ
        this.nAvailablePart0=SimParameters.N_PARTS;                    //Prt0
        this.nVehicWaitingPart0=0;                 //NV0
        this.flagMissingPart0='F';                   //S0
        this.nAvailablePart1=SimParameters.N_PARTS;                    //Prt1
        this.nVehicWaitingPart1=0;                 //NV1
        this.flagMissingPart1='F';                   //S1
        this.nAvailablePart2=SimParameters.N_PARTS;                    //Prt2
        this.nVehicWaitingPart2=0;                 //NV2
        this.flagMissingPart2='F';                   //S2
        this.partPurchaseCounter0=0;               //PP0
        this.partPurchaseCounter1=0;               //PP1
        this.partPurchaseCounter2=0;               //PP2
        
        
        
        
        
        logHeader();
        logToFile();
        
    }
    
    /**
     * Log a new line of states
     */
    private synchronized void logToFile(){
        //using a monospace font each char is the same size
        //line size = 215 chars
        String logStates="";
        
        //first header
        /*
        logStates+="\n MAN MECHANIC  "
                + new String(new char[80]).replace('\0', ' ')
                + "CUSTOMER\n";
        */
        //line 1
        logStates+= this.managerState
                + " " + this.mechanicState[0]
                + " " + this.mechanicState[1] + " ";
        
        int idx,k;
        for (idx=0; idx<10; idx++){
            //log first linee
            //logStates += String.format("%4d %4d %4d %4d ", this.customerState[idx],1,1,1);
            logStates += String.format("%s %s %s %s ", this.customerState[idx], 
                    this.customerVehicleDriven[idx], 
                    this.customerRequiresReplacement[idx], 
                    this.customerVehicleRepaired[idx]);
        }
        logStates+="\n";
        for (k=1;k<3;k++){
            //+offset+
            logStates+=new String(new char[15]).replace('\0', ' ');
            for(idx=0;idx<10;idx++){
                //line + \n
                logStates += String.format("%s %s %s %s ", this.customerState[10*k+idx], 
                    this.customerVehicleDriven[10*k+idx], 
                    this.customerRequiresReplacement[10*k+idx], 
                    this.customerVehicleRepaired[10*k+idx]);
            }
            logStates += "\n";
        }
         
     
        /*
            log the rest of the states
        */
        logStates += new String(new char[15]).replace('\0', ' ');
        logStates += String.format("%4d %4d %4d ",
                this.inQueueSize,                
                this.nWaitingReplacement,        
                this.nRepairedVehicles         
        );
        logStates += new String(new char[5]).replace('\0', ' ');
        logStates += String.format("%4d %4d ",           
                this.nParkedCustVehicles,          
                this.nAvailableReplacementVehicles
        );
        logStates += new String(new char[10]).replace('\0', ' ');
        logStates += String.format("%4d %4d %4d    %c %4d %4d "
                + "   %c %4d %4d    %c ",
                this.nServiceRequests,             
                this.nAvailablePart0,              
                this.nVehicWaitingPart0,           
                this.flagMissingPart0,             
                this.nAvailablePart1,              
                this.nVehicWaitingPart1,           
                this.flagMissingPart1,             
                this.nAvailablePart2,              
                this.nVehicWaitingPart2,           
                this.flagMissingPart2
        );
        logStates += new String(new char[30]).replace('\0', ' ');
        logStates += String.format("%4d %4d %4d  \n",             
                this.partPurchaseCounter0,         
                this.partPurchaseCounter1,         
                this.partPurchaseCounter2
        );
        
        
        //finally, attempt to write
        try{
            this.fWriter.write(logStates);
            this.fWriter.flush();
        }
        catch(IOException ex){
            GenericIO.writelnString("IO Exception attempting to logtoFile\n");
        }
        catch(NullPointerException ex){
            GenericIO.writelnString("No FileWriter\n");
        }
    }
    /**
     * close file
     */
    public synchronized void closeFile(){
        try{
            this.fWriter.flush();
            this.fWriter.close();
        }
        catch(IOException e){GenericIO.writelnString("IOExc closing");
        }
        catch(NullPointerException e){GenericIO.writelnString("null closing");
        }
    }
    
    
    /*
    *   Write the header to the log file.
    */
    private synchronized void logHeader(){
        //using a monospace font each char is the same size
        //line size = 215 chars
        
        String logStates="";
        
        //title
        logStates+="\n"
                + new String(new char[71]).replace('\0', ' ')
                + "REPAIR SHOP ACTIVITIES - Description of the internal "
                + "state of the problem\n"; //72 chars to center
        //first header
        logStates+="\n MAN MECHANIC  "
                + new String(new char[80]).replace('\0', ' ')
                + "CUSTOMER";
        
        //content
        //line 1
        logStates+="\nStat St0  St1  ";
        
        int idx;
        String idx_str;
        
        for (idx=0; idx<10; idx++){
            idx_str = String.valueOf(idx);
            logStates+="S0" + idx_str + "  ";
            logStates+="C0" + idx_str + "  ";
            logStates+="P0" + idx_str + "  ";
            logStates+="R0" + idx_str + "  ";   
        }
        logStates+="\n";
        
        //lines 2 and 3
        int k;
        String k_str;
        
        for (k=1;k<3;k++){
            //lines 2 and 3
            //offset 15 chars 
            logStates+=new String(new char[15]).replace('\0', ' ');
            k_str = String.valueOf(k);
            for (idx=0;idx<10;idx++){
                idx_str = String.valueOf(idx);
                logStates+="S" + k_str +""+ idx_str + "  ";
                logStates+="C" + k_str +""+ idx_str + "  ";
                logStates+="P" + k_str +""+ idx_str + "  ";
                logStates+="R" + k_str +""+ idx_str + "  ";
            }
            logStates+="\n";
        }
        //second header
        logStates+=new String(new char[19]).replace('\0', ' ')+
                "LOUNGE"
                + new String(new char[13]).replace('\0', ' ')
                + "PARK"
                + new String(new char[32]).replace('\0', ' ')
                + "REPAIR AREA"
                + new String(new char[51]).replace('\0', ' ')
                + "SUPPLIER SITE\n";
        //line 4
        logStates+=new String(new char[15]).replace('\0', ' ')+
                " InQ  Wtk  NRV "
                + new String(new char[5]).replace('\0', ' ')
                + " NCV  NPV "
                + new String(new char[10]).replace('\0', ' ')
                + "NSRQ Prt0  NV0   S0 Prt1  NV1   S1 Prt2  NV2   S2 "
                + new String(new char[30]).replace('\0', ' ')
                + " PP0  PP1  PP2 \n";
        
        //finally, attempt to write
        try{
            this.fWriter.write(logStates);
            this.fWriter.flush();
        }
        catch(IOException ex){
            GenericIO.writelnString("IO Exception attempting to logHeader\n");
            ex.printStackTrace();
        }
        catch(NullPointerException ex){
            GenericIO.writelnString("No FileWriter\n");
            ex.printStackTrace();
        }
    }
   
    /**
     * set customer car driven
     * @param cId customer id
     * @param repId replacement id
     * @param secondTime secondtime
     * @throws RemoteException 
     */
    public synchronized void setCustCarDriven(int cId, int repId, boolean secondTime) throws RemoteException{
        if(secondTime){
            //second time: always leaves with own car NLWC
            customerVehicleDriven[cId]=String.format("%4d",+cId);
        }
        else if(!secondTime && repId==-1){
            //first time and no key: NLWO
            customerVehicleDriven[cId]="----";
            //System.err.printf("LOG: setCustCarDriven{cId: %d, repId: %d,"
            //        + " secondTime:%b}\n",cId,repId,secondTime);
        }
        else if(!secondTime && repId!=-1 ){
            //first time and has a key: NLWC (rep car)
            customerVehicleDriven[cId]=String.format("  R%d",+repId);
            //System.err.printf("LOG2: setCustCarDriven{cId: %d, repId: %d,"
            //        + " secondTime:%b}\n",cId,repId,secondTime);
        }
        else{
            //this should never happen
            System.err.printf("ERROR: setCustCarDriven{cId: %d, repId: %d,"
                    + " secondTime:%b}\n",cId,repId,secondTime);
        }
    }
    /**
     * Set customer state 
     * @param cId customer id
     * @param cState customer state
     * @throws RemoteException 
     */
    public synchronized void setCustomerState(int cId, CustomerState cState) throws RemoteException{
        customerState[cId]=cState;
        logToFile();
        
    }
    /**
     * increment or decrement parked vehicles depending on Remove value
     * @param remove 
     * @throws RemoteException 
     */
    public synchronized void setParkedVehicles(boolean remove) throws RemoteException{
        if(remove)
            nParkedCustVehicles--;
        else
            nParkedCustVehicles++;
    }
    /**
     * set mechanic state
     * @param mId mech id
     * @param mState mech state
     * @throws RemoteException 
     */
    public synchronized void setMechanicState(int mId, MechanicState mState) throws RemoteException{
        mechanicState[mId]=mState;
        logToFile();
    }
    /**
     * set manager state
     * @param mState manager state
     * @throws RemoteException 
     */
    public synchronized void setManagerState(ManagerState mState) throws RemoteException{
        managerState=mState;
        logToFile();
    }
    /**
     * repair car submitted
     * @param customerId customer id
     * @throws RemoteException 
     */
    public synchronized void repairCarSubmitted(int customerId) throws RemoteException{
        nServiceRequests++;
        logToFile();
        //only this?
    }
    /**
     * set inQ
     * @param qsize in queue size
     * @throws RemoteException 
     */
    public synchronized void setInQueueSize(int qsize) throws RemoteException{
        inQueueSize = qsize;
    }
    /**
     * set waiting replacement
     * @param cId customer id
     * @param wantRep want replacement car
     * @throws RemoteException 
     */
    public synchronized void setWaitingReplacement(int cId, boolean wantRep) throws RemoteException{
        
        customerRequiresReplacement[cId]=wantRep ? "   T":"   F";
        //logToFile();
    }
    /**
     * increment waiting for replacement
     * @param log if write to file
     * @throws RemoteException 
     */
    public synchronized void incrementNWaitingRepl(boolean log) throws RemoteException{
        nWaitingReplacement++;
        if(log)logToFile();
    }
    /**
     * decrement waiting for replacement
     * @param log if write to file
     * @throws RemoteException 
     */
    public synchronized void decrementNWaitingRepl(boolean log) throws RemoteException{
        nWaitingReplacement--;
        if(log)logToFile();
    }
    /**
     * set replacement vehicles
     * @param ncars number of cars
     * @throws RemoteException 
     */
    public synchronized void setReplacementVehicles(int ncars) throws RemoteException{
        nAvailableReplacementVehicles = ncars;
        //nWaitingReplacement--;
        logToFile();
    }
    
    /**
     * increment available replacement vehicles
     * @throws RemoteException 
     */
    public synchronized void incrementNCars() throws RemoteException{
        nAvailableReplacementVehicles++;
    }
    /**
     * decrement available replacement vehicles
     * @throws RemoteException 
     */
    public synchronized void decrementNCars() throws RemoteException{
        nAvailableReplacementVehicles--;
    }
    /**
     * increment number of repaired vehicles
     * @param cId
     * @throws RemoteException 
     */
    public synchronized void setNRepaired(int cId) throws RemoteException{
        nRepairedVehicles++;
        customerVehicleRepaired[cId]="   T";
        //logToFile();
    }
    
    /**
     * set vehicles waiting for specific part
     * @param spendPart spend part
     * @param pId part id
     * @param wasWaitingPart was already waiting
     * @throws RemoteException 
     */
    public synchronized void setVehicleWaitingPart(boolean spendPart, int pId, 
            boolean wasWaitingPart) throws RemoteException{
        if(spendPart){
            if(wasWaitingPart){
                switch(pId){
                    case 0:{
                        nVehicWaitingPart0--;
                        nAvailablePart0--;
                        break;
                    }
                    case 1:{
                        nVehicWaitingPart1--;
                        nAvailablePart1--;
                        break;
                    }
                    case 2: {
                        nVehicWaitingPart2--;
                        nAvailablePart2--;
                        break;
                    }
                }
            
            }
            else{
                switch(pId){
                    case 0:{
                        nAvailablePart0--;
                        break;
                    }
                    case 1:{
                        nAvailablePart1--;
                        break;
                    }
                    case 2: {
                        nAvailablePart2--;
                        break;
                    }
                }
            }
        }
        else{
            if(!wasWaitingPart){
                switch(pId){
                    case 0:{
                        nVehicWaitingPart0++;
                        break;
                    }
                    case 1:{
                        nVehicWaitingPart1++;
                        break;
                    }
                    case 2: {
                        nVehicWaitingPart2++;
                        break;
                    }
                }
            }
         
        }
        logToFile();
    }
    /**
     * set flag part missing
     * @param pId part id
     * @throws RemoteException 
     */
    public synchronized void setFlagPartMissing(int pId) throws RemoteException{
        switch (pId){
            case 0: {
                flagMissingPart0='T';
                break;
            }
            case 1: {
                flagMissingPart1='T';
                break;
            }
            case 2: {
                flagMissingPart2='T';
                break;
            }
            default:break;
        }
        //logToFile();
    }
    /**
     * set replenished part
     * @param part part id
     * @throws RemoteException 
     */
    public synchronized void setReplenishPart(int part) throws RemoteException{
        switch (part){
            case 0: {
                flagMissingPart0='F';
                nAvailablePart0=SimParameters.N_PARTS;
                
                break;
            }
            case 1: {
                flagMissingPart1='F';
                nAvailablePart1=SimParameters.N_PARTS;
                
                break;
            }
            case 2: {
                flagMissingPart2='F';
                nAvailablePart2=SimParameters.N_PARTS;
                
                break;
            }
            default:break;
        }
        //logToFile();
    }
    /**
     * set bought part
     * @param part part id
     * @throws RemoteException 
     */
    public synchronized void setBoughtPart(int part) throws RemoteException{
        switch (part){
            case 0: 
            {
                partPurchaseCounter0+=5;
                break;
            }
            case 1: 
            {
                partPurchaseCounter1+=5;
                break;
            }
            case 2:
            { 
                partPurchaseCounter2+=5;
                break;
            }
           
        }
        //logToFile();
    }
    
    
    /**
     * Stop the service and shut down the betting center.
     * @throws java.rmi.RemoteException
     */
    public synchronized void serviceEnd() throws RemoteException{
        Main.serviceEnd = true;
        notifyAll();
    }
    
}
