package MonSolution;

/**
 * Interface - Customer Outside World
 */

public interface iCustomerOutside {
    
    /**
     * Back to work by car (originated by the customer).
     * 
     * @param customerID Identifier of the customer.
     * @param owncar Identifies the car driven by the customer.
     */
    public void backToWorkByCar(int customerID, boolean owncar);
    
    /**
     * Back to work by bus (originated by the customer).
     * 
     * @param customerID Identifier of the customer    
     */
    public void backToWorkByBus(int customerID);
    
}

