package MonSolution;

/**
 * Interface - Manager Supplier Site
 */

public interface iManagerSupplier {
    
    /**
     * Manager go to Supplier Site
     * 
     * @param part Part to replace.
     * @return parts Number of parts of that type to store.
     */
    public int goToSupplier(int part);
    
}
