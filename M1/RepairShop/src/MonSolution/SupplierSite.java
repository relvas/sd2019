package MonSolution;
import Entities.Customer;
import Entities.Manager;
import Entities.ManagerState;
import repairshop.*;

/**
 * 
 * Shared region - Supplier Site
 */
public class SupplierSite implements iManagerSupplier{
    
    /**
     * General Information
     * 
     * @serialField logger
     */
    private GeneralInformation logger; 
 
    public SupplierSite(GeneralInformation repository){
        logger = repository;
    }
    
    /**
     * Manager go to Supplier Site
     * 
     * @param part Part to replace.
     * @return parts Number of parts of that type to store.
     */
    @Override
    public synchronized int goToSupplier(int part)
    {
       ((Manager) Thread.currentThread()).setEntityState(ManagerState.GNCA);
       logger.setBoughtPart(part);
       logger.setManagerState(ManagerState.GNCA); 
       
       return part;
    }
}
