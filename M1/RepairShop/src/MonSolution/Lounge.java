package MonSolution;

import repairshop.*;
import Entities.*;

/** 
 * Shared region - Lounge
 * 
 */

public class Lounge implements iCustomerLounge, iMechanicLounge, iManagerLounge{
 
    /**
     * General Information
     * 
     * @serialField logger
     */
    private GeneralInformation logger;
    
    
    /**
    * Number of available replace car.
    * 
    * @serialField  ncars
    */
   private int ncars;
    
    /**
    * Number of replace car keys. 
    * 
    * @serialField keys[]
    */
    private int keys[];
    
    /**
    * Boolean client payed the service 
    * 
    * @serialField payed
    */
    private boolean payed;
    

    /**
    * Manager tasks related to customers
    *
    * @serialField managerToDo;
    */
    private MemFIFO managerToDo;
    
    /**
    * Manager tasks related to mechanics
    *
    * @serialField managerToDoMec;
    */
    private MemFIFO managerToDoMec;
    
    
    /**
    * Alert manager car is ready to pick up
    *
    * @serialField managerToDoReady;
    */
    private MemFIFO managerToDoReady;
    
    
    /**
    * Attending the current customerId
    *
    * @serialField turnCurrentCId;
    */
    private int turnCurrentCId;
    
    /**
    * Attending the current customerId (control)
    *
    * @serialField turnCurrentCIdCondition;
    */
    private int turnCurrentCIdCondition;
    
    /**
    * Waiting for customer
    *
    * @serialField waitingCustomer;
    */
    private boolean waitingCustomer;
    
    /**
    * Check if the current customer wants replecement car
    *
    * @serialField currentCWantRepCar;
    */
    private boolean currentCWantRepCar;
    
    /**
    * Customer wants to pay
    *
    * @serialField wantToPay;
    */
    private boolean wantToPay;
    
    
    /**
    *  Customers waiting for replace car.
    *
    * @serialField waitingRepCar
    */
    private MemFIFO waitingRepCar;
    
    /**
    * Array to know if customer has to pay
    * index=cId
    * value=hasToPay
    * @serialField doIHaveToPay;
    */
    private boolean[] doIHaveToPay;
    
    /**
     * Queue size
     * 
     * @serialField queueSize
     */
    private int queueSize = 0;
    
    /**
     * Not first
     * 
     * @serialField notFirst
     */
    private boolean notFirst;
    
    /**
     * Flag Missing
     * 
     * @serialField flagMissing[]
     */
    private boolean[] flagMissing;
    
    /**
     * Turn to pick up car
     * 
     * @serialField turnTopickUpCar
     */
    private int turnToPickUpCar=-1;
    
    /**
     * Is Coming from queue in
     * 
     * @serialField isComingFromQueueIn
     */
    private boolean isComingFromQueueIn;
    
    /**
     * Id of who's paying
     * 
     * @serialField payingId
     */
    private int payingId=-1;
    
    /**
    * Init the Lounge.
    *
    * @param repository Instance that implements GeneralInformation methods.
    */
    public Lounge(GeneralInformation repository)
    {
        logger = repository;
        waitingRepCar = new MemFIFO (SimulationParameters.nCustomers);
        ncars = SimulationParameters.nReplacementCars;
        
        managerToDo= new MemFIFO(SimulationParameters.nCustomers);
        managerToDoMec= new MemFIFO(SimulationParameters.nCustomers);
        managerToDoReady= new MemFIFO(SimulationParameters.nCustomers);
        
        keys = new int [SimulationParameters.nReplacementCars];
        
        for(int i = 0; i < SimulationParameters.nReplacementCars; i++)
            keys[i] = -1;
        
        payed = false;
        waitingCustomer=true;
        isComingFromQueueIn=false;
        turnCurrentCId=-1;
        turnCurrentCIdCondition=-1;
        wantToPay=false;
        doIHaveToPay = new boolean[SimulationParameters.nCustomers];
        for (int k = 0; k<SimulationParameters.nCustomers;k++ ){
            doIHaveToPay[k] = false;
        }
        
        notFirst=false;
        flagMissing = new boolean[SimulationParameters.nDiffParts];
        for (int k = 0; k<SimulationParameters.nDiffParts;k++){
            flagMissing[k] = false;
        }
    }
    
    /**
    * Get next task (originated by the manager).
    * 
    *  
    */
    @Override
    public synchronized boolean getNextTask()
    {
        if(notFirst){
            ((Manager) Thread.currentThread()).setEntityState(ManagerState.CWTD);
            logger.setManagerState(ManagerState.CWTD);

        }
        else
            notFirst = true;
        
          
        while (managerToDo.empty() && managerToDoMec.empty() && managerToDoReady.empty()) 
        {
            try
            {
                wait ();
            }
            catch (InterruptedException e){return false; }
        }
        
        return true;
    }
    
    /**
     *  Manager is appraising situation to decide what to do.
     * 
     */
    @Override
    public synchronized Situation appraiseSit()
    {
       if (!managerToDo.empty()) {
            isComingFromQueueIn=true;
            return Situation.ATTENDINGCOSTUMER; 
        }
        else if (!managerToDoMec.empty()) {
            return Situation.GETTINGNEWPARTS;
        }
        else if(!managerToDoReady.empty()){
            return Situation.ALERTCOSTUMER;
        }
        return Situation.ERRO;
    }
    
    /**
    * The Manager talks to the Customer.
    * 
    * @return What the customer wants
    */
    
    @Override
    public synchronized Action talkToCustomer(){
        ((Manager) Thread.currentThread()).setEntityState(ManagerState.ATTC);
        decrementQueueSize();
        logger.setInQueueSize(queueSize);
        logger.setManagerState(ManagerState.ATTC);
        turnCurrentCId=-1;
        
        waitingCustomer=true;
          
        turnCurrentCId = (int)managerToDo.read();
        turnCurrentCIdCondition=turnCurrentCId;
        
        
        notifyAll();
        
        while(waitingCustomer)
        {
            try
            {
                wait();
            } catch (InterruptedException e){}
        }
        if(doIHaveToPay[turnCurrentCId])
            return Action.PAY;
        
        doIHaveToPay[turnCurrentCId]=true;
        
        if(currentCWantRepCar){
            currentCWantRepCar=false;
            return Action.KEY;
        }
        
        return Action.NOKEY;
        
        
    }
    
    /**
     * The Manager checks if there is a replacement car available and if there is a key is delivery to the Customer.  
     * 
     * @param customerID Identifies the Customer.
     */
    @Override
    public synchronized void handCarKey(int customerID)
    {
       turnToPickUpCar=customerID;
       turnCurrentCId=-1;
       notifyAll();
    }
    
    /**
     * Manager checks if the Customer have a replace car.  
     * 
     */ 
    @Override
    public synchronized void receivePayment()
    {
        notifyAll();
        while(!payed)
        {
            try
            {
                wait();
            } catch (InterruptedException e){}
        }
        turnToPickUpCar=-1;
        for(int i = 0; i < SimulationParameters.nReplacementCars; i++)
        {
            if(keys[i] == payingId){
                keys[i]=-1;
                try {
                    turnToPickUpCar=(int)waitingRepCar.peek();
                } catch (Exception e) {
                }
                logger.incrementNCars();
                notifyAll();       
            }
        }
        payingId=-1;
        payed = false;
    }
    
    /**
     * Get part
     * 
     * @return Part
     */
    @Override
    public synchronized int getPartId(){
        return (int)managerToDoMec.read();
    }
    
    /**
     * Set Flag restocked
     * 
     * @param partId Identifies the part
     */
    @Override
    public synchronized void setFlagRestocked(int partId){
       flagMissing[partId]=false;
    }
    /**
     * Get customer
     * 
     * @return customer
     */
    @Override
    public synchronized int getCId(){
        return turnCurrentCId;
    }
    
    /**
     * Get Phone
     * 
     * @return phone
     */
    @Override
    public synchronized int getPhoneId(){
        return (int)managerToDoReady.read();
    }
    
    
    /**
    * The Mechanic let the Manager know we need to replace a part.
    * 
    * @param mechanicID Identifies the Mechanic.
    * @param part Identifies the part needed.
    */
    @Override
    public synchronized void letManagerKnow(int mechanicID, int part)
    {
        ((Mechanic)Thread.currentThread()).setEntityState(MechanicState.ALLM); 
        logger.setParkedVehicles(false);
        logger.setMechanicState(mechanicID, MechanicState.ALLM);
        if(part>=0){
            managerToDoMec.write(part); 
        }    
        notifyAll();
    }
    
    /**
    * The Mechanic let the Manager knows that the car from customerID is ready.
    * 
    * @param mechanicID Identifies the Mechanic.
    * @param customerID Identifies the Customer car.
    */
    @Override
    public synchronized void repairConcluded(int mechanicID, int customerID)
    {
        ((Mechanic)Thread.currentThread()).setEntityState(MechanicState.ALLM);
        managerToDoReady.write(customerID);
        
        logger.setMechanicState(mechanicID, MechanicState.ALLM);
        
        notifyAll();
    }
    
    
    /**
     * Customer is waiting for is turn to talk with the Manager.   
     * 
     * @param customerID Identifies the Customer.
     */
    @Override
    public synchronized void queueIn(int customerID)
    {
        ((Customer) Thread.currentThread()).setEntityState(CustomerState.RECE);
        incrementQueueSize();
        logger.setInQueueSize(queueSize);
        logger.setCustomerState(customerID, CustomerState.RECE);
        
        managerToDo.write(customerID);
       
        notifyAll();
        while(turnCurrentCIdCondition!=customerID)
        {
            try
            {
                wait();
            } catch (InterruptedException e){}
        }
        turnCurrentCIdCondition=-1; 
    }
    
    /**
     * Talk with the Manager.  
     * 
     * @param customerID Identifies the Customer
     * @param wantRepCar Identifies if a replace car is needed.
     */
    @Override
    public synchronized void talkWithManager(int customerID, boolean wantRepCar)
    {
        logger.setWaitingReplacement(turnCurrentCId, wantRepCar);
        if (wantRepCar) { 
            currentCWantRepCar=true;
            waitingRepCar.write(customerID);
        }
        else{
            currentCWantRepCar=false;
        }
        waitingCustomer=false;
        notifyAll();
    }
    
    /**
     * The Customer collects the key of the replaced car.  
     * 
     * @param customerID Identifies the Customer.
     * @return key Identifies the replaced car.
     */
    @Override
    public synchronized int collectKey(int customerID)
    {
        ((Customer) Thread.currentThread()).setEntityState(CustomerState.WFRC);
        logger.incrementNWaitingRepl(false);
        logger.setCustomerState(customerID, CustomerState.WFRC);
        
        turnCurrentCIdCondition=-1;
        
        while(true)
        {
            if(turnToPickUpCar==customerID)
            {
                turnToPickUpCar=-1;
                for(int i = 0; i < SimulationParameters.nReplacementCars; i++)
                {
                    if(keys[i] == -1){
                        keys[i]=customerID;
                        waitingRepCar.read();
                        logger.decrementNCars();
                        
                        return i;
                    }
                }
            }
            try
            {  
                wait();
            } catch (InterruptedException e){} 
        }   
    }
    
    /**
    * Pay for Service.  
    * @param customerID Identifies the Customer.
    */
    @Override
    public synchronized void payForService(int customerID)
    {
        turnCurrentCIdCondition=-1;
        waitingCustomer=false;
        payingId=customerID;
        payed = true;
        notifyAll();
    }
    
    /**
     * Increment queue size
     */
    private synchronized void incrementQueueSize(){
        this.queueSize++;
    }
    
    /**
     * Decrement queue size
     */
    private synchronized void decrementQueueSize(){
        this.queueSize--;
    }
}