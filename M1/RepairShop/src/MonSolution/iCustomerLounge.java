package MonSolution;

/**
 * Interface - Customer Lounge
 */

public interface iCustomerLounge {
    
    /**
     * Customer is waiting for is turn to talk with the Manager.   
     * 
     * @param customerID Identifies the Customer.
     */
    public void queueIn(int customerID);
    
    /**
     * Talk with the Manager.  
     * 
     * @param customerID Identifies the Customer
     * @param wantRepCar Identifies if a replace car is needed.
     */
    public void talkWithManager(int customerID, boolean wantRepCar);
    
    /**
     * The Customer collects the key of the replaced car.  
     * 
     * @param customerID Identifies the Customer.
     * @return key Identifies the replaced car.
     */
    public int collectKey(int customerID);
    
    
    /**
    * Pay for Service.  
    * @param customerID Identifies the Customer.
    */
    public void payForService(int customerID);
    
    
}
