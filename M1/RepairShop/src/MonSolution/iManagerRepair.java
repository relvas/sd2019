package MonSolution;

/**
 * Interface - Manager Repair Area
 */

public interface iManagerRepair {
    
    /**
     * Manager store part.  
     *  
     * @param part Identifies the part.
     */
    public void storePart(int part);
    
    /**
     * The Manager register a service.
     * 
     * @param customerID Identifies the Customer car.
     */
    public void registerService(int customerID);
    
}
