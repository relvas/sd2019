package MonSolution;
import repairshop.*;
import Entities.*;
import java.util.Random;


/**
 * 
 * Shared region - Park
 */

public class Park implements iCustomerPark, iMechanicPark{
    
    /**
     * Reference to the General Information
     * 
     * @serialField logger
     */
    private GeneralInformation logger; 
    
    
    /**
     * Array to Customers cars.
     * 
     * @serialFiel cars[]
     */
    private boolean [] cars;

    /**
     * Array to Replacement cars.
     * 
     * @serialField carsRep[]
     */
    private int [] carsRep;
    
    /**
     * Which part needs to be replaced.
     * 
     * @serialField part
     */
    private int part;
    
   /**
     * Cart part id
     * 
     * @serialField carPartId
     */
    private int[] carPartId;
    
    
    /**
    * Init the Park.
    *
    * @param repository Instance that implements GeneralInformation methods.
    */        
    public Park(GeneralInformation repository){
        logger = repository;
        cars = new boolean [SimulationParameters.nCustomers];
        carsRep = new int [SimulationParameters.nReplacementCars];
        part=-1;
        carPartId = new int[SimulationParameters.nCustomers];
        for (int i = 0; i < SimulationParameters.nCustomers; i++) 
        {
            cars[i]=false;
            carPartId[i]=-1;
        }
        
        for (int i = 0; i < SimulationParameters.nReplacementCars; i++) 
        {
            carsRep[i]=-1;
        }
    }
    
    /**
     * Customer go to the repair shop and Park the car - his own or the replace car.
     * 
     * @param customerID Identifies the Customer car. 
     */
    @Override
    public synchronized void goToRepairShop(int customerID)
    {
        ((Customer) Thread.currentThread()).setEntityState(CustomerState.PARK);
        
        
        if(!cars[customerID]){
            logger.setParkedVehicles(false);
        }
        
        logger.setCustomerState(customerID, CustomerState.PARK);
        for(int i = 0; i < SimulationParameters.nReplacementCars; i++)
        {
            if(carsRep[i] == customerID) 
                carsRep[i] = -1;
            else  
                cars[customerID] = true;
        }
    }
    
  
    /**
     * Customer WFRCs the replace car.
     * 
     * @param customerID Identifies the Customer car.
     * @param key Identifies the replace car.
     */
    @Override
    public synchronized void findCar(int customerID, int key)
    {
        
        ((Customer) Thread.currentThread()).setEntityState(CustomerState.PARK);
        logger.decrementNWaitingRepl(false);
        logger.setCustomerState(customerID, CustomerState.PARK);
        
        for(int i = 0; i < SimulationParameters.nReplacementCars; i++)
        {
            if(i == key)
                carsRep[i] = customerID;
        }   
    }
    
    /**
     * Customer removes his repaired car from the Park. 
     * 
     * @param customerID Identifies the Customer car. 
    */
    @Override
    public synchronized void collectCar(int customerID)
    {
        ((Customer) Thread.currentThread()).setEntityState(CustomerState.PARK);
        logger.setCustomerState(customerID, CustomerState.PARK);
        cars[customerID]=false;
        notifyAll();
    }
    
    
    /**
     * Mechanic goes to the park to get the car and verifies which part needs to be replaced.  
     * 
     * @param mechanicID Identifies the Customer car.
     * @param customerID Identifies the Mechanic.
     * @return Returns the part to be replaced.
     */
    @Override
    public synchronized int getVehicle(int mechanicID, int customerID)
    {
        
        ((Mechanic) Thread.currentThread()).setEntityState(MechanicState.FIXI);
        logger.setParkedVehicles(true);
        logger.setMechanicState(mechanicID, MechanicState.FIXI);
        if (carPartId[customerID]>=0)
            return carPartId[customerID];
                
        double a;
        cars[customerID] = false;
        a = (Math.random() * ((3 - 0) + 1)) + 0;
        if(a < 1)
            part = 0;
        else if(a > 1 && a < 2)
            part = 1;
        else
            part = 2;
        
        carPartId[customerID]=part;
        return part; 
    }
    
    /**
     * The Mechanic returns the Customer car repared to the Park.  
     * 
     * @param customerID Identifies the Customer car.
     */
    @Override
    public synchronized void returnVehicle(int customerID)
    {
        logger.setParkedVehicles(false);
        logger.setNRepaired(customerID);
        cars[customerID] = true;
    }
}
