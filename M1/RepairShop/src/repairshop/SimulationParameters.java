package repairshop;

/**
 * Serves as a way of providing global variables to all the Entities and Shared
 * Regions in the problem.
 * 
 */ 

public class SimulationParameters {
    
    /**
     * Number of existing Mechanics.
     */
    public static final int nMechanics = 2;
    
    /**
     * Number of customers.
     */
    public static final int nCustomers = 30;
    
    
    /**
     * Number of existing replacements cars. 
     */
    public static final int nReplacementCars = 3;
    
    
    /**
     * Number of different parts.
     */
    public static final int nDiffParts = 3;
    
    /**
     * Number of exising parts stored.
     */
    public static final int nParts = 5;
    
}
