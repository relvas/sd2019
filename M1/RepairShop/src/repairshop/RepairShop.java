package repairshop;

import Entities.Customer;
import Entities.Manager;
import Entities.Mechanic;
import genclass.GenericIO;
import genclass.FileOp;

import MonSolution.Lounge;
import MonSolution.Park;
import MonSolution.OutsideWorld;
import MonSolution.RepairArea;
import MonSolution.SupplierSite;
import java.util.Random;
import java.util.Scanner;
import repairshop.SimulationParameters;

/**
 *  Este tipo de dados simula o problema de uma oficina implementando uma solução concorrente baseada em
 *  monitores como elemento de sincronização entre os threads envolventes.
 */

public class RepairShop {
    public static void main (String [] args)
    {
        int nCustomers = SimulationParameters.nCustomers;
        int nMechanics = SimulationParameters.nMechanics;
        String fName = "log.txt";
        
        GenericIO.writelnString ("\n" + "Initialização...\n");
        GeneralInformation logger;
        logger = new GeneralInformation(fName);
        Random random = new Random();
        Lounge lounge =  new Lounge(logger);
        Park park = new Park(logger);
        RepairArea repairArea = new RepairArea(logger);
        SupplierSite supplierSite = new SupplierSite(logger);
        OutsideWorld outsideWorld = new OutsideWorld(logger);
        
        /* Entitities Instanciation */
        Manager manager = new Manager(0, supplierSite, lounge, repairArea,
                outsideWorld);
        
        int n; 
        Customer[] customer = new Customer[nCustomers];
        for(n=0; n<nCustomers; n++){
            customer[n] = new Customer(n, park, lounge, outsideWorld, random.nextBoolean());
        }
        
        Mechanic[] mechanic = new Mechanic[nMechanics];
        for(n=0; n<nMechanics; n++){
            mechanic[n] = new Mechanic(n, park, lounge, repairArea);
        }
        
        /* Begin Simulation */
        manager.start();
        for(n=0; n<nMechanics; n++){
            mechanic[n].start();
        }
        for(n=0; n<nCustomers; n++){
            customer[n].start();
        }
        
        for(n=0; n<nCustomers; n++){
            try{
                customer[n].join();     
            }catch(Exception e){
            }
        }
        System.err.println("All customers finished");
      
        for(n=0;n<nMechanics;n++){
            if(mechanic[n].isAlive()){
                mechanic[n].interrupt();
            }
            
        }
        System.err.println("All mechanics finished");
        
        manager.interrupt();
        while (manager.isAlive()){}
        System.err.println("Manager finished");
        logger.closeFile();
        System.err.println("DONE");
    } 
}