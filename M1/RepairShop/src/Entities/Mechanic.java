package Entities;;
import MonSolution.*;
/**
 *  Este tipo de dados implementa o thread mecânico.<p>
 *  A sua vida divide-se por duas tarefas principais, ler o jornal e trabalhar, que são realizadas sequencialmente.<p>
 *  Nesta solução, trabalhar supõe o desenvolver das sub-tarefas seguintes:
 *      - no princípio da sua actividade é acordado pelo manager e vai buscar um carro ao parque;
 *      - verifica se existe a peça disponível para efeturar a reparação;
 *      - caso exista começa a reparação;
 *      - caso não exista avisa o manager e volta a ler o jornal;
 *      - ao terminar a reparação leva o carro até ao parque e volta a ler o jornal;
 */

public class Mechanic extends Thread {
    
    /**
    *   Estado do mecanico
    * 
    *   @serialField entityState
    */
    private MechanicState entityState;
    
    
   /**
   *  Identificação do mecanico
   *
   *    @serialField mechanicId
   */

   private int mechanicId;
   
   
   
  /**
   *  Parque
   *
   *    @serialField park
   */

   private Park park;
   
   
  /**
   *  Lounge
   *
   *    @serialField lounge
   */

   private Lounge lounge;
   
   
   
  /**
   *  Repair Area
   *
   *    @serialField repair
   */

   private RepairArea repair;
   
   /**
   *  Instanciação do thread mecânico.
   *
   *    @param mechanicId identificação do mecanico
   *    @param park parque
   *    @param lounge lounge
   *    @param repair repair area
   */

   public Mechanic (int mechanicId, Park park, Lounge lounge, RepairArea repair)
   {
      this.mechanicId = mechanicId;
      this.park = park;
      this.lounge = lounge;
      this.repair = repair;
   }
   
   /**
   *  Ciclo de vida do thread mecânico.
   */

   @Override
   public void run ()
   {
      boolean test;
      while(true)                                 
      {
          test=repair.readThePaper(mechanicId);
          if(!test) break;
          int cId;
          int pId;
          cId=repair.startRepairProcedure(mechanicId);                       
          if(cId==-1){
              continue;
          }
          pId=park.getVehicle(mechanicId, cId);                              
          repair.getRequiredPart(mechanicId, cId, pId);                      
          int pId2=repair.partAvailable(cId, pId);
          if (pId2<0)                                     
          {
              if (pId2==-1) {
                lounge.letManagerKnow(mechanicId, pId);                          
                continue; 
              }
              else{
                lounge.letManagerKnow(mechanicId, -2);                          
                continue; 
              }                                                                
          }
          else
          {
              repair.resumeRepairProcedure(mechanicId, cId, pId);          
          }
          repair.fixIt(mechanicId, cId, pId);                                 
          park.returnVehicle(cId); 
          lounge.repairConcluded(mechanicId, cId);                 
      } 
      
   }
  

   /**
     * This funtions returns the Mechanic's actual state.
     *
     * @return entityState Actual state of the Mechanic.
     */
    public MechanicState getEntityState()
    {
        return entityState;
    }

    /**
     * This funtions sets the Mechanic's state.
     *
     * @param entityState Next state to set the Mechanic.
     */
    public void setEntityState(MechanicState entityState)
    {
        this.entityState = entityState;
    }
    
    /**
     * This functions returns the Mechanic ID
     * 
     * @return mechanicId
     */
    public int getMechId(){
        return mechanicId;
    }
}
