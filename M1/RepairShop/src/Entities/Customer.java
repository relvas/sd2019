package Entities;
import MonSolution.*;
/**
 *  Este tipo de dados implementa o thread costumer.<p>
 *  A sua vida divide-se por duas tarefas principais, ter uma vida normal e decidir ir reparar o carro, 
 *  que são realizadas sequencialmente.<p>
 *  Nesta solução, decidir ir reparar o carro divide-se nas sub-tarefas seguintes:
 *      - no princípio da sua actividade é acordado ao fim de um tempo aleatório e decide ir reparar o carro;
 *      - mete-se na fila da receção à espera de ser atendido;
 *      - Quando chega à sua vez fala com o manager  decide se quer ou não um carro de substituição; 
 *      - se quiser volta para casa de carro, caso contratio volta para casa de autocarro;
 *      - quando o carro está pronto o manger liga-lhe e ele volta à fila de espera para ser atendido;
 *      - paga o serviço, recolhe as chaves do seu carro e volta à sia vida normal;
 */

public class Customer extends Thread{
    /**
    *   Estado do customer
    *   @serialField entityState
    */
    private CustomerState entityState;
    
    /**
    *  Identificação do customer
    *
    *    @serialField customerId
    */
    
    private int customerId;
    
    /**
    *  Lounge
    *
    *    @serialField lounge
    */

    private Lounge lounge;
    
    
    /**
    *  Outside World
    *
    *    @serialField outside
    */

    private OutsideWorld outside;
    
    /**
    *  Park
    *
    *    @serialField park
    */

    private Park park;
    
    /**
    *  Want replacement car
    *
    *    @serialField wantRepCar
    */
    private boolean wantRepCar;
    
    /**
    *  Car Key
    *
    *    @serialField key
    */
    private int key;
    
    /**
    *  Thread manager instantiation.
    *
    *    costumerId identificação do customer
    *    @param park park
    *    @param lounge lounge
    *    @param outside outside world
    *    @param wantRepCar want replacement car
    */

    public Customer (int customerId, Park park, Lounge lounge, OutsideWorld outside, boolean wantRepCar)
    {
        this.customerId = customerId;
        this.park = park;
        this.lounge = lounge;
        this.outside = outside;
        this.wantRepCar = wantRepCar;
    }
    
    
    /**
    *  Customer thread's life cycle.
    */
    @Override
    public void run ()
    {
        
        decideOnRepair();
        park.goToRepairShop(customerId);
        lounge.queueIn(customerId);
        lounge.talkWithManager(customerId, wantRepCar);
        if (wantRepCar){
            key=lounge.collectKey(customerId); 
            park.findCar(customerId, key);
            outside.backToWorkByCar(customerId,false);
            park.goToRepairShop(customerId);
        }
        else
        {
            outside.backToWorkByBus(customerId);
            
        }
        lounge.queueIn(customerId);
        lounge.payForService(customerId);
        park.collectCar(customerId);
        outside.backToWorkByCar(customerId, true);
        
    }
    
    /**
     * This function waits a random time for the customer to decide on repairing
     * the car.
     */
    private void decideOnRepair()
    {
        try
        { 
            sleep ((long) (1 + 100 * Math.random ()));
        }
        catch (InterruptedException e) {}
    }

      
    /**
     * This function returns the Customer's current state.
     *
     * @return entityState current state of the Customer.
     */
    public CustomerState getEntityState()
    {
        return this.entityState;
    }

    /**
     * This function sets the Customer's state.
     *
     * @param entityState New Customer State.
     */
    public void setEntityState(CustomerState entityState)
    {
        this.entityState = entityState;
    }

    /**
     * This function returns the Key
     * 
     * @return key identifies the replace car
     */
    public int getKey() {
        return key;
    }

    /**
     * This function identifies if the customer wants replacement car
     * 
     * @return wantRepCar
     */
    public boolean isWantRepCar() {
        return wantRepCar;
    }
    
    
}

