package Entities;
import MonSolution.*;
import com.sun.xml.internal.ws.util.xml.CDATA;
import java.util.logging.Logger;
/**
 *  Este tipo de dados implementa o thread manager.<p>
 *  A sua vida divide-se por várias tarefas que são realizadas sequencialmente.<p>
 *  Nesta solução, as tarefas vão ser as seguintes:
 *      - no princípio da sua actividade pode ser acordado pelo mecânico ou 
 *            pelo costumer;
 *      - se for acordado pelo costumer fala com ele;
 *      - se for acordado pelo mecânico pode ter de mandar vir peças do site do fornecedor 
 *            ou ligar ao cliente para vaisar que o seu carro esta pronto;
 *      - está sempre a verificar o que ainda tem para fazer até não haver nada;
 */

public class Manager extends Thread{
    
    /**
    *   Estado do manager
    *   @serialField entityState
    */
    private ManagerState entityState;
    
    /**
    *  Identificação do manager
    *
    *    @serialField managerId
    */
    
    private int managerId;
    
    /**
    *  Lounge
    *
    *    @serialField lounge
    */

    private Lounge lounge;
    
    
    /**
    *  Repair Area
    *
    *    @serialField repair
    */

    private RepairArea repair;
    
    /**
    *  Supplier Site
    *
    *    @serialField supplier
    */

    private SupplierSite supplier;
    
    /**
    *  Outside World
    *
    *    @serialField outside
    */

    private OutsideWorld outside;
    
    
    /**
    *  Action
    *
    *    @serialField action
    */
    private Action action;
    
    /**
    *  Instanciação do thread manager.
    *
    *    @param managerId identificação do manager
    *    @param supplier supplier site
    *    @param lounge lounge
    *    @param repair repair area
    *    @param outside outside world
    */

    public Manager (int managerId, SupplierSite supplier, Lounge lounge, RepairArea repair, OutsideWorld outside)
    {
        this.managerId = managerId;
        this.supplier = supplier;
        this.lounge = lounge;
        this.repair = repair;
        this.outside = outside;
    }
    
    
    /**
    *  Ciclo de vida do thread mecânico.
    */
    @Override
    public void run ()
    {
        int partId;
        int stock;
        int cId;
        boolean test;
        while(true) 
        { 
            test=lounge.getNextTask();
            if(!test){
                break;
            }
            switch(this.lounge.appraiseSit())
            {
                case GETTINGNEWPARTS:
                    
                    partId=lounge.getPartId();
                    stock=supplier.goToSupplier(partId);
                    repair.storePart(stock);
                    lounge.setFlagRestocked(partId);
                    break;
                 
                case ALERTCOSTUMER:
                    
                    cId = lounge.getPhoneId();
                    outside.phoneCustomer(cId);
                    break;
                    
                case ATTENDINGCOSTUMER:

                    action = lounge.talkToCustomer();
                    cId = lounge.getCId();
                    switch(action)
                    {
                        case NOKEY:
                            repair.registerService(cId);
                            break;
                        
                        case KEY:
          
                            lounge.handCarKey(cId);
                            repair.registerService(cId);
                            break;
                            
                        case PAY:
                            lounge.receivePayment();
                            break;
                    }
                    break;
            }
        }
    } 


    /**
     * This functions returns the Manager's current state.
     *
     * @return entityState current state of the Manager.
     */
    public ManagerState getEntityState()
    {
        return entityState;
    }

    /**
     * This function sets the Manager's state.
     *
     * @param entityState Next state to set the Manager.
     */
    public void setEntityState(ManagerState entityState)
    {
        this.entityState = entityState;
    }

}